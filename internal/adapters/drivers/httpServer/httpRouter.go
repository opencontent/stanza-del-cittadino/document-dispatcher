package httpserver

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/swaggest/openapi-go/openapi3"
	"github.com/swaggest/rest/nethttp"
	"github.com/swaggest/rest/web"
	"github.com/swaggest/swgui/v5emb"
	"opencitylabs.it/documentdispatcher/config"
)

func (s *DocumentDispatcherServer) initRoutes() {

	setInfos(*s.conf, s.webService)

	s.webService.Post("/import", s.CreateDocument(), nethttp.SuccessStatus(http.StatusCreated))

	s.webService.Get("/status", s.GetStatus(), nethttp.SuccessStatus(http.StatusOK))
	s.webService.Method(http.MethodGet, "/metrics", promhttp.Handler())
	s.webService.Docs("/docs", v5emb.New)
}

func setInfos(conf config.Config, service *web.Service) {

	appName := "Document-Dispatcher:" + config.VERSION
	basePath := conf.Server.AddressPort

	service.OpenAPISchema().SetTitle("Document Dispatcher API")
	service.OpenAPISchema().SetDescription("Questo servizio si occupa di creare documenti digitali adatti alla protocollazione")
	service.OpenAPISchema().SetVersion(config.VERSION)

	openapiSchema := service.OpenAPICollector.Reflector().Spec

	contactName := "Support"
	contactUrl := "https://www.opencitylabs.it"
	contactEmail := "support@opencitylabs.it"
	openapiSchema.Info.WithContact(openapi3.Contact{
		Name:  &contactName,
		URL:   &contactUrl,
		Email: &contactEmail,
	})
	openapiSchema.Info.WithTermsOfService("https://opencitylabs.it/")
	openapiSchema.Info.WithMapOfAnything(map[string]interface{}{
		"x-api-id":  appName,
		"x-summary": "Questo servizio si occupa di creare documenti digitali adatti alla protocollazione",
	})
	serverDescription := appName
	openapiSchema.WithServers(openapi3.Server{
		URL:         basePath,
		Description: &serverDescription,
	})
	openapiSchema.WithTags([]openapi3.Tag{
		{
			Name: "Documents",
		},
		{
			Name: "Health Check",
		},
	}...)
}
