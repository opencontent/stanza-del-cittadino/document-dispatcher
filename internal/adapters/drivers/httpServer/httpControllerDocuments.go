package httpserver

import (
	"context"

	"github.com/swaggest/usecase"
	"github.com/swaggest/usecase/status"
	"go.uber.org/zap"
	"opencitylabs.it/documentdispatcher/internal/core/domain"
	"opencitylabs.it/documentdispatcher/metrics"
)

func (s *DocumentDispatcherServer) CreateDocument() usecase.Interactor {
	u := usecase.NewInteractor(func(ctx context.Context, input domain.DocumentDto, out *DocumentResponse) error {
		err := s.service.ProcessMessage(&input)
		if err != nil {
			s.log.Error("Error processing request",
				zap.String("tenant", input.TenantID),
				zap.String("entity instance id", input.RemoteCollection.EntityInstanceId),
				zap.String("external id", input.ExternalId),
				zap.String("remote collection type", input.RemoteCollection.EntityType),
				zap.Error(err))
			metrics.OpsFailedProcessed.WithLabelValues(s.conf.App.Environment, input.TenantID).Inc()
			return status.Wrap(err, status.Internal)
		}

		*out = s.buildResponse()
		metrics.OpsSuccessProcessed.WithLabelValues(s.conf.App.Environment, input.TenantID).Inc()

		return nil
	})

	u.SetTitle("Import Document")
	u.SetDescription("Import a document created externally")
	u.SetTags("Documents")
	u.SetExpectedErrors(status.Internal)
	return u
}

func (s *DocumentDispatcherServer) buildResponse() DocumentResponse {
	doc := s.service.GetDocument()

	out := DocumentResponse{
		Title:             doc.Title,
		RegistrySearchKey: doc.RegistrySearchKey,
		ID:                doc.ID,
		Status:            doc.Status,
		ShortDescription:  doc.ShortDescription,
		MainDocument:      doc.MainDocument,
		TenantID:          doc.TenantID,
		OwnerID:           doc.OwnerID,
		CreatedAt:         doc.CreatedAt,
		SourceType:        doc.SourceType,
		Sender:            doc.Sender,
		Recipients:        doc.Recipients,
		Holders:           doc.Holders,
	}
	if doc.FolderInfo != nil {
		out.FolderInfo = &domain.FolderInfo{}
		*out.FolderInfo = *doc.FolderInfo
	}
	RemoteCollection := RemoteCollectionResponse{}
	if doc.RemoteID != nil {
		out.RemoteCollection = &RemoteCollection
		out.RemoteCollection.EntityInstanceId = *doc.RemoteID
	}
	if doc.RemoteCollection != nil && doc.RemoteCollection.ID != "" {
		out.RemoteCollection = &RemoteCollection
		out.RemoteCollection.EntityId = doc.RemoteCollection.ID
	}
	if doc.RemoteCollection != nil && doc.RemoteCollection.Type != "" {
		out.RemoteCollection = &RemoteCollection
		out.RemoteCollection.EntityType = doc.RemoteCollection.Type
	}

	if doc.Description != nil {
		out.Description = doc.Description
	}
	if doc.Attachments != nil {
		out.Attachments = doc.Attachments
	}
	if doc.ValidFrom != nil {
		out.ValidFrom = *doc.ValidFrom
	}
	if doc.ValidTo != nil {
		out.ValidTo = *doc.ValidTo
	}
	if doc.TransactionID != nil {
		out.TransactionID = doc.TransactionID
	}
	if doc.ExternalId != nil {
		out.ExternalId = *doc.ExternalId
	}
	if doc.RegistrationData != nil && doc.RegistrationData.DocumentNumber != nil {
		out.RegistrationInfo.DocumentNumber = *doc.RegistrationData.DocumentNumber
	}
	if doc.RegistrationData != nil && doc.RegistrationData.Date != nil {
		out.RegistrationInfo.Date = *doc.RegistrationData.Date
	}
	if doc.RegistrationData != nil && doc.RegistrationData.RegistryCode != nil {
		out.RegistrationInfo.RegistryCode = *doc.RegistrationData.RegistryCode
	}

	return out
}
