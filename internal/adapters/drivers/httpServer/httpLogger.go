package httpserver

import (
	"net/http"
	"time"

	"github.com/go-chi/chi/v5/middleware"
	"go.uber.org/zap"
)

type httpLoggerFormatter struct {
	log *zap.Logger
}

func (formatter *httpLoggerFormatter) NewLogEntry(request *http.Request) middleware.LogEntry {
	return &httpLoggerEntry{
		log:     formatter.log,
		request: request,
	}
}

type httpLoggerEntry struct {
	log     *zap.Logger
	request *http.Request
}

func (entry *httpLoggerEntry) Write(status, bytes int, header http.Header, elapsed time.Duration, extra interface{}) {
	milliseconds := float64(elapsed) / float64(time.Millisecond)
	duration := float64(int(milliseconds*100+.5)) / 100

	fields := []zap.Field{
		zap.String("request_id", middleware.GetReqID(entry.request.Context())),
		zap.String("client_ip", entry.request.RemoteAddr),
		zap.String("referrer", entry.request.Referer()),
		zap.String("host", entry.request.Host),
		zap.Float64("duration", duration),
		zap.String("method", entry.request.Method),
		zap.String("path", entry.request.RequestURI),
		zap.Int("status", status),
	}

	if status == 404 {
		entry.log.Info("Not Found", fields...)
		// MetricsPaymentsValidationError.Inc()
	} else if status >= 500 {
		entry.log.Error("Server Error", fields...)
	} else {
		entry.log.Info("Request Processed", fields...)
	}
}

func (entry *httpLoggerEntry) Panic(v interface{}, stack []byte) {
	entry.log.Fatal("Panic occurred", zap.String("stack", string(stack)))
}
