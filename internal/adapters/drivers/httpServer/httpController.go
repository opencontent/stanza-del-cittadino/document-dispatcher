package httpserver

import (
	"context"

	"github.com/swaggest/usecase"
	"github.com/swaggest/usecase/status"
)

type output struct {
	Status string `json:"status"`
}

func (s *DocumentDispatcherServer) GetStatus() usecase.Interactor {
	u := usecase.NewInteractor(func(ctx context.Context, input struct{}, out *output) error {
		*out = output{
			Status: "healthy",
		}
		return nil
	})

	u.SetTitle("Health Check")
	u.SetDescription("check the service status")
	u.SetTags("Health Check")
	u.SetExpectedErrors(status.Internal)

	return u
}
