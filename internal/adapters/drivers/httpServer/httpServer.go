package httpserver

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"

	sentryhttp "github.com/getsentry/sentry-go/http"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/swaggest/openapi-go/openapi3"
	"github.com/swaggest/rest"
	"github.com/swaggest/rest/nethttp"
	"github.com/swaggest/rest/response"
	"github.com/swaggest/rest/response/gzip"
	"github.com/swaggest/rest/web"
	"go.uber.org/zap"
	"opencitylabs.it/documentdispatcher/config"
	"opencitylabs.it/documentdispatcher/internal/core/ports"
	"opencitylabs.it/documentdispatcher/logger"
)

var requestBodyCache string

// DocumentDispatcherServer rappresenta il server HTTP dell'applicazione.
type DocumentDispatcherServer struct {
	webService *web.Service
	conf       *config.Config
	service    ports.DocumentService
	log        *zap.Logger
}

// NewHttpDocumentServer inizializza il server con le configurazioni.
func NewHttpDocumentServer(ctx context.Context, service ports.DocumentService) *DocumentDispatcherServer {
	conf := config.GetConfig()
	webService := initService()

	// Configura middlewares (log, sicurezza, ecc.)
	/* 	server.OpenAPISchema().SetTitle = "Document Dispatcher API"
	   	server.OpenAPI.Info.Version = "1.0.0" */

	// Aggiunta interfaccia Swagger
	//webService.Docs("/docs", v5emb.New)

	// Inizializza il server
	Documentserver := &DocumentDispatcherServer{
		webService: webService,
		conf:       conf,
		service:    service,
		log:        logger.GetLogger(),
	}

	Documentserver.initRoutes()

	return Documentserver
}

// Run avvia il server HTTP sulla porta configurata.
func (s *DocumentDispatcherServer) Run() {
	log.Printf("Server listening on %s", s.conf.Server.AddressPort)
	if err := http.ListenAndServe(s.conf.Server.AddressPort, s.webService); err != nil {
		log.Fatalf("error starting http server: %v", err)
	}
}

func initService() *web.Service {
	log := logger.GetLogger()
	response.DefaultErrorResponseContentType = "application/problem+json"

	service := web.NewService(openapi3.NewReflector())

	service.Wrap(
		gzip.Middleware,
		//enhancedErrorHandler(),
	)

	middlewareSentry := sentryhttp.New(sentryhttp.Options{
		Repanic: true,
	})

	service.Use(
		middleware.RealIP,
		middleware.RequestID,
		middleware.CleanPath,
		middleware.StripSlashes,
		middleware.NoCache,
		middleware.RequestLogger(&httpLoggerFormatter{log}),
		middleware.Recoverer,
		// middleware.Timeout(60 * time.Second),
		middlewareSentry.Handle,
	)
	//service.Use(LogRequestBody)
	return service
}

type EnhancedErrResponse struct {
	error
	Detail   string `json:"detail" description:"Error detail."`
	Type     string `json:"type" description:"Error type."`
	Status   int    `json:"status" description:"Error status." format:"int32"`
	Title    string `json:"title" description:"Error title."`
	Instance string `json:"instance" description:"Error instance."`
}

func enhancedErrorHandler() func(handler http.Handler) http.Handler {
	return func(handler http.Handler) http.Handler {
		var h *nethttp.Handler

		if nethttp.HandlerAs(handler, &h) {
			h.MakeErrResp = func(ctx context.Context, err error) (int, interface{}) {
				code, errResponse := rest.Err(err)

				var enhancedErrResponse EnhancedErrResponse

				if errors.As(err, &enhancedErrResponse) {
					return http.StatusBadRequest, enhancedErrResponse
				}

				return code, EnhancedErrResponse{
					Detail:   errResponse.ErrorText,
					Type:     "https://httpstatuses.io/" + strconv.Itoa(code),
					Status:   code,
					Title:    errResponse.StatusText,
					Instance: "/",
				}
			}

			h.HandleErrResponse = func(w http.ResponseWriter, r *http.Request, err error) {
				code, errResponse := rest.Err(err)
				responseEncoder := response.Encoder{}

				var enhancedErrResponse EnhancedErrResponse
				/* 				if code == 400 {

					fmt.Println("errore 400! " + err.Error())
					fmt.Println("📩 Body salvato:", requestBodyCache)
				} */
				if errors.As(err, &enhancedErrResponse) {
					responseEncoder.WriteErrResponse(w, r, http.StatusBadRequest, enhancedErrResponse)
					return
				}

				enhancedErrResponse = EnhancedErrResponse{
					Detail:   errResponse.ErrorText,
					Type:     "https://httpstatuses.io/" + strconv.Itoa(code),
					Status:   code,
					Title:    errResponse.StatusText,
					Instance: r.RequestURI,
				}

				responseEncoder.WriteErrResponse(w, r, code, enhancedErrResponse)
			}
		}

		return handler
	}
}

func LogRequestBody(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Body == nil {
			fmt.Println("Body assente")
			requestBodyCache = "" // Reset della variabile globale
			next.ServeHTTP(w, r)
			return
		}
		bodyBytes, err := io.ReadAll(r.Body)
		if err != nil {
			fmt.Println("Errore lettura body:", err)
			requestBodyCache = "" // Reset in caso di errore
			next.ServeHTTP(w, r)
			return
		}
		// Salviamo il body nella variabile globale
		requestBodyCache = string(bodyBytes)

		next.ServeHTTP(w, r)
	})
}
