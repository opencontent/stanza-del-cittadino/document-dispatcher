package httpserver

import "opencitylabs.it/documentdispatcher/internal/core/domain"

type DocumentResponse struct {
	Title             string                     `json:"title" description:"Title of the document" required:"true"`
	RegistrySearchKey string                     `json:"registry_search_key" description:"search key into registry provider system" required:"true"`
	ID                string                     `json:"id" description:"identifier of the document" required:"true"`
	Status            string                     `json:"status" description:"status of the document" required:"true"`
	FolderInfo        *domain.FolderInfo         `json:"folder,omitempty" description:"folder document information" `
	RemoteCollection  *RemoteCollectionResponse  `json:"remote_collection,omitempty" description:"ifnormation of entity type related to the document"`
	ShortDescription  string                     `json:"short_description" description:"short description of the document" required:"true"`
	Description       *string                    `json:"description" description:"description of the document" required:"true"`
	MainDocument      domain.File                `json:"main_document" description:"main document" required:"true"`
	Attachments       []domain.File              `json:"attachments,omitempty" description:"attachments of the document" `
	RegistrationInfo  domain.RegistrationInfoDto `json:"registration_info" description:"registration data"`
	TenantID          string                     `json:"tenant_id" description:"Tenant ID" required:"true"`
	OwnerID           string                     `json:"owner_id" description:"Owner ID" required:"true"`
	CreatedAt         string                     `json:"created_at" description:"cration date" required:"true"`
	SourceType        string                     `json:"source_type" description:"Type of source" required:"true"`
	RecipientType     *string                    `json:"recipient_type,omitempty" description:"Type of recipient"`
	Sender            domain.Actor               `json:"sender" description:"Sender details" required:"true"`
	Recipients        []domain.Actor             `json:"recipient" description:"List of recipients" required:"true"`
	Holders           []domain.Actor             `json:"holder" description:"List of holders" required:"true"`
	TransactionID     *string                    `json:"transaction_id" description:"Identifier of the creation document request" required:"true"`
	ExternalId        string                     `json:"external_id" description:"External ID coming from external system" required:"true"`
	ValidFrom         string                     `json:"valid_from" description:"Start date of the document's validity in ISO 8601 format (YYYY-MM-DD)" required:"true"`
	ValidTo           string                     `json:"valid_to" description:"End date of the document's validity in ISO 8601 format (YYYY-MM-DD), indicating until when the document is considered valid" required:"true"`
}

type RemoteCollectionResponse struct {
	EntityId         string `json:"entity_id" description:"Remote collection ID" required:"true"`
	EntityType       string `json:"entity_type" description:"Remote collection type" required:"true"`
	EntityInstanceId string `json:"entity_instance_id" description:"Remote collection isntance id" required:"true"`
}
