package drivers

import (
	"context"
	"errors"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/segmentio/kafka-go"
	"go.uber.org/zap"
	"opencitylabs.it/documentdispatcher/config"
	"opencitylabs.it/documentdispatcher/internal/core/ports"
	"opencitylabs.it/documentdispatcher/logger"
	"opencitylabs.it/documentdispatcher/metrics"
	"opencitylabs.it/documentdispatcher/sentryutils"
	"opencitylabs.it/documentdispatcher/utils"
)

type KafkaConsumer struct {
	KafkaReader *kafka.Reader
	log         *zap.Logger
	config      *config.Config
	service     ports.DispatcherService
	sentryHub   *sentry.Hub
}

func NewKafkaDocumentConsumer(kafkaServer []string, kafkaTopic, consumerGroup string, service ports.DispatcherService) *KafkaConsumer {
	r := &KafkaConsumer{
		log:       logger.GetLogger(),
		config:    config.GetConfig(),
		service:   service,
		sentryHub: sentryutils.InitLocalSentryhub(),
	}
	r.KafkaReader = r.getKafkaReader(kafkaServer, kafkaTopic, consumerGroup)
	return r
}

func (r *KafkaConsumer) getKafkaReader(kafkaServer []string, kafkaTopic, consumerGroup string) *kafka.Reader {
	r.log.Sugar().Debug("Connecting to topic: ", kafkaTopic, " from kafka server ", kafkaServer)
	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers: kafkaServer,
		Topic:   kafkaTopic,
		GroupID: consumerGroup,
		MaxWait: 3 * time.Second,
	})

	return reader
}

func (r *KafkaConsumer) CloseKafkaReader() {
	err := r.KafkaReader.Close()
	if err != nil {
		r.log.Error("Error closing consumer: ", zap.Error(err))
		sentry.CaptureException(err)
		return
	}
	r.log.Debug("Consumer closed")
}

func (r *KafkaConsumer) ProcessMessage(ctx context.Context, config *config.Config) {
	for {
		m, err := r.KafkaReader.FetchMessage(ctx)
		if err != nil {
			r.log.Error("Error reading message from Kafka",
				zap.Error(err),
			)
			sentry.WithScope(func(scope *sentry.Scope) {
				scope.SetTag("key", string(m.Key))
				sentry.CaptureException(errors.New("error reading message from Kafka"))
			})
			metrics.OpsFailedProcessed.WithLabelValues(r.config.App.Environment, "").Inc()
			continue
		}

		application, err := utils.GetApplicationStruct(m.Value)
		if err != nil {
			event, _ := utils.GetEventIDs(m.Value)
			r.log.Info("Error building application struct",
				zap.String("event_id", event.EventID),
				zap.String("id", event.ID),
				zap.Error(err),
			)
			r.log.Error("Error building application struct",
				zap.String("event_id", event.EventID),
				zap.String("id", event.ID),
				zap.Error(err),
			)

			sentry.WithScope(func(scope *sentry.Scope) {
				scope.SetTag("environment", r.config.App.Environment)
				scope.SetTag("tenant_id", application.TenantID)
				scope.SetTag("event_id", event.EventID)
				scope.SetTag("id", event.ID)
				sentry.CaptureException(errors.New("error building application struct"))
			})
			metrics.OpsFailedProcessed.WithLabelValues(r.config.App.Environment, application.TenantID).Inc()
			continue

		}
		eventVersion, err := utils.GetEventVersion(config.App.ApplicationEventVersion)
		if err != nil {
			r.log.Info("Failed to get event version",
				zap.String("environment", r.config.App.Environment),
				zap.String("application_id", application.ID),
				zap.String("tenant_id", application.TenantID),
				zap.Error(err),
			)
		}
		if float32(config.App.ApplicationEventVersion) != eventVersion {
			r.log.Info("Application version not supported",
				zap.String("environment", r.config.App.Environment),
				zap.String("application_id", application.ID),
				zap.String("tenant_id", application.TenantID),
			)
			continue
		}

		r.log.Debug("Processing application",
			zap.String("application_id", application.ID),
		)
		if err = r.service.ProcessMessage(&application); err != nil {
			metrics.OpsFailedProcessed.WithLabelValues(r.config.App.Environment, application.TenantID).Inc()
			continue
		}
		err = r.KafkaReader.CommitMessages(ctx, m)
		if err != nil {
			/* 			r.log.Info("Failed to commit messages",
				zap.String("environment", r.config.App.Environment),
				zap.String("application_id", application.ID),
				zap.String("tenant_id", application.TenantID),
				zap.Error(err),
			) */
			r.log.Error("Failed to commit messages",
				zap.String("environment", r.config.App.Environment),
				zap.String("application_id", application.ID),
				zap.String("tenant_id", application.TenantID),
				zap.Error(err),
			)
			sentry.WithScope(func(scope *sentry.Scope) {
				scope.SetTag("environment", r.config.App.Environment)
				scope.SetTag("application_id", application.ID)
				scope.SetTag("tenant_id", application.TenantID)
				sentry.CaptureException(errors.New("failed to commit messages"))
			})
			metrics.OpsFailedProcessed.WithLabelValues(r.config.App.Environment, application.TenantID).Inc()
		} else {
			/* r.log.Debug("Message committed",
				zap.String("application_id", application.ID),
			)*/
			r.log.Info("Event processed successfully",
				zap.String("environment", r.config.App.Environment),
				zap.String("application_id", application.ID),
				zap.String("tenant_id", application.TenantID),
				zap.String("message", r.service.GetMessage()),
			)
			metrics.OpsSuccessProcessed.WithLabelValues(r.config.App.Environment, application.TenantID).Inc()
		}
	}
}
