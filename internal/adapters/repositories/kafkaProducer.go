package repositories

import (
	"context"
	"encoding/json"

	"github.com/getsentry/sentry-go"
	"github.com/segmentio/kafka-go"
	"go.uber.org/zap"
	"opencitylabs.it/documentdispatcher/config"
	"opencitylabs.it/documentdispatcher/internal/core/domain"
	"opencitylabs.it/documentdispatcher/internal/core/ports"
	"opencitylabs.it/documentdispatcher/logger"
	"opencitylabs.it/documentdispatcher/sentryutils"
	"opencitylabs.it/documentdispatcher/utils"
)

type kafkaProducer struct {
	log         *zap.Logger
	conf        *config.Config
	KafkaWriter *kafka.Writer
	ctx         context.Context
	sentryHub   *sentry.Hub
}

func NewKafkaProducer(ctx context.Context) ports.DocumentProducerRepository {
	r := &kafkaProducer{
		log:       logger.GetLogger(),
		conf:      config.GetConfig(),
		ctx:       ctx,
		sentryHub: sentryutils.InitLocalSentryhub(),
	}
	r.KafkaWriter = r.getKafkaWriter(r.conf.Kafka.KafkaServer, r.conf.Kafka.ProducerTopic, r.conf.Kafka.ConsumerGroup)
	return r
}

func (r *kafkaProducer) ProduceMessage(document domain.Document) error {
	utils.SetEventFieldBeforProduce(&document)
	jsonDoc, err := json.Marshal(document)
	if err != nil {
		return err
	}

	err = r.KafkaWriter.WriteMessages(r.ctx, kafka.Message{
		Key:   []byte(document.RemoteCollection.ID),
		Value: jsonDoc,
	})

	if err != nil {
		r.log.Error("Could not write message",
			zap.String("environment", r.conf.App.Environment),
			zap.String("application_id", document.ID),
			zap.String("tenant_id", document.TenantID),
			zap.Error(err),
		)
		return err
	}

	return nil
}

func (r *kafkaProducer) getKafkaWriter(kafkaServer []string, kafkaTopic, producerGroup string) *kafka.Writer {
	r.log.Debug("Kafka producer: Connecting to topic",
		zap.String("topic", kafkaTopic),
		zap.Strings("kafka_server", kafkaServer),
	)
	transport := &kafka.DefaultTransport
	kwriter := &kafka.Writer{
		Addr:      kafka.TCP(kafkaServer...),
		Topic:     kafkaTopic,
		Balancer:  &kafka.LeastBytes{},
		Transport: *transport,
	}
	return kwriter
}

func (r *kafkaProducer) CloseKafkaWriter() {
	err := r.KafkaWriter.Close()
	if err != nil {

		r.log.Error("Error closing producer",
			zap.Error(err),
		)
		sentry.CaptureException(err)
		return
	}
	r.log.Debug("producer closed")
}
