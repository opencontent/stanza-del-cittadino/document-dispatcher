package repositories

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"strings"

	"github.com/getsentry/sentry-go"
	"go.uber.org/zap"
	"opencitylabs.it/documentdispatcher/config"
	"opencitylabs.it/documentdispatcher/internal/core/domain"
	"opencitylabs.it/documentdispatcher/internal/core/ports"
	"opencitylabs.it/documentdispatcher/logger"
	"opencitylabs.it/documentdispatcher/sentryutils"
)

type ksqlRepository struct {
	log          *zap.Logger
	conf         *config.Config
	ksqlDBServer string
	client       *http.Client
	sentryHub    *sentry.Hub
}

func NewKsqlRepository() ports.DocumentsDataRepository {
	r := &ksqlRepository{
		log:       logger.GetLogger(),
		conf:      config.GetConfig(),
		client:    &http.Client{},
		sentryHub: sentryutils.InitLocalSentryhub(),
	}
	r.ksqlDBServer = r.conf.Kafka.KsqlDb
	return r
}
func (r *ksqlRepository) IsDocumentExists(app *domain.Application, createdAt, docType string) (bool, error) {
	var appId string
	var tenantId string

	if app != nil {
		appId = app.ID
		tenantId = app.TenantID
	}
	query := "select * from " + r.conf.Kafka.KsqlDbTable + " where remote_id ='" + appId + "' and type='" + docType + "' and created_at='" + createdAt + "';"
	queryEndpoint := r.conf.Kafka.KsqlDb + "/query"
	payload := map[string]string{
		"ksql": query,
	}

	payloadBytes, err := json.Marshal(payload)
	if err != nil {
		r.log.Error("Error marshalling payload",
			zap.String("environment", r.conf.App.Environment),
			zap.String("application_id", appId),
			zap.String("tenant_id", tenantId),
			zap.Error(err),
		)
		return true, err

	}
	req, err := http.NewRequest("POST", queryEndpoint, bytes.NewBuffer(payloadBytes))
	if err != nil {

		r.log.Error("Error creating request",
			zap.String("environment", r.conf.App.Environment),
			zap.String("application_id", appId),
			zap.String("tenant_id", tenantId),
			zap.Error(err),
		)
		return true, err

	}

	req.Header.Set("Accept", "application/vnd.ksql.v1+json")
	req.Header.Set("Content-Type", "application/json")

	resp, err := r.client.Do(req)
	if err != nil {

		r.log.Error("Error sending request",
			zap.String("environment", r.conf.App.Environment),
			zap.String("application_id", appId),
			zap.String("tenant_id", tenantId),
			zap.Error(err),
		)
		return true, err

	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		r.log.Debug("Received unexpected response",
			zap.Int("status_code", resp.StatusCode),
			zap.String("application_id", appId),
			zap.String("message", "Failed to check if document already exists"),
		)
		return true, errors.New("Failed to  check if document already exists for application with id: " + appId)
	}

	body, err := io.ReadAll(resp.Body)

	if err != nil {

		sentry.WithScope(func(scope *sentry.Scope) {
			scope.SetTag("environment", r.conf.App.Environment)
			scope.SetTag("application_id", appId)
			scope.SetTag("tenant_id", tenantId)
			sentry.CaptureException(errors.New("error reading HTTP response"))
		})

		r.log.Error("Error reading HTTP response",
			zap.String("environment", r.conf.App.Environment),
			zap.String("application_id", appId),
			zap.String("tenant_id", tenantId),
			zap.Error(err),
		)
		return true, err

	}
	bodyCleaned := r.removeSpacesAndNewlines(body)
	var ksqlResponse KsqlDbResponse

	err = json.Unmarshal(bodyCleaned, &ksqlResponse)
	if err != nil {

		sentry.WithScope(func(scope *sentry.Scope) {
			scope.SetTag("environment", r.conf.App.Environment)
			scope.SetTag("application_id", appId)
			scope.SetTag("tenant_id", tenantId)
			sentry.CaptureException(errors.New("error unmarshalling HTTP response"))
		})

		r.log.Error("Error unmarshalling HTTP response",
			zap.String("environment", r.conf.App.Environment),
			zap.String("application_id", appId),
			zap.String("tenant_id", tenantId),
			zap.Error(err),
		)
		return true, err
	}

	if len(ksqlResponse) > 1 {

		r.log.Debug("Application already processed",
			zap.String("application_id", appId),
		)
		return true, nil
	}

	r.log.Debug("Application not processed yet. Need to create the related document",
		zap.String("application_id", appId),
	)

	return false, nil
}

type KsqlDbResponse []struct {
	Header Header `json:"header,omitempty"`
	//Row    Row    `json:"row,omitempty"`
}

type Header struct {
	QueryID string `json:"queryId"`
	Schema  string `json:"schema"`
}

type Row struct {
	Columns []interface{} `json:"columns"`
}

func (r *ksqlRepository) removeSpacesAndNewlines(jsonData []byte) []byte {
	// Replace all spaces and newlines with an empty string
	cleanedJSON := strings.ReplaceAll(string(jsonData), " ", "")
	cleanedJSON = strings.ReplaceAll(cleanedJSON, "\n", "")

	return []byte(cleanedJSON)
}
