package domain

type Application struct {
	AppID                  string           `json:"app_id"`
	Type                   string           `json:"type"`
	AppVersion             string           `json:"app_version"`
	Attachments            []Attachment     `json:"attachments"`
	Authentication         Authentication   `json:"authentication"`
	BackofficeData         string           `json:"backoffice_data"`
	CompiledModules        []CompiledModule `json:"compiled_modules"`
	CreatedAt              string           `json:"created_at"`
	CreationTime           int              `json:"creation_time"`
	Data                   Data             `json:"data"`
	EventCreatedAt         string           `json:"event_created_at"`
	EventID                string           `json:"event_id"`
	EventVersion           interface{}      `json:"event_version"`
	ExternalID             string           `json:"external_id"`
	FlowChangedAt          string           `json:"flow_changed_at"`
	GeographicAreas        []GeographicArea `json:"geographic_areas"`
	ID                     string           `json:"id" validate:"required"`
	Integrations           []Integration    `json:"integrations"`
	LatestStatusChangeAt   string           `json:"latest_status_change_at"`
	LatestStatusChangeTime int              `json:"latest_status_change_time"`
	//Links                     []Link           `json:"links"`
	Meetings                  []interface{}    `json:"meetings"`
	OperatorID                string           `json:"operator_id"`
	Outcome                   bool             `json:"outcome"`
	OutcomeAttachments        []Attachment     `json:"outcome_attachments"`
	OutcomeFile               OutcomeFile      `json:"outcome_file"`
	OutcomeMotivation         string           `json:"outcome_motivation"`
	OutcomeProtocolDocumentID string           `json:"outcome_protocol_document_id"`
	OutcomeProtocolNumber     string           `json:"outcome_protocol_number"`
	OutcomeProtocolledAt      string           `json:"outcome_protocolled_at"`
	Path                      string           `json:"path"`
	PaymentType               string           `json:"payment_type"`
	ProtocolDocumentID        string           `json:"protocol_document_id"`
	ProtocolFolderCode        string           `json:"protocol_folder_code"`
	ProtocolFolderNumber      string           `json:"protocol_folder_number"`
	ProtocolNumber            string           `json:"protocol_number"`
	ProtocolNumbers           []interface{}    `json:"protocol_numbers"`
	ProtocolledAt             string           `json:"protocolled_at"`
	Service                   string           `json:"service"`
	ServiceGroupName          string           `json:"service_group_name"`
	ServiceID                 string           `json:"service_id"`
	ServiceName               string           `json:"service_name" validate:"required"`
	SourceType                string           `json:"source_type"`
	Status                    string           `json:"status" validate:"required"`
	StatusName                string           `json:"status_name"`
	Subject                   string           `json:"subject" validate:"required"`
	SubmissionTime            int              `json:"submission_time"`
	SubmittedAt               string           `json:"submitted_at"`
	Tenant                    string           `json:"tenant"`
	TenantID                  string           `json:"tenant_id" validate:"required"`
	Timestamp                 string           `json:"timestamp"`
	User                      string           `json:"user"`
	UserCompilationNotes      string           `json:"user_compilation_notes"`
	UserGroupID               string           `json:"user_group_id"`
	UserName                  string           `json:"user_name"`
	XForwardedFor             string           `json:"x-forwarded-for"`
	RegistryMetadata          RegistryMetadata `json:"__registry_metadata"`
}

type OutcomeFile struct {
	CreatedAt        string `json:"created_at"`
	Description      string `json:"description"`
	ExternalID       string `json:"external_id"`
	ID               string `json:"id"`
	Name             string `json:"name"`
	OriginalName     string `json:"originalName"`
	ProtocolNumber   string `json:"protocol_number"`
	ProtocolRequired bool   `json:"protocol_required"`
	URL              string `json:"url"`
}

type Attachment struct {
	ID               string `json:"id"`
	Name             string `json:"name"`
	URL              string `json:"url"`
	OriginalName     string `json:"originalName"`
	Description      string `json:"description"`
	CreatedAt        string `json:"created_at"`
	ProtocolRequired bool   `json:"protocol_required"`
	ProtocolNumber   string `json:"protocol_number"`
	ExternalID       string `json:"external_id"`
}

type GeographicArea struct {
	Name string `json:"name"`
}

type Data struct {
	DiagnosiFunzionale1                                 []DiagnosiFunzionale1 `json:"Diagnosi_funzionale1"`
	DiagnosiFunzionale2                                 []interface{}         `json:"Diagnosi_funzionale2"`
	RichiestaSiNoNo                                     bool                  `json:"RichiestaSiNo.no"`
	RichiestaSiNoSi                                     bool                  `json:"RichiestaSiNo.si"`
	AccettazioneDichiarazioneDiResponsabilta6           bool                  `json:"accettazioneDichiarazioneDiResponsabilta6"`
	AccettazioneDichiarazioneDiResponsabilta7           bool                  `json:"accettazioneDichiarazioneDiResponsabilta7"`
	AnnoProtocolloNumeroProtocollo                      string                `json:"annoProtocolloNumeroProtocollo"`
	AnnoProtocolloNumeroProtocollo1                     string                `json:"annoProtocolloNumeroProtocollo1"`
	AnnoScolastico                                      string                `json:"annoScolastico"`
	ApplicantDataBornDataNatoAIl                        string                `json:"applicant.data.Born.data.natoAIl"`
	ApplicantDataBornDataPlaceOfBirth                   string                `json:"applicant.data.Born.data.place_of_birth"`
	ApplicantDataAddressDataAddress                     string                `json:"applicant.data.address.data.address"`
	ApplicantDataAddressDataAddressDataCodCatastale     string                `json:"applicant.data.address.data.address_data.cod_catastale"`
	ApplicantDataAddressDataAddressDataCodiceComune     int                   `json:"applicant.data.address.data.address_data.codice_comune"`
	ApplicantDataAddressDataAddressDataCodiceNuts2      string                `json:"applicant.data.address.data.address_data.codice_nuts2"`
	ApplicantDataAddressDataAddressDataComune           string                `json:"applicant.data.address.data.address_data.comune"`
	ApplicantDataAddressDataAddressDataEstinto          int                   `json:"applicant.data.address.data.address_data.estinto"`
	ApplicantDataAddressDataAddressDataEstintoDal       string                `json:"applicant.data.address.data.address_data.estinto_dal"`
	ApplicantDataAddressDataAddressDataIDLuogoIstituito string                `json:"applicant.data.address.data.address_data.id_luogo_istituito"`
	ApplicantDataAddressDataAddressDataIstituitoDal     string                `json:"applicant.data.address.data.address_data.istituito_dal"`
	ApplicantDataAddressDataAddressDataLuogoIstat       string                `json:"applicant.data.address.data.address_data.luogo_istat"`
	ApplicantDataAddressDataAddressDataNote             string                `json:"applicant.data.address.data.address_data.note"`
	ApplicantDataAddressDataAddressDataPr               string                `json:"applicant.data.address.data.address_data.pr"`
	ApplicantDataAddressDataAddressDataProvincia        string                `json:"applicant.data.address.data.address_data.provincia"`
	ApplicantDataAddressDataAddressDataRegione          string                `json:"applicant.data.address.data.address_data.regione"`
	ApplicantDataAddressDataAddressDataSlug             string                `json:"applicant.data.address.data.address_data.slug"`
	ApplicantDataAddressDataCodiceCatastale             string                `json:"applicant.data.address.data.codice_catastale"`
	ApplicantDataAddressDataCountry                     string                `json:"applicant.data.address.data.country"`
	ApplicantDataAddressDataCounty                      string                `json:"applicant.data.address.data.county"`
	ApplicantDataAddressDataHouseNumber                 string                `json:"applicant.data.address.data.house_number"`
	ApplicantDataAddressDataMunicipality                string                `json:"applicant.data.address.data.municipality"`
	ApplicantDataAddressDataPostalCode                  string                `json:"applicant.data.address.data.postal_code"`
	ApplicantDataCellNumber                             string                `json:"applicant.data.cell_number"`
	ApplicantDataCompletenameDataName                   string                `json:"applicant.data.completename.data.name"`
	ApplicantDataCompletenameDataSurname                string                `json:"applicant.data.completename.data.surname"`
	ApplicantDataEmailAddress                           string                `json:"applicant.data.email_address"`
	ApplicantDataFiscalCodeDataFiscalCode               string                `json:"applicant.data.fiscal_code.data.fiscal_code"`
	ApplicantDataGenderDataGender                       string                `json:"applicant.data.gender.data.gender"`
	ApplicantDataNumeroDiTelefonoFisso                  string                `json:"applicant.data.numeroDiTelefonoFisso"`
	CodiceMeccanograficoIstituto                        string                `json:"codiceMeccanograficoIstituto"`
	DellistitutoScolastico                              string                `json:"dellistitutoScolastico"`
	EMailItstituto                                      string                `json:"eMailItstituto"`
	EPresenteCertificazioneAggiornata                   bool                  `json:"ePresenteCertificazioneAggiornata"`
	EPresenteCertificazioneAggiornata1                  bool                  `json:"ePresenteCertificazioneAggiornata1"`
	EmailGenitore                                       string                `json:"emailGenitore"`
	EventualiNote1                                      string                `json:"eventualiNote1"`
	EventualiNote2                                      string                `json:"eventualiNote2"`
	Form1DataName                                       string                `json:"form1.data.name"`
	Form1DataSurname                                    string                `json:"form1.data.surname"`
	Form2DataNatoAIl                                    string                `json:"form2.data.natoAIl"`
	Form2DataPlaceOfBirth                               string                `json:"form2.data.place_of_birth"`
	Form3DataFiscalCode                                 string                `json:"form3.data.fiscal_code"`
	FrequentanteListututo                               string                `json:"frequentanteListututo"`
	FrequentanteListututo1                              string                `json:"frequentanteListututo1"`
	FrequentanteListututo2                              string                `json:"frequentanteListututo2"`
	FrequentanteListututo3                              string                `json:"frequentanteListututo3"`
	OreSettimanali                                      string                `json:"oreSettimanali"`
	RecapitoTelefonico                                  string                `json:"recapitoTelefonico"`
	RecapitoTelefonicoIstituto                          string                `json:"recapitoTelefonicoIstituto"`
	ReferenteInsegnanti                                 string                `json:"referenteInsegnanti"`
	Select                                              string                `json:"select"`
	SpecialistaDiRiferimento                            string                `json:"specialistaDiRiferimento"`
	TextField                                           string                `json:"textField"`
	VerbaleSituazioneh1                                 []VerbaleSituazioneh1 `json:"verbale_situazioneh1"`
}

type VerbaleSituazioneh1 struct {
	Description      string `json:"description"`
	ID               string `json:"id"`
	Name             string `json:"name"`
	OriginalName     string `json:"originalName"`
	ProtocolRequired bool   `json:"protocol_required"`
	URL              string `json:"url"`
}

type CompiledModule struct {
	CreatedAt        string `json:"created_at"`
	Description      string `json:"description"`
	ExternalID       string `json:"external_id"`
	ID               string `json:"id"`
	Name             string `json:"name"`
	OriginalName     string `json:"originalName"`
	ProtocolNumber   string `json:"protocol_number"`
	ProtocolRequired bool   `json:"protocol_required"`
	URL              string `json:"url"`
}

type DiagnosiFunzionale1 struct {
	Description      string `json:"description"`
	ID               string `json:"id"`
	Name             string `json:"name"`
	OriginalName     string `json:"originalName"`
	ProtocolRequired bool   `json:"protocol_required"`
	URL              string `json:"url"`
}

type Link struct {
	Action      string `json:"action"`
	Description string `json:"description"`
	URL         string `json:"url"`
}

type Authentication struct {
	AuthenticationMethod string `json:"authentication_method"`
	Certificate          string `json:"certificate"`
	CertificateIssuer    string `json:"certificate_issuer"`
	CertificateSubject   string `json:"certificate_subject"`
	Instant              string `json:"instant"`
	SessionID            string `json:"session_id"`
	SessionIndex         string `json:"session_index"`
	SpidCode             string `json:"spid_code"`
	SpidLevel            string `json:"spid_level"`
}

type RegistryMetadata struct {
	Attempts   int    `json:"attempts"`
	RetryAt    string `json:"retry_at"`
	RetryTopic string `json:"retry_topic"`
}

type Integration struct {
	Inbound  Inbound  `json:"inbound"`
	Outbound Outbound `json:"outbound"`
}

type Outbound struct {
	Attachments      []Attachment `json:"attachments"`
	CreatedAt        string       `json:"created_at"`
	Description      string       `json:"description"`
	ExternalID       string       `json:"external_id"`
	ID               string       `json:"id"`
	Name             string       `json:"name"`
	OriginalName     string       `json:"originalName"`
	ProtocolNumber   interface{}  `json:"protocol_number"`
	ProtocolRequired bool         `json:"protocol_required"`
	URL              string       `json:"url"`
}

type Inbound struct {
	Attachments      []Attachment `json:"attachments"`
	CreatedAt        string       `json:"created_at"`
	Description      string       `json:"description"`
	ExternalID       string       `json:"external_id"`
	ID               string       `json:"id"`
	Name             string       `json:"name"`
	OriginalName     string       `json:"originalName"`
	ProtocolNumber   interface{}  `json:"protocol_number"`
	ProtocolRequired bool         `json:"protocol_required"`
	URL              string       `json:"url"`
}
