package domain

type ActorDto struct {
	Type                    string `json:"type" description:"Type of actor (human or legal)" required:"true"`
	TaxIdentificationNumber string `json:"tax_identification_number" description:"Tax ID number of the actor" required:"true"`
	Name                    string `json:"name" description:"Name of the actor" required:"true"`
	FamilyName              string `json:"family_name" description:"Family Name of the actor" required:"true"`
	StreetName              string `json:"street_name,omitempty" description:"Street name"`
	BuildingNumber          string `json:"building_number,omitempty" description:"Building number"`
	PostalCode              string `json:"postal_code,omitempty" description:"Postal code"`
	TownName                string `json:"town_name,omitempty" description:"Town name"`
	CountrySubdivision      string `json:"country_subdivision,omitempty" description:"Country subdivision"`
	Country                 string `json:"country,omitempty" description:"Country"`
	Email                   string `json:"email" description:"Email address of the actor" required:"true"`
}

type FileDto struct {
	Name        string `json:"name" description:"File name" required:"true"`
	Description string `json:"description" description:"File description" required:"true"`
	MimeType    string `json:"mime_type" description:"MIME type of the file" required:"true"`
	URL         string `json:"url" description:"URL of the file" required:"true"`
	Filename    string `json:"filename" description:"Filename of the uploaded file" required:"true"`
}

type ImageDto struct {
	Name         string `json:"name" description:"Image name" required:"true"`
	Filename     string `json:"filename" description:"Image filename" required:"true"`
	Description  string `json:"description" description:"Image description" required:"true"`
	License      string `json:"license" description:"License of the image" required:"true"`
	Type         string `json:"type" description:"Type of the image" required:"true"`
	URL          string `json:"url" description:"URL of the image" required:"true"`
	Organization string `json:"organization" description:"Organization associated with the image" required:"true"`
}

type RemoteCollectionDto struct {
	EntityId         string `json:"entity_id" description:"Remote collection ID" required:"true"`
	EntityType       string `json:"entity_type" description:"Remote collection type" required:"true"`
	EntityInstanceId string `json:"entity_instance_id" description:"Remote collection isntance id" required:"true"`
}
type FolderData struct {
	ID    string `json:"id" description:"Folder ID"`
	Title string `json:"title" description:"folder title"`
}
type RegistrationInfoDto struct {
	Date           string `json:"date" description:"Date of document registration in ISO 8601 format (YYYY-MM-DD)"`
	DocumentNumber string `json:"document_number" description:"Unique number assigned to the registered document"`
	RegistryCode   string `json:"registry_code" description:"Code identifying the registry where the document was recorded"`
}

type DocumentDto struct {
	Title               string              `json:"title" description:"Title of the document" required:"true"`
	Description         string              `json:"description" description:"Description of the document" required:"true"`
	MainDocument        FileDto             `json:"main_document" description:"Main document file" required:"true"`
	Attachments         []FileDto           `json:"attachments" description:"List of attachment files"`
	ExternalId          string              `json:"external_id" description:"External ID coming from external system" required:"true"`
	ValidFrom           string              `json:"valid_from" description:"Start date of the document's validity in ISO 8601 format (YYYY-MM-DD)" required:"true"`
	ValidTo             string              `json:"valid_to" description:"End date of the document's validity in ISO 8601 format (YYYY-MM-DD), indicating until when the document is considered valid" required:"true"`
	RemoteCollection    RemoteCollectionDto `json:"remote_collection" description:"Details about remote collection" required:"true"`
	TenantID            string              `json:"tenant_id" description:"Tenant ID" required:"true"`
	OwnerID             string              `json:"owner_id" description:"Owner ID" required:"true"`
	SourceType          string              `json:"source_type" description:"Type of source" required:"true"`
	FolderData          FolderData          `json:"folder" description:"folder data"`
	RegistrationInfoDto RegistrationInfoDto `json:"registration_info" description:"registration data"`
	Sender              ActorDto            `json:"sender" description:"Sender details" required:"true"`
	Recipient           []ActorDto          `json:"recipient" description:"List of recipients" required:"true"`
	Holder              []ActorDto          `json:"holder" description:"List of holders" required:"true"`
}
