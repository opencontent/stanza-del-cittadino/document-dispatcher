package domain

type Document struct {
	AppId                 string                 `json:"app_id"`
	EventCreatedAt        string                 `json:"event_created_at"`
	EventId               string                 `json:"event_id"`
	Title                 string                 `json:"title"`
	RegistrySearchKey     string                 `json:"registry_search_key,omitempty"`
	ID                    string                 `json:"id"`
	Event_Version         int                    `json:"event_version"`
	Status                string                 `json:"status"`
	RegistrationData      *RegistrationInfo      `json:"registration_data,omitempty"`
	FolderInfo            *FolderInfo            `json:"folder,omitempty"`
	Type                  *string                `json:"type"`
	RemoteID              *string                `json:"remote_id" validate:"required"`
	ExternalId            *string                `json:"external_id,omitempty"`
	RemoteCollection      *RemoteCollection      `json:"remote_collection,omitempty"`
	Topics                []string               `json:"topics"`
	ShortDescription      string                 `json:"short_description"`
	Description           *string                `json:"description,omitempty"`
	MainDocument          File                   `json:"main_document"`
	ImageGallery          []Image                `json:"image_gallery"`
	HasOrganization       *string                `json:"has_organization,omitempty"`
	Attachments           []File                 `json:"attachments"`
	DistributionLicenseID *string                `json:"distribution_license_id,omitempty"`
	RelatedPublicServices []RelatedPublicService `json:"related_public_services"`
	ValidFrom             *string                `json:"valid_from,omitempty"`
	ValidTo               *string                `json:"valid_to,omitempty"`
	RemovedAt             *string                `json:"removed_at,omitempty"`
	ExpireAt              *string                `json:"expire_at,omitempty"`
	MoreInfo              *string                `json:"more_info,omitempty"`
	NormativeRequirements []string               `json:"normative_requirements"`
	RelatedDocuments      []string               `json:"related_documents"`
	LifeEvents            []string               `json:"life_events"`
	BusinessEvents        []string               `json:"business_events"`
	AllowedReaders        []string               `json:"allowed_readers"`
	TenantID              string                 `json:"tenant_id"`
	OwnerID               string                 `json:"owner_id"`
	DocumentURL           *string                `json:"document_url,omitempty"`
	CreatedAt             string                 `json:"created_at"`
	UpdatedAt             string                 `json:"updated_at"`
	Authors               []Actor                `json:"author"`
	SourceType            string                 `json:"source_type"`
	RecipientType         *string                `json:"recipient_type"`
	LastSeen              *string                `json:"last_seen,omitempty"`
	Sender                Actor                  `json:"sender,omitempty"`
	Recipients            []Actor                `json:"recipient,omitempty"`
	Holders               []Actor                `json:"holder,omitempty"`
	TransactionID         *string                `json:"transaction_id,omitempty"`
}

type RegistrationInfo struct {
	TransmissionType string  `json:"transmission_type"`
	Date             *string `json:"date"`
	DocumentNumber   *string `json:"document_number"`
	RegistryCode     *string `json:"registry_code,omitempty"`
}

type FolderInfo struct {
	Title string  `json:"title" description:"folder document title"`
	ID    *string `json:"id" description:"folder document identifier"`
}

type File struct {
	Name        string  `json:"name" description:"file document name" required:"true"`
	Description *string `json:"description" description:"file document description" required:"true"`
	MimeType    string  `json:"mime_type" description:"file document mime type" required:"true"`
	URL         string  `json:"url" description:"file document url" required:"true"`
	MD5         *string `json:"md5" description:"file document md5" required:"true"`
	Filename    string  `json:"filename" description:"file document file name" required:"true"`
	InternalURL *string `json:"internal_url" description:"file document internal url" required:"true"`
}

type Image struct {
	Name        string `json:"name"`
	Filename    string `json:"filename"`
	Description string `json:"description"`
	License     string `json:"license"`
	Type        string `json:"type"`
	URL         string `json:"url"`
}

type RemoteCollection struct {
	ID   string `json:"id" description:"identifier of entity type related to the document"`
	Type string `json:"type" description:"type of entity type related to the document"`
}

type RelatedPublicService struct {
	ID   string `json:"id"`
	Type string `json:"type"`
	URL  string `json:"url"`
}

type Actor struct {
	Type                    string  `json:"type" description:"Type of actor (human or legal)" required:"true"`
	TaxIdentificationNumber *string `json:"tax_identification_number" description:"Tax ID number of the actor" required:"true"`
	Name                    *string `json:"name" description:"Name of the actor" required:"true"`
	FamilyName              *string `json:"family_name" description:"Family Name of the actor" required:"true"`
	StreetName              *string `json:"street_name" description:"Street name"  required:"true"`
	BuildingNumber          *string `json:"building_number," description:"Building number" required:"true"`
	PostalCode              *string `json:"postal_code" description:"Postal code" required:"true"`
	TownName                *string `json:"town_name" description:"Town name" required:"true"`
	CountrySubdivision      *string `json:"country_subdivision" description:"Country subdivision" required:"true"`
	Country                 *string `json:"country" description:"Country" required:"true"`
	Email                   *string `json:"email" description:"Email address of the actor" required:"true" required:"true"`
	Role                    *string `json:"role" description:"Role of the actor" required:"true"`
}
