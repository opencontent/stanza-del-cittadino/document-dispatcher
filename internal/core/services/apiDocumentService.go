package services

import (
	"errors"
	"strings"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/google/uuid"
	"go.uber.org/zap"
	"opencitylabs.it/documentdispatcher/config"
	"opencitylabs.it/documentdispatcher/internal/core/domain"
	"opencitylabs.it/documentdispatcher/internal/core/ports"
	"opencitylabs.it/documentdispatcher/logger"
	"opencitylabs.it/documentdispatcher/sentryutils"
	"opencitylabs.it/documentdispatcher/utils"
)

type DocumentService struct {
	log          *zap.Logger
	conf         config.Config
	sentryHub    *sentry.Hub
	producerRepo ports.DocumentProducerRepository
	docsDataRepo ports.DocumentsDataRepository
	Dto          domain.DocumentDto
	Document     *domain.Document
	Msg          string
	Err          error
}

func NewDocumentService(producerRepo ports.DocumentProducerRepository, docsDataRepo ports.DocumentsDataRepository) ports.DocumentService {
	return &DocumentService{
		log:          logger.GetLogger(),
		conf:         *config.GetConfig(),
		sentryHub:    sentryutils.InitLocalSentryhub(),
		producerRepo: producerRepo,
		docsDataRepo: docsDataRepo,
	}
}

// GetMessage implements ports.ApiDocumentService.
func (r *DocumentService) GetDocument() *domain.Document {
	if r.Document != nil {
		return r.Document
	}
	return new(domain.Document)
}

// GetMessage implements ports.ApiDocumentService.
func (r *DocumentService) GetMessage() string {
	panic("unimplemented")
}

// ProcessMessage implements ports.ApiDocumentService.
func (r *DocumentService) ProcessMessage(dto *domain.DocumentDto) error {
	r.Document = &domain.Document{}
	r.Dto = *dto
	if !r.BuildDocument() {
		return r.Err
	}
	if r.conf.Kafka.KafkaProducerEnabled {
		err := r.producerRepo.ProduceMessage(*r.Document)
		if err != nil {
			r.Msg = err.Error()
			r.log.Error(r.Msg)
			sentry.CaptureException(errors.New(r.Msg))
			return err
		}
	}
	return nil
}

func (r *DocumentService) BuildDocument() bool {

	dto := r.Dto
	doc := r.Document
	doc.Title = utils.GetMax255String(dto.Title)
	doc.RegistrySearchKey = r.BuildRegistrySearchKey()
	doc.ShortDescription = utils.GetMax255String(dto.Description)
	doc.Description = &dto.Description
	newUuid := uuid.NewString()
	doc.ID = newUuid
	doc.SourceType = dto.SourceType
	newUuid = uuid.NewString()
	doc.TransactionID = &newUuid

	var folderTitle string
	if dto.FolderData.Title != "" {
		folderTitle = dto.FolderData.Title
	} else {
		folderTitle = r.buildFolderTitle()
	}
	newFolderInfo := domain.FolderInfo{Title: folderTitle}
	doc.FolderInfo = &newFolderInfo
	if dto.FolderData.ID != "" {
		doc.FolderInfo.ID = &dto.FolderData.ID
	}
	registrationdata := domain.RegistrationInfo{}
	doc.RegistrationData = &registrationdata
	if dto.RegistrationInfoDto.RegistryCode != "" {
		doc.RegistrationData.RegistryCode = new(string)
		*doc.RegistrationData.RegistryCode = dto.RegistrationInfoDto.RegistryCode
	}
	if dto.RegistrationInfoDto.Date != "" {
		doc.RegistrationData.Date = new(string)
		*doc.RegistrationData.Date = dto.RegistrationInfoDto.Date
	}
	if dto.RegistrationInfoDto.DocumentNumber != "" {
		doc.RegistrationData.DocumentNumber = new(string)
		*doc.RegistrationData.DocumentNumber = dto.RegistrationInfoDto.DocumentNumber
	}
	doc.RegistrationData.TransmissionType = "external"

	doc.Status = "DOCUMENT_CREATED"
	doc.ExternalId = &dto.ExternalId
	doc.ValidFrom = &dto.ValidFrom
	doc.ValidTo = &dto.ValidTo
	doc.RemoteCollection = &domain.RemoteCollection{}
	doc.RemoteCollection.ID = "d66e11e1-6240-44cd-8793-bae7142bc9cc"
	doc.RemoteCollection.Type = dto.RemoteCollection.EntityType
	doc.RemoteID = &dto.RemoteCollection.EntityInstanceId

	if !utils.IsValidUUID(dto.TenantID) {
		r.Err = errors.New("tenant id received in not a valid uuid")
		r.Msg = "tenant id received in not a valid uuid"
		return false
	} else {
		doc.TenantID = dto.TenantID
	}
	doc.OwnerID = dto.OwnerID

	// actors
	doc.Sender = r.MapActorDtoToActor(dto.Sender, "")
	if len(dto.Holder) > 0 {
		for _, holder := range dto.Holder {
			doc.Holders = append(doc.Holders, r.MapActorDtoToActor(holder, ""))
		}
	}

	if len(dto.Recipient) > 0 {
		for _, recipients := range dto.Recipient {
			doc.Recipients = append(doc.Recipients, r.MapActorDtoToActor(recipients, ""))
		}
	}
	if len(dto.Holder) > 0 {
		for _, holder := range dto.Holder {
			doc.Authors = append(doc.Authors, r.MapActorDtoToActor(holder, ""))
		}
	}
	//files
	doc.MainDocument = r.MapFileDtoToFile(dto.MainDocument)

	if len(dto.Attachments) > 0 {

		for _, attachment := range dto.Attachments {
			doc.Attachments = append(doc.Attachments, r.MapFileDtoToFile(attachment))
		}
	}
	currentTime := time.Now()
	doc.CreatedAt = currentTime.Format("2006-01-02T15:04:05-07:00")
	doc.UpdatedAt = doc.CreatedAt
	doc.Event_Version = 1
	return true
}

func (r *DocumentService) BuildRegistrySearchKey() string {
	dto := r.Dto
	registrySearchKey := dto.Title

	if len(dto.Holder) > 0 {
		holder := dto.Holder[0]
		utils.ConcatIfNotPresent(&registrySearchKey, holder.Name)
		utils.ConcatIfNotPresent(&registrySearchKey, holder.FamilyName)
		utils.ConcatIfNotPresent(&registrySearchKey, holder.TaxIdentificationNumber)
	}

	utils.ConcatIfNotPresent(&registrySearchKey, dto.RemoteCollection.EntityInstanceId)

	return registrySearchKey
}

func (r *DocumentService) MapActorDtoToActor(dto domain.ActorDto, role string) domain.Actor {
	return domain.Actor{
		Type: dto.Type,
		TaxIdentificationNumber: func() *string {
			if dto.TaxIdentificationNumber != "" {
				return utils.StringPtr(dto.TaxIdentificationNumber)
			}
			return nil
		}(),
		Name: func() *string {
			if dto.Name != "" {
				return utils.StringPtr(dto.Name)
			}
			return nil
		}(),
		FamilyName: func() *string {
			if dto.FamilyName != "" {
				return utils.StringPtr(dto.FamilyName)
			}
			return nil
		}(),
		StreetName: func() *string {
			if dto.StreetName != "" {
				return utils.StringPtr(dto.StreetName)
			}
			return nil
		}(),
		BuildingNumber: func() *string {
			if dto.BuildingNumber != "" {
				return utils.StringPtr(dto.BuildingNumber)
			}
			return nil
		}(),
		PostalCode: func() *string {
			if dto.PostalCode != "" {
				return utils.StringPtr(dto.PostalCode)
			}
			return nil
		}(),
		TownName: func() *string {
			if dto.TownName != "" {
				return utils.StringPtr(dto.TownName)
			}
			return nil
		}(),
		CountrySubdivision: func() *string {
			if dto.CountrySubdivision != "" {
				return utils.StringPtr(dto.CountrySubdivision)
			}
			return nil
		}(),
		Country: func() *string {
			if dto.Country != "" {
				return utils.StringPtr(dto.Country)
			}
			return nil
		}(),
		Email: func() *string {
			if dto.Email != "" {
				return utils.StringPtr(dto.Email)
			}
			return nil
		}(),
	}
}

func (r *DocumentService) buildFolderTitle() string {
	dto := r.Dto
	folderTitle := dto.Title

	if len(dto.Holder) > 0 {
		holder := dto.Holder[0]
		utils.ConcatIfNotPresent(&folderTitle, holder.Name)
		utils.ConcatIfNotPresent(&folderTitle, holder.FamilyName)
		utils.ConcatIfNotPresent(&folderTitle, holder.TaxIdentificationNumber)
	}
	folderTitle = strings.ReplaceAll(folderTitle, dto.RemoteCollection.EntityInstanceId, "")

	return folderTitle
}

func (r *DocumentService) MapFileDtoToFile(dto domain.FileDto) domain.File {
	return domain.File{
		Name:        dto.Name,
		Description: &dto.Description,
		MimeType:    dto.MimeType,
		URL:         dto.URL,
		Filename:    dto.Filename,
		InternalURL: &dto.URL,
	}
}
