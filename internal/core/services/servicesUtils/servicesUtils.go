package servicesutils

func GetDocType(status string) string {
	switch status {
	case "2000":
		return "application-request"
	case "4100":
		return "integration-request"
	case "4300":
		return "integration-response"
	case "7000", "9000":
		return "application-outcome"
	case "20000":
		return "application-withdraw"
	case "50000":
		return "application-revocation"
	default:
		return ""
	}
}
