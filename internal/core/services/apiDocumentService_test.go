package services

import (
	"testing"

	testutils "opencitylabs.it/documentdispatcher/internal/core/testUtils"
)

func TestBuildRegistrySearchKey(t *testing.T) {
	tests := []struct {
		name           string
		jsonInput      string
		expectedResult string
	}{
		{
			name:           "All fields present",
			jsonInput:      testutils.CORRECT_FULL_DTO,
			expectedResult: "Test Document John Doe 1234567890 EXT12345",
		},
		{
			name:           "Missing Holder",
			jsonInput:      testutils.MISSING_HOLDER_DTO,
			expectedResult: "Test Document EXT12345",
		},
		{
			name:           "Partial Holder Info",
			jsonInput:      testutils.PARTIAL_HOLDER_DTO,
			expectedResult: "Test Document John EXT12345",
		},
		{
			name:           "Multiple Holders (only first considered)",
			jsonInput:      testutils.MULTIPLE_HOLDERS_DTO,
			expectedResult: "Test Document John Doe 123 EXT12345",
		},
		{
			name:           "Title contains full holder info",
			jsonInput:      testutils.TITLE_CONTAINS_HOLDER_INFO_DTO,
			expectedResult: "John Doe 1234567890 EXT12345",
		},
		{
			name:           "Title contains partial holder info",
			jsonInput:      testutils.TITLE_CONTAINS_PARTIAL_INFO_DTO,
			expectedResult: "Test Document John Doe EXT12345",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dto, err := testutils.GetDocumentDtoFromJson(tt.jsonInput)
			if err != nil {
				t.Errorf("expected no error, got: %v", err)
			}

			docService := &DocumentService{
				Dto: *dto,
			}
			result := docService.BuildRegistrySearchKey()
			if result != tt.expectedResult {
				t.Errorf("expected %q, got %q", tt.expectedResult, result)
			}
		})
	}
}
