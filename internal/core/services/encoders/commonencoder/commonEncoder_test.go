package commonencoder

import (
	"testing"

	"opencitylabs.it/documentdispatcher/config"
	"opencitylabs.it/documentdispatcher/internal/core/domain"
	testutils "opencitylabs.it/documentdispatcher/internal/core/testUtils"
	"opencitylabs.it/documentdispatcher/logger"
	"opencitylabs.it/documentdispatcher/utils"
)

//todo

func TestMapFields(t *testing.T) {
	config := config.GetConfig()
	logger := logger.GetLogger()
	defer func() {
		_ = logger.Sync()
	}()
	application, _ := utils.GetApplicationStruct([]byte(testutils.INPUT_APPLICATION_STATUS_2000))
	doc := domain.Document{}
	encoder := commonEncoder{
		log:    logger,
		config: config,
	}

	_ = encoder.mapFields(&application, &doc)

	if doc.Title != "Richiesta: Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024 Coccorita Coccorico CCCCCC73C50F158S 22222222-0e5d-4a14-8de1-7f423571fb3e" {
		t.Fatal("expecting \"Richiesta: Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024 Coccorita Coccorico CCCCCC73C50F158S  22222222-0e5d-4a14-8de1-7f423571fb3e\" but got: ", doc.Title)
	}

	if doc.FolderInfo == nil {
		t.Fatal("expeting folderInfo not empty")
	}
	if doc.FolderInfo.Title != "Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024 - Coccorita Coccorico CCCCCC73C50F158S" {
		t.Fatal("expected \"Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024 - Coccorita Coccorico CCCCCC73C50F158S\" but got: ", doc.FolderInfo.Title)
	}
	if doc.Authors != nil {
		cf := "XXXXXX89H28G942I"
		if doc.Authors[0].TaxIdentificationNumber != &cf {
			t.Fatal("expected \"XXXXXX89H28G942I\" but got: ", doc.Authors[0].TaxIdentificationNumber)
		}
		name := "xx Maria"
		if doc.Authors[0].Name != &name {
			t.Fatal("expected \"xx Maria\" but got: ", doc.Authors[0].Name)
		}
		if doc.Authors[0].Type != "human" {
			t.Fatal("expected \"human\" but got: ", doc.Authors[0].Type)
		}
	}
	if doc.RemoteCollection != nil {
		if doc.RemoteCollection.ID != "f542da62-9796-419c-bcc6-d309c3fdeb4a" {
			t.Fatal("expected \"f542da62-9796-419c-bcc6-d309c3fdeb4a\" but got: ", doc.RemoteCollection.ID)
		}
	}

}

func TestGetSourceTypeAndRecipientType(t *testing.T) {
	config := config.GetConfig()
	logger := logger.GetLogger()
	defer func() {
		_ = logger.Sync()
	}()
	encoder := commonEncoder{
		log:    logger,
		config: config,
	}
	receivedSourceType, receivedRecipientType := encoder.getSourceTypeAndRecipientType("2000")
	if receivedSourceType != "user" {
		t.Fatal("epected \"user\" but got:", receivedSourceType)
	}
	if receivedRecipientType != "tenant" {
		t.Fatal("epected \"tenant\" but got:", receivedRecipientType)
	}
	receivedSourceType, receivedRecipientType = encoder.getSourceTypeAndRecipientType("4300")
	if receivedSourceType != "user" {
		t.Fatal("epected \"user\" but got:", receivedSourceType)
	}
	if receivedRecipientType != "tenant" {
		t.Fatal("epected \"tenant\" but got:", receivedRecipientType)
	}
	receivedSourceType, receivedRecipientType = encoder.getSourceTypeAndRecipientType("50000")
	if receivedSourceType != "tenant" {
		t.Fatal("epected \"user\" but got:", receivedSourceType)
	}
	if receivedRecipientType != "user" {
		t.Fatal("epected \"tenant\" but got:", receivedRecipientType)
	}
	receivedSourceType, receivedRecipientType = encoder.getSourceTypeAndRecipientType("4100")
	if receivedSourceType != "tenant" {
		t.Fatal("epected \"tenant\" but got:", receivedSourceType)
	}
	if receivedRecipientType != "user" {
		t.Fatal("epected \"user\" but got:", receivedRecipientType)
	}
	receivedSourceType, receivedRecipientType = encoder.getSourceTypeAndRecipientType("7000")
	if receivedSourceType != "tenant" {
		t.Fatal("epected \"tenant\" but got:", receivedSourceType)
	}
	if receivedRecipientType != "user" {
		t.Fatal("epected \"user\" but got:", receivedRecipientType)
	}
	receivedSourceType, receivedRecipientType = encoder.getSourceTypeAndRecipientType("9000")
	if receivedSourceType != "tenant" {
		t.Fatal("epected \"tenant\" but got:", receivedSourceType)
	}
	if receivedRecipientType != "user" {
		t.Fatal("epected \"user\" but got:", receivedRecipientType)
	}
	receivedSourceType, receivedRecipientType = encoder.getSourceTypeAndRecipientType("20000")
	if receivedSourceType != "user" {
		t.Fatal("epected \"user\" but got:", receivedSourceType)
	}
	if receivedRecipientType != "tenant" {
		t.Fatal("epected \"tenant\" but got:", receivedRecipientType)
	}
}

func TestConcatIfNotPresent(t *testing.T) {
	config := config.GetConfig()
	logger := logger.GetLogger()
	defer func() {
		_ = logger.Sync()
	}()
	application, _ := utils.GetApplicationStruct([]byte(testutils.INPUT_APPLICATION_STATUS_2000))
	encoder := commonEncoder{
		log:    logger,
		config: config,
	}
	expetedTitleOne := "Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024 Coccorita Coccorico CCCCCC73C50F158S 22222222-0e5d-4a14-8de1-7f423571fb3e"
	subject := encoder.processSubject(&application)
	if subject != expetedTitleOne {
		t.Fatal("test 1: expected "+expetedTitleOne+"\n but got: ", subject)
	}
	application.Subject = "Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024"
	subject = encoder.processSubject(&application)
	if subject != expetedTitleOne {
		t.Fatal("test 2: expected "+expetedTitleOne+"\n but got: ", subject)
	}

	expetedTitleTwo := "Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024 Coccorita Coccorico 22222222-0e5d-4a14-8de1-7f423571fb3e CCCCCC73C50F158S"
	application.Subject = "Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024 Coccorita Coccorico 22222222-0e5d-4a14-8de1-7f423571fb3e"
	subject = encoder.processSubject(&application)
	if subject != expetedTitleTwo {
		t.Fatal("test 3: expected "+expetedTitleTwo+"\n but got: ", subject)
	}

	expetedTitleThree := "Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024 CCCCCC73C50F158S 22222222-0e5d-4a14-8de1-7f423571fb3e Coccorita Coccorico"
	application.Subject = "Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024 CCCCCC73C50F158S 22222222-0e5d-4a14-8de1-7f423571fb3e"
	subject = encoder.processSubject(&application)
	if subject != expetedTitleThree {
		t.Fatal("test 3: expected "+expetedTitleThree+"\n but got: ", subject)
	}

	expetedTitleFour := "Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024 CCCCCC73C50F158S Coccorita Coccorico 22222222-0e5d-4a14-8de1-7f423571fb3e"
	application.Subject = "Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024 CCCCCC73C50F158S"
	subject = encoder.processSubject(&application)
	if subject != expetedTitleFour {
		t.Fatal("test 3: expected "+expetedTitleFour+"\n but got: ", subject)
	}
}
