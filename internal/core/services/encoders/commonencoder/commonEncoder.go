package commonencoder

import (
	"errors"

	"github.com/google/uuid"
	"go.uber.org/zap"
	"opencitylabs.it/documentdispatcher/config"
	"opencitylabs.it/documentdispatcher/internal/core/domain"
	services "opencitylabs.it/documentdispatcher/internal/core/services/encoders"
	servicesutils "opencitylabs.it/documentdispatcher/internal/core/services/servicesUtils"
	"opencitylabs.it/documentdispatcher/logger"
	"opencitylabs.it/documentdispatcher/utils"
)

type commonEncoder struct {
	log    *zap.Logger
	config *config.Config
}

func NewCommonEncoder() services.CommonEncoder {
	return &commonEncoder{
		log:    logger.GetLogger(),
		config: config.GetConfig(),
	}
}

func (r *commonEncoder) EncodeCommonFields(app *domain.Application, doc *domain.Document) error {
	err := r.mapFields(app, doc)
	return err
}

func (r *commonEncoder) mapFields(app *domain.Application, doc *domain.Document) error {

	//title
	rawSubject := app.Subject
	if rawSubject == "" {
		return errors.New("document title can't be null. Application subject is empty")
	}
	subject := r.processSubject(app)
	title := r.getTitlePrefix(app.Status) + subject
	doc.Title = utils.GetMax255String(title)
	//short description
	doc.ShortDescription = utils.GetMax255String(subject)
	//description
	doc.Description = &subject

	//ID
	newUuid := uuid.NewString()
	doc.ID = newUuid

	//version
	doc.Event_Version = 1

	//folder title
	folderTitle := r.buildFolderTitle(app)
	if folderTitle == "" {
		return errors.New("folder.title can't be empty")
	}
	if doc.FolderInfo != nil {
		doc.FolderInfo.Title = folderTitle
	} else {
		newFolderInfo := domain.FolderInfo{Title: folderTitle}
		doc.FolderInfo = &newFolderInfo
	}

	//type
	doctype := servicesutils.GetDocType(app.Status)
	doc.Type = &doctype
	doc.Status = "DOCUMENT_CREATED"
	//remote id todo valutare se deve essere required
	remoteId := app.ID
	if remoteId != "" && utils.IsValidUUID(remoteId) {
		doc.RemoteID = &remoteId
	} else {
		return errors.New("document.remote_id can't be empty")
	}
	//remote collection
	remotecollectionId := app.ServiceID
	if remotecollectionId != "" && utils.IsValidUUID(remotecollectionId) && doc.RemoteCollection != nil {
		doc.RemoteCollection.Type = "service" //todo chiedere come calcolarlo. temporaneamente hardocded
		doc.RemoteCollection.ID = remotecollectionId
	} else {
		newRemoteCollection := domain.RemoteCollection{Type: "service", ID: remotecollectionId}
		doc.RemoteCollection = &newRemoteCollection
	}

	//tenant Id
	if !utils.IsValidUUID(app.TenantID) {
		return errors.New("application tenant id is not a valid UUIDv4")
	}
	//owners id
	if app.User == "" {
		return errors.New("application user can't be null")
	}
	doc.OwnerID = app.User
	//tenant id
	doc.TenantID = app.TenantID
	//created_at
	//currentTime := time.Now()
	//doc.CreatedAt = currentTime.Format("2006-01-02T15:04:05-07:00")

	//author
	r.mapAuthor(app, doc)
	//source_type, recipient_type
	sourceType, recipientType := r.getSourceTypeAndRecipientType(app.Status)
	doc.SourceType = sourceType
	if recipientType != "" {
		doc.RecipientType = &recipientType
	}

	registrationdata := domain.RegistrationInfo{}
	doc.RegistrationData = &registrationdata
	doc.RegistrationData.TransmissionType = r.setTrasmissionType(doc)

	return nil

}

func (r *commonEncoder) buildFolderTitle(app *domain.Application) string {

	title := app.ServiceName + " - " + app.Data.ApplicantDataCompletenameDataName + " " + app.Data.ApplicantDataCompletenameDataSurname + " " + app.Data.ApplicantDataFiscalCodeDataFiscalCode

	if len(title) > 255 {

		return utils.TruncateStringWithThreeDots(251, title)
	}

	return title
}

func (r *commonEncoder) mapAuthor(app *domain.Application, doc *domain.Document) {
	appData := app.Data
	newAuthor := domain.Actor{
		Type: "human",
	}
	if appData.ApplicantDataFiscalCodeDataFiscalCode != "" {
		newAuthor.TaxIdentificationNumber = &appData.ApplicantDataFiscalCodeDataFiscalCode
	} else {
		cf := "anonymous"
		newAuthor.TaxIdentificationNumber = &cf
	}
	if appData.ApplicantDataCompletenameDataName != "" {
		newAuthor.Name = &appData.ApplicantDataCompletenameDataName
	}
	if appData.ApplicantDataCompletenameDataSurname != "" {
		newAuthor.FamilyName = &app.Data.ApplicantDataCompletenameDataSurname
	}
	if appData.ApplicantDataAddressDataAddress != "" {
		newAuthor.StreetName = &app.Data.ApplicantDataAddressDataAddress
	}
	if appData.ApplicantDataAddressDataHouseNumber != "" {
		newAuthor.BuildingNumber = &app.Data.ApplicantDataAddressDataHouseNumber
	}
	if appData.ApplicantDataAddressDataPostalCode != "" {
		newAuthor.PostalCode = &app.Data.ApplicantDataAddressDataPostalCode
	}
	if appData.ApplicantDataAddressDataAddressDataComune != "" {
		newAuthor.TownName = &app.Data.ApplicantDataAddressDataAddressDataComune
	}
	if appData.ApplicantDataAddressDataAddressDataProvincia != "" {
		newAuthor.CountrySubdivision = &app.Data.ApplicantDataAddressDataAddressDataProvincia
	}
	if appData.ApplicantDataAddressDataCountry != "" {
		newAuthor.Country = &app.Data.ApplicantDataAddressDataCountry
	}
	if appData.ApplicantDataEmailAddress != "" {
		newAuthor.Email = &appData.ApplicantDataEmailAddress
	}
	role := r.getAuthorRole(app.Status)
	newAuthor.Role = &role
	doc.Authors = append(doc.Authors, newAuthor)
	if len(doc.Authors) == 0 {
		doc.Authors = []domain.Actor{}
	}
}

func (r *commonEncoder) getAuthorRole(status string) string {
	switch status {
	case "2000":
		return "sender"
	case "4100":
		return "receiver"
	case "4300":
		return "sender"
	case "7000", "9000":
		return "receiver"
	case "20000":
		return "sender"
	case "50000":
		return "receiver"
	}
	return ""
}

func (r *commonEncoder) getSourceTypeAndRecipientType(status string) (string, string) {
	switch status {
	case "2000", "4300", "20000":
		sourceType := "user"
		recipient_type := "tenant"
		return sourceType, recipient_type
	case "4100", "7000", "9000", "50000":
		sourceType := "tenant"
		recipient_type := "user"
		return sourceType, recipient_type
	default:
		return "", ""
	}
}

func (r *commonEncoder) getTitlePrefix(status string) string {
	switch status {
	case "2000":
		return "Richiesta: "
	case "4100":
		return "Richiesta integrazione: "
	case "4300":
		return "Risposta integrazione: "
	case "7000":
		return "Accettazione: "
	case "9000":
		return "Rifiuto: "
	case "20000":
		return "Ritiro: "
	case "50000":
		return "Annullamento: "
	default:
		return ""
	}
}

func (r *commonEncoder) setTrasmissionType(doc *domain.Document) string {
	if doc.SourceType == "tenant" && doc.RecipientType != nil && *doc.RecipientType == "internal" {
		return "Internal"
	} else if doc.SourceType == "tenant" && doc.RecipientType != nil && *doc.RecipientType == "user" {
		return "Outbound"
	} else if doc.SourceType == "user" && doc.RecipientType != nil && *doc.RecipientType == "tenant" {
		return "Inbound"
	}
	return ""
}

func (r *commonEncoder) processSubject(app *domain.Application) string {
	subject := app.Subject
	data := app.Data
	nameSurname := data.ApplicantDataCompletenameDataName + " " + data.ApplicantDataCompletenameDataSurname
	if nameSurname != "" {
		utils.ConcatIfNotPresent(&subject, nameSurname)
	}

	fiscalCode := data.ApplicantDataFiscalCodeDataFiscalCode
	if fiscalCode != "" {
		utils.ConcatIfNotPresent(&subject, fiscalCode)
	}

	appId := app.ID
	if appId != "" {
		utils.ConcatIfNotPresent(&subject, appId)
	}

	return subject
}
