package encoderutils

import (
	"testing"

	testutils "opencitylabs.it/documentdispatcher/internal/core/testUtils"
	"opencitylabs.it/documentdispatcher/logger"
	"opencitylabs.it/documentdispatcher/utils"
)

func TestIsIntegrationsEmpty(t *testing.T) {
	logger := logger.GetLogger()
	defer func() {
		_ = logger.Sync()
	}()
	application, _ := utils.GetApplicationStruct([]byte(testutils.INPUT_APPLICATION_STATUS_4100))

	if result := IsIntegrationsEmpty(&application); result != false {
		t.Fatal("Expected true, received: ", result)
	}

	application, _ = utils.GetApplicationStruct([]byte(testutils.INPUT_APPLICATION_STATUS_2000))

	if result := IsIntegrationsEmpty(&application); result != true {
		t.Fatal("Expected true, received: ", result)
	}
}

func TestGetLatestModule(t *testing.T) {
	logger := logger.GetLogger()
	defer func() {
		_ = logger.Sync()
	}()
	application, _ := utils.GetApplicationStruct([]byte(testutils.INPUT_APPLICATION_WITHOUT_COMPAILED_MODULES))

	_, err := GetLatestModule(&application)
	if err == nil {
		t.Fatal("Expected error, received nil: ", err)
	}
}

func TestGetLatestModuleTwo(t *testing.T) {
	logger := logger.GetLogger()
	defer func() {
		_ = logger.Sync()
	}()
	application, _ := utils.GetApplicationStruct([]byte(testutils.INPUT_APPLICATION_STATUS_2000))

	lastModule, err := GetLatestModule(&application)
	if err != nil {
		t.Fatal("unexpetcted error: ", err)
	}
	if lastModule.CreatedAt != "2023-07-15T13:31:27+02:00" {
		t.Fatal("Expected 2023-07-15T13:31:27+02:00, received: ", lastModule.CreatedAt)
	}

}

func TestGetLatestIntegrationOutbound(t *testing.T) {
	logger := logger.GetLogger()
	defer func() {
		_ = logger.Sync()
	}()
	application, _ := utils.GetApplicationStruct([]byte(testutils.INPUT_APPLICATION_STATUS_4100))

	lastOutbound, err := GetLatestIntegrationOutbound(&application)
	if err != nil {
		t.Fatal("unexpetcted error: ", err)
	}
	if lastOutbound.CreatedAt != "2023-06-06T11:30:53+02:00" {
		t.Fatal("Expected 2023-06-06T11:30:53+02:00, received: ", lastOutbound.CreatedAt)
	}

}

func TestGetLatestIntegrationInbound(t *testing.T) {
	logger := logger.GetLogger()
	defer func() {
		_ = logger.Sync()
	}()
	application, _ := utils.GetApplicationStruct([]byte(testutils.INPUT_APPLICATION_STATUS_4300))

	lastInbound, err := GetLatestIntegrationInbound(&application)
	if err != nil {
		t.Fatal("unexpetcted error: ", err)
	}
	if lastInbound.CreatedAt != "2023-06-11T13:50:54+02:00" {
		t.Fatal("Expected 2023-06-11T13:50:54+02:00, received: ", lastInbound.CreatedAt)
	}

}
