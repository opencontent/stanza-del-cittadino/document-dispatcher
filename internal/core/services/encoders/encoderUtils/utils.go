package encoderutils

import (
	"errors"
	"time"

	"opencitylabs.it/documentdispatcher/internal/core/domain"
)

const DATA_TIME_LAYOUT = "2006-01-02T15:04:05-07:00"

func IsOutcomeFileEmpty(app *domain.Application) bool {
	return app.OutcomeFile == domain.OutcomeFile{}
}

func IsIntegrationsEmpty(app *domain.Application) bool {
	return len(app.Integrations) == 0
}

func GetLatestModule(app *domain.Application) (*domain.CompiledModule, error) {
	if len(app.CompiledModules) == 0 {
		return nil, errors.New("received empty compileModules array")
	}
	latestDate, err := time.Parse(DATA_TIME_LAYOUT, app.CompiledModules[0].CreatedAt)
	if err != nil {
		return nil, err
	}
	latestIndex := 0

	for index, compiledModule := range app.CompiledModules {
		date, err := time.Parse(DATA_TIME_LAYOUT, compiledModule.CreatedAt)
		if err != nil {
			return nil, err
		}
		if latestDate.Before(date) {
			latestDate = date
			latestIndex = index
		}
	}
	return &app.CompiledModules[latestIndex], nil
}

func GetLatestIntegrationOutbound(app *domain.Application) (*domain.Outbound, error) {
	if len(app.Integrations) == 0 {
		return nil, errors.New("received empty integrations array")
	}
	if !HasPopulatedOutbound(app.Integrations) {
		return nil, errors.New("no populated  integrations outbound found")
	}
	latestDate, err := time.Parse(DATA_TIME_LAYOUT, app.Integrations[0].Outbound.CreatedAt)
	if err != nil {
		return nil, err
	}
	latestIndex := 0

	for index, integration := range app.Integrations {
		date, err := time.Parse(DATA_TIME_LAYOUT, integration.Outbound.CreatedAt)
		if err != nil {
			return nil, err
		}

		if latestDate.Before(date) {
			latestDate = date
			latestIndex = index
		}
	}
	return &app.Integrations[latestIndex].Outbound, nil
}

func GetLatestIntegrationInbound(app *domain.Application) (*domain.Inbound, error) {
	if len(app.Integrations) == 0 {
		return nil, errors.New("received empty integrations array")
	}
	if !HasPopulatedInbound(app.Integrations) {
		return nil, errors.New("no populated integrations inbound found")
	}
	latestDate, err := time.Parse(DATA_TIME_LAYOUT, app.Integrations[0].Inbound.CreatedAt)
	if err != nil {
		return nil, err
	}
	latestIndex := 0

	for index, integration := range app.Integrations {
		if integration.Inbound.ID == "" {
			break
		}
		date, err := time.Parse(DATA_TIME_LAYOUT, integration.Inbound.CreatedAt)
		if err != nil {
			return nil, err
		}
		if latestDate.Before(date) {
			latestDate = date
			latestIndex = index
		}
	}
	return &app.Integrations[latestIndex].Inbound, nil
}

func GetLatestAttachment(app *domain.Application) (*domain.Attachment, error) {
	if len(app.Attachments) == 0 {
		return nil, errors.New("received empty attachment array")
	}
	latestDate, err := time.Parse(DATA_TIME_LAYOUT, app.Attachments[0].CreatedAt)
	if err != nil {
		return nil, err
	}
	latestIndex := 0

	for index, attachment := range app.Attachments {

		date, err := time.Parse(DATA_TIME_LAYOUT, attachment.CreatedAt)
		if err != nil {
			return nil, err
		}

		if latestDate.Before(date) {
			latestDate = date
			latestIndex = index
		}
	}
	return &app.Attachments[latestIndex], nil
}

func HasPopulatedOutbound(integrations []domain.Integration) bool {
	for _, integration := range integrations {
		if IsOutboundPopulated(integration.Outbound) {
			return true
		}
	}
	return false
}

func IsOutboundPopulated(outbound domain.Outbound) bool {
	return outbound.ID != "" ||
		outbound.URL != "" ||
		outbound.Name != "" ||
		outbound.Description != "" ||
		outbound.OriginalName != "" ||
		outbound.CreatedAt != ""
}

func HasPopulatedInbound(integrations []domain.Integration) bool {
	for _, integration := range integrations {
		if IsInboundPopulated(integration.Inbound) {
			return true
		}
	}
	return false
}

func IsInboundPopulated(inbound domain.Inbound) bool {
	return inbound.ID != "" ||
		inbound.URL != "" ||
		inbound.Name != "" ||
		inbound.Description != "" ||
		inbound.OriginalName != "" ||
		inbound.CreatedAt != ""
}

func HasPopulatedAttachment(attachments []domain.Attachment) bool {
	for _, attachment := range attachments {
		if IsAttachmentPopulated(attachment) {
			return true
		}
	}
	return false
}

func IsAttachmentPopulated(attachment domain.Attachment) bool {
	return attachment.ID != "" ||
		attachment.URL != "" ||
		attachment.Name != "" ||
		attachment.Description != "" ||
		attachment.OriginalName != "" ||
		attachment.CreatedAt != ""
}
