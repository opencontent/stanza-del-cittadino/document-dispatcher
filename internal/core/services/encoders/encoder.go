package encoders

import (
	"go.uber.org/zap"
	"opencitylabs.it/documentdispatcher/internal/core/domain"
	"opencitylabs.it/documentdispatcher/logger"
)

type encoder struct {
	log      *zap.Logger
	cEncoder CommonEncoder
	sEncoder SpecificEncoder
}

func NewEncoder(c CommonEncoder, s SpecificEncoder) Encoder {
	return &encoder{
		log:      logger.GetLogger(),
		cEncoder: c,
		sEncoder: s,
	}
}

func (r *encoder) EncodeDocument(app *domain.Application, doc *domain.Document) error {
	err := r.sEncoder.EncodeSpecificFields(app, doc)
	if err != nil {
		return err
	}
	err = r.cEncoder.EncodeCommonFields(app, doc)
	if err != nil {
		return err
	}
	return nil
}
