package specificencoders

import (
	"testing"

	"opencitylabs.it/documentdispatcher/internal/core/domain"
	encoderutils "opencitylabs.it/documentdispatcher/internal/core/services/encoders/encoderUtils"
	testutils "opencitylabs.it/documentdispatcher/internal/core/testUtils"
	"opencitylabs.it/documentdispatcher/logger"
	"opencitylabs.it/documentdispatcher/utils"
)

// todo rinominare
func TestEncodeAttachmentsTwo20000(t *testing.T) {

	logger := logger.GetLogger()
	defer func() {
		_ = logger.Sync()
	}()
	_, err := utils.GetApplicationStruct([]byte(testutils.INPUT_APPLICATION_STATUS_20000))
	if err != nil {
		t.Fatal("error GetApplicationStruct document: ", err)
	}
}

func TestMapMainDocumentWithdraw(t *testing.T) {
	logger := logger.GetLogger()
	defer func() {
		_ = logger.Sync()
	}()
	application, _ := utils.GetApplicationStruct([]byte(testutils.INPUT_APPLICATION_STATUS_20000))
	latestAttachment, _ := encoderutils.GetLatestAttachment(&application)
	document := domain.Document{}
	encoder := encoderrApplicationWithdraw{
		log: logger,
	}
	err := encoder.mapMainDocument(latestAttachment, &document)
	if err != nil {
		t.Fatal("error mappingMain document: ", err)
	}

	if document.MainDocument.Name != "ritiro-pratica-richiesta-del-servizio3.pdf" {
		t.Fatal("expected \"ritiro-pratica-richiesta-del-servizio3.pdf\" but got: ", document.MainDocument.Name)
	}

	if document.MainDocument.Filename != "ritiro-pratica-richiesta-del-servizio3.pdf" {
		t.Fatal("expected \"ritiro-pratica-richiesta-del-servizio3.pdf\" but got: ", document.MainDocument.Filename)
	}

	if *document.MainDocument.Description != "Ritiro pratica Richiesta del servizio3" {
		t.Fatal("expected \"Ritiro pratica Richiesta del servizio3\" but got:", *document.MainDocument.Description)
	}

	if document.MainDocument.MimeType != "application/pdf" {
		t.Fatal("expected \"application/pdf\" but got: ", document.MainDocument.MimeType)
	}
	if document.MainDocument.URL != "https://www2.test.it/3" {
		t.Fatal("expected \"https://www2.test.it/3\" but got: ", document.MainDocument.URL)
	}
}
