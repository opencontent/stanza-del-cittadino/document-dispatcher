package specificencoders

import (
	"errors"

	"go.uber.org/zap"
	"opencitylabs.it/documentdispatcher/internal/core/domain"
	"opencitylabs.it/documentdispatcher/internal/core/services/encoders"
	encoderutils "opencitylabs.it/documentdispatcher/internal/core/services/encoders/encoderUtils"
	"opencitylabs.it/documentdispatcher/logger"
	"opencitylabs.it/documentdispatcher/utils"
)

type encoderApplicationOutcome struct {
	log *zap.Logger
}

func NewEncoderApplicationOutcome() encoders.SpecificEncoder {
	return &encoderApplicationOutcome{
		log: logger.GetLogger(),
	}
}

// doc is both input and output
func (r *encoderApplicationOutcome) EncodeSpecificFields(app *domain.Application, doc *domain.Document) error {
	r.log.Debug("encoderApplicationOutcome")
	err := r.encodeMainDocument(app, doc)
	if err != nil {
		return err
	}
	err = r.encodeAttachments(app, doc)
	if err != nil {
		return err
	}
	err = r.encodeOthersFields(app, doc)
	if err != nil {
		return err
	}
	return nil
}

func (r *encoderApplicationOutcome) encodeMainDocument(app *domain.Application, doc *domain.Document) error {
	if encoderutils.IsOutcomeFileEmpty(app) {
		return nil
	}
	err := r.mapMainDocument(&app.OutcomeFile, doc)
	if err != nil {
		return err
	}

	return nil
}

func (r *encoderApplicationOutcome) mapMainDocument(outcomeFile *domain.OutcomeFile, doc *domain.Document) error {
	var emptyOutcomeFileStrcut *domain.OutcomeFile
	if outcomeFile == emptyOutcomeFileStrcut {
		return nil
	}
	if !outcomeFile.ProtocolRequired {
		return nil
	}

	logger := logger.GetLogger()
	defer func() {
		_ = logger.Sync()
	}()

	maindoc := &doc.MainDocument

	if outcomeFile.OriginalName == "" {
		return errors.New("outcomeFile.OriginalName cannot be empty")
	}
	maindoc.Name = outcomeFile.OriginalName

	if outcomeFile.Description == "" {
		return errors.New(" outcomeFile.Description cannot be empty")
	}
	maindoc.Description = &outcomeFile.Description

	mimetype, err := utils.GetMIMEtypeFromFileName(outcomeFile.OriginalName)
	if err != nil {
		return err
	}
	maindoc.MimeType = mimetype

	if len(outcomeFile.URL) == 0 {
		return errors.New(" outcomeFile.Url cannot be empty")
	}
	maindoc.URL = outcomeFile.URL

	if outcomeFile.Name == "" {
		return errors.New(" outcomeFile.Name cannot be empty")
	}
	doc.MainDocument.Filename = outcomeFile.Name

	return nil
}

func (r *encoderApplicationOutcome) encodeAttachments(app *domain.Application, doc *domain.Document) error {
	for _, attachment := range app.OutcomeAttachments {
		if !attachment.ProtocolRequired {
			continue
		}
		docAttachment := domain.File{}

		if attachment.OriginalName == "" {
			return errors.New("OutcomeAttachments.OriginalName cannot be empty")
		}
		docAttachment.Name = attachment.OriginalName

		if attachment.Description == "" {
			return errors.New("OutcomeAttachments.Description cannot be empty")
		}
		description := attachment.Description
		docAttachment.Description = &description

		mimetype, err := utils.GetMIMEtypeFromFileName(attachment.OriginalName)
		if err != nil {
			return err
		}
		docAttachment.MimeType = mimetype
		if len(attachment.URL) == 0 {
			return errors.New("OutcomeAttachments.Url cannot be empty")
		}
		docAttachment.URL = attachment.URL

		if attachment.Name == "" {
			return errors.New("OutcomeAttachments.Name cannot be empty")
		}
		docAttachment.Filename = attachment.OriginalName

		doc.Attachments = append(doc.Attachments, docAttachment)

	}
	if len(doc.Attachments) == 0 {
		doc.Attachments = []domain.File{}
	}
	return nil
}

func (r *encoderApplicationOutcome) encodeOthersFields(app *domain.Application, doc *domain.Document) error {

	var emptyOutcomeFileStrcut domain.OutcomeFile
	if app.OutcomeFile == emptyOutcomeFileStrcut {
		return nil
	}
	doc.CreatedAt = app.OutcomeFile.CreatedAt
	return nil
}
