package specificencoders

import (
	"testing"

	"opencitylabs.it/documentdispatcher/internal/core/domain"
	encoderutils "opencitylabs.it/documentdispatcher/internal/core/services/encoders/encoderUtils"
	testutils "opencitylabs.it/documentdispatcher/internal/core/testUtils"
	"opencitylabs.it/documentdispatcher/utils"
)

func TestMapMainDocumentIntegrationRequest(t *testing.T) {
	application, _ := utils.GetApplicationStruct([]byte(testutils.INPUT_APPLICATION_STATUS_4100))
	integrationOutbound, _ := encoderutils.GetLatestIntegrationOutbound(&application)
	document := domain.Document{}
	encoder := encoderIntegrationRequest{}
	err := encoder.mapMainDocument(integrationOutbound, &document)
	if err != nil {
		t.Fatal("error mappingMain document: ", err)
	}

	if document.MainDocument.Name != "original name 3.pdf" {
		t.Fatal("expected \"original name 3.pdf\" but got: ", document.MainDocument.Name)
	}

	if document.MainDocument.Filename != "name3.pdf" {
		t.Fatal("expected \"name3.pdf\" but got: ", document.MainDocument.Filename)
	}

	if *document.MainDocument.Description != "description 3" {
		t.Fatal("expected \"description 3\" but got:", *document.MainDocument.Description)
	}

	if document.MainDocument.MimeType != "application/pdf" {
		t.Fatal("expected \"application/pdf\" but got: ", document.MainDocument.MimeType)
	}
	if document.MainDocument.URL != "url3" {
		t.Fatal("expected \"url3\" but got: ", document.MainDocument.URL)
	}

}

func TestEncodeAttachmentsIntegrationRequestTwo(t *testing.T) {

	application, _ := utils.GetApplicationStruct([]byte(testutils.INPUT_APPLICATION_STATUS_4100))
	document := domain.Document{}

	encoder := encoderIntegrationRequest{}

	err := encoder.encodeAttachments(&application, &document)

	if err != nil {
		t.Fatal("error encodingAttachment document: ", err)
	}

	if document.Attachments[0].Name != "3 originalName1.pdf" {
		t.Fatal("expected \"3 originalName1.pdf\" but got: ", document.Attachments[0].Name)
	}
	if document.Attachments[0].Filename != "3 originalName1.pdf" {
		t.Fatal("expected \"3 originalName1.pdf\" but got: ", document.Attachments[0].Filename)
	}
	if *document.Attachments[0].Description != "3 description 1" {
		t.Fatal("expected \"3 description 1\" but got: ", *document.Attachments[0].Description)
	}
	if document.Attachments[0].MimeType != "application/pdf" {
		t.Fatal("expected \"application/pdf\" but got: ", document.Attachments[0].MimeType)
	}
	if document.Attachments[0].URL != "3 url1" {
		t.Fatal("expected \"3 url1\" but got: ", document.Attachments[0].URL)
	}
	if len(document.Attachments) != 3 {
		t.Fatal("expected \"2\" attachments but got: ", len(document.Attachments))
	}
	////////////////////////////////////////////////////////////////////////
	if document.Attachments[1].Name != "3 originalName2.pdf" {
		t.Fatal("expected \"3 originalName2.pdf\" but got: ", document.Attachments[1].Name)
	}
	if document.Attachments[1].Filename != "3 originalName2.pdf" {
		t.Fatal("expected \"3 originalName2.pdf\" but got: ", document.Attachments[1].Filename)
	}
	if *document.Attachments[1].Description != "3 description 2" {
		t.Fatal("expected \"3 description 2\" but got: ", *document.Attachments[1].Description)
	}
	if document.Attachments[1].MimeType != "application/pdf" {
		t.Fatal("expected \"application/pdf\" but got: ", document.Attachments[1].MimeType)
	}
	if document.Attachments[1].URL != "3 url2" {
		t.Fatal("expected \"3 url2\" but got: ", document.Attachments[1].URL)
	}

}
