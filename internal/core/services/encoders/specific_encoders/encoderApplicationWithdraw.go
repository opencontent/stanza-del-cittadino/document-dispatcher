package specificencoders

import (
	"errors"

	"go.uber.org/zap"
	"opencitylabs.it/documentdispatcher/internal/core/domain"
	"opencitylabs.it/documentdispatcher/internal/core/services/encoders"
	encoderutils "opencitylabs.it/documentdispatcher/internal/core/services/encoders/encoderUtils"
	"opencitylabs.it/documentdispatcher/logger"
	"opencitylabs.it/documentdispatcher/utils"
)

type encoderrApplicationWithdraw struct {
	log *zap.Logger
}

func NewEncoderrApplicationWithdraw() encoders.SpecificEncoder {
	return &encoderrApplicationWithdraw{
		log: logger.GetLogger(),
	}
}

// document is both input and output
func (r *encoderrApplicationWithdraw) EncodeSpecificFields(app *domain.Application, doc *domain.Document) error {
	err := r.encodeMainDocument(app, doc)
	if err != nil {
		return err
	}
	err = r.encodeOthersFields(app, doc)
	if err != nil {
		return err
	}
	return nil
}

func (r *encoderrApplicationWithdraw) encodeMainDocument(app *domain.Application, doc *domain.Document) error {
	if !encoderutils.HasPopulatedAttachment(app.Attachments) {
		return errors.New("attachments cannot be empty")
	}
	latesAttachment, err := encoderutils.GetLatestAttachment(app)
	if err != nil {
		return err
	}
	err = r.mapMainDocument(latesAttachment, doc)
	if err != nil {
		return err
	}

	return nil
}

func (r *encoderrApplicationWithdraw) mapMainDocument(attachment *domain.Attachment, doc *domain.Document) error {

	maindoc := &doc.MainDocument
	if attachment.OriginalName == "" {
		return errors.New("attachment.OriginalName cannot be empty")
	}
	maindoc.Name = attachment.OriginalName

	if attachment.Description == "" {
		return errors.New(" attachment.Description cannot be empty")
	}
	maindoc.Description = &attachment.Description

	mimetype, err := utils.GetMIMEtypeFromFileName(attachment.OriginalName)
	if err != nil {
		return err
	}
	maindoc.MimeType = mimetype

	if len(attachment.URL) == 0 {
		return errors.New(" outcomeFile.Url cannot be empty")
	}
	maindoc.URL = attachment.URL

	if attachment.Name == "" {
		return errors.New(" attachment.Name cannot be empty")
	}
	doc.MainDocument.Filename = attachment.OriginalName

	return nil
}

func (r *encoderrApplicationWithdraw) encodeOthersFields(app *domain.Application, doc *domain.Document) error {
	latesAttachment, err := encoderutils.GetLatestAttachment(app)
	if err != nil {
		return err
	}
	var emptyAttachmentFileStrcut *domain.Attachment
	if latesAttachment == emptyAttachmentFileStrcut {
		return nil
	}
	if len(doc.Attachments) == 0 {
		doc.Attachments = []domain.File{}
	}
	doc.CreatedAt = latesAttachment.CreatedAt
	return nil
}
