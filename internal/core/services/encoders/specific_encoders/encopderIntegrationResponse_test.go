package specificencoders

import (
	"testing"

	"opencitylabs.it/documentdispatcher/internal/core/domain"
	encoderutils "opencitylabs.it/documentdispatcher/internal/core/services/encoders/encoderUtils"
	testutils "opencitylabs.it/documentdispatcher/internal/core/testUtils"
	"opencitylabs.it/documentdispatcher/utils"
)

func TestMapMainDocumentIntegrationResponse(t *testing.T) {

	application, _ := utils.GetApplicationStruct([]byte(testutils.INPUT_APPLICATION_STATUS_4300))
	integrationInbound, _ := encoderutils.GetLatestIntegrationInbound(&application)
	document := domain.Document{}
	encoder := encoderIntegrationResponse{}
	err := encoder.mapMainDocument(integrationInbound, &document)
	if err != nil {
		t.Fatal("error mappingMain document: ", err)
	}

	if document.MainDocument.Name != "1 originalName.pdf" {
		t.Fatal("expected \"1 originalName.pdf\n but got ", document.MainDocument.Name)
	}
	if document.MainDocument.Filename != "1 name.pdf" {
		t.Fatal("expected \"1 name.pdf\n but got", document.MainDocument.Filename)
	}
	if *document.MainDocument.Description != "1 description" {
		t.Fatal("expected \"1 description\" but got : ", *document.MainDocument.Description)
	}
	if document.MainDocument.MimeType != "application/pdf" {
		t.Fatal("expected \"application/pdf\" but got: ", document.MainDocument.MimeType)
	}
	if document.MainDocument.URL != "1 url" {
		t.Fatal("expected \"1 url\" but got: ", document.MainDocument.URL)
	}
}

func TestEncodeAttachmentsIntegrationResponse(t *testing.T) {
	application, _ := utils.GetApplicationStruct([]byte(testutils.INPUT_APPLICATION_STATUS_4300))
	document := domain.Document{}
	encoder := encoderIntegrationResponse{}

	err := encoder.encodeAttachments(&application, &document)
	if err != nil {
		t.Fatal("error encodingAttachment document: ", err)
	}
	if document.Attachments[0].Name != "1 originalName1.pdf" {
		t.Fatal("expected \"1 originalName1.pdf\" but got: ", document.Attachments[0].Name)
	}
	if document.Attachments[0].Filename != "1 originalName1.pdf" {
		t.Fatal("expected \"1 originalName1.pdf\" but got: ", document.Attachments[0].Filename)
	}
	if *document.Attachments[0].Description != "1 description 1" {
		t.Fatal("expected \"1 description 1\" but got: ", *document.Attachments[0].Description)
	}
	if document.Attachments[0].MimeType != "application/pdf" {
		t.Fatal("expected \"application/pdf\" but got: ", document.Attachments[0].MimeType)
	}
	if document.Attachments[0].URL != "1 url1" {
		t.Fatal("expected \"1 url1\" but got: ", document.Attachments[0].URL)
	}
	if len(document.Attachments) != 2 {
		t.Fatal("expected \"2\" attachments but got: ", len(document.Attachments))
	}
	////////////////////////////////////////////////////////////////////////
	if document.Attachments[1].Name != "1 originalName2.pdf" {
		t.Fatal("expected \"1 originalName2.pdf\" but got: ", document.Attachments[1].Name)
	}
	if document.Attachments[1].Filename != "1 originalName2.pdf" {
		t.Fatal("expected \"1 originalName2.pdf\" but got: ", document.Attachments[1].Filename)
	}
	if *document.Attachments[1].Description != "1 description 2" {
		t.Fatal("expected \"1 description 2\" but got: ", *document.Attachments[1].Description)
	}
	if document.Attachments[1].MimeType != "application/pdf" {
		t.Fatal("expected \"application/pdf\" but got: ", document.Attachments[1].MimeType)
	}
	if document.Attachments[1].URL != "1 url2" {
		t.Fatal("expected \"1 url2\" but got: ", document.Attachments[1].URL)
	}
}
