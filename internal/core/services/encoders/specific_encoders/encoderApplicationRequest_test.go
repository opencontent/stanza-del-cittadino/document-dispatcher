package specificencoders

import (
	"testing"

	"opencitylabs.it/documentdispatcher/internal/core/domain"
	encoderutils "opencitylabs.it/documentdispatcher/internal/core/services/encoders/encoderUtils"
	testutils "opencitylabs.it/documentdispatcher/internal/core/testUtils"
	"opencitylabs.it/documentdispatcher/utils"
)

func TestMapMainDocument(t *testing.T) {
	application, _ := utils.GetApplicationStruct([]byte(testutils.INPUT_APPLICATION_STATUS_2000))
	compiledModule, _ := encoderutils.GetLatestModule(&application)
	document := domain.Document{}
	encoder := encoderApplicationRequest{}
	err := encoder.mapMainDocument(compiledModule, &document)
	if err != nil {
		t.Fatal("error mappingMain document: ", err)
	}

	if document.MainDocument.Name != "sdfg3.2024-202.30630013E.pdf" {
		t.Fatal("expected \"sdfg32024-20230630013E.pdf\" but got: ", document.MainDocument.Name)
	}

	if document.MainDocument.Filename != "649ebd00a8161.pdf" {
		t.Fatal("expected \"649ebd00a8161.pdf\" but got: ", document.MainDocument.Filename)
	}

	if *document.MainDocument.Description != "Modulo Richiesta del servizio" {
		t.Fatal("expected \"Modulo Richiesta del servizio\" but got:", document.MainDocument.Description)
	}
	if document.MainDocument.MimeType != "application/pdf" {
		t.Fatal("expected \"application/pdf\" but got: ", document.MainDocument.MimeType)
	}
	if document.MainDocument.URL != "https://www2.test.it" {
		t.Fatal("expected \"outcomeFile url\" but got: ", document.MainDocument.URL)
	}
}

func TestMapMainDocumentTwo(t *testing.T) {
	application, _ := utils.GetApplicationStruct([]byte(testutils.INPUT_APPLICATION_WITHOUT_COMPAILED_MODULES))
	compiledModule, _ := encoderutils.GetLatestModule(&application)
	document := domain.Document{}
	encoder := encoderApplicationRequest{}
	err := encoder.mapMainDocument(compiledModule, &document)
	if err != nil {
		t.Fatal("error mappingMain document: ", err)
	}
	if document.MainDocument.Name != "" {
		t.Fatal("expected empty field but got: ", document.MainDocument.Name)
	}
	if document.MainDocument.Filename != "" {
		t.Fatal("expected empty field but got: ", document.MainDocument.Filename)
	}
	if document.MainDocument.Description != nil {
		t.Fatal("expected empty field but got: ", *document.MainDocument.Description)
	}
}

func TestEncodeAttachments(t *testing.T) {
	application, _ := utils.GetApplicationStruct([]byte(testutils.INPUT_APPLICATION_STATUS_2000))
	document := domain.Document{}
	encoder := encoderApplicationRequest{}

	err := encoder.encodeAttachments(&application, &document)
	if err != nil {
		t.Fatal("error encodingAttachment document: ", err)
	}

	if document.Attachments[0].Name != "attachment original name 1.pdf" {
		t.Fatal("expected \"attachment original name 1.pdf\" but got: ", document.Attachments[0].Name)
	}
	if document.Attachments[0].Filename != "attachment original name 1.pdf" {
		t.Fatal("expected \"attachment original name 1.pdf\" but got: ", document.Attachments[0].Filename)
	}
	if *document.Attachments[0].Description != "attachment description 1" {
		t.Fatal("expected \"attachment description 1\" but got: ", *document.Attachments[0].Description)
	}
	if document.Attachments[0].MimeType != "application/pdf" {
		t.Fatal("expected \"application/pdf\" but got: ", document.Attachments[0].MimeType)
	}
	if document.Attachments[0].URL != "attachment url 1" {
		t.Fatal("expected \"attachment url 1\" but got: ", document.Attachments[0].URL)
	}
	if len(document.Attachments) != 3 {
		t.Fatal("expected \"2\" attachments but got: ", len(document.Attachments))
	}
	////////////////////////////////////////////////////////////////////////
	if document.Attachments[1].Name != "attachment original name 2.png" {
		t.Fatal("expected \"attachment original name 2.pdf\" but got: ", document.Attachments[1].Name)
	}
	if document.Attachments[1].Filename != "attachment original name 2.png" {
		t.Fatal("expected \"attachment original name 2.png\" but got: ", document.Attachments[1].Filename)
	}
	if *document.Attachments[1].Description != "attachment description 2" {
		t.Fatal("expected \"attachment description 2\" but got: ", *document.Attachments[1].Description)
	}
	if document.Attachments[1].MimeType != "image/png" {
		t.Fatal("expected \"image/png\" but got: ", document.Attachments[1].MimeType)
	}
	if document.Attachments[1].URL != "attachment url 2" {
		t.Fatal("expected \"attachment url 3\" but got: ", document.Attachments[1].URL)
	}
}

//input applications has 3 attachmens, but one of them has "protocol_required": true so it should be ignored

func TestEncodeAttachmentsTwo(t *testing.T) {

	application, _ := utils.GetApplicationStruct([]byte(testutils.INPUT_APPLICATION_WITHOUT_ID))
	document := domain.Document{}
	encoder := encoderApplicationRequest{}

	err := encoder.encodeAttachments(&application, &document)
	if err != nil {
		t.Fatal("error encodingAttachment document: ", err)
	}

	if len(document.Attachments) != 2 {
		t.Fatal("expected \"2\" attachments but got: ", len(document.Attachments))
	}
}

func TestEncodeOtherfileds(t *testing.T) {

	application, _ := utils.GetApplicationStruct([]byte(testutils.INPUT_APPLICATION_WITHOUT_ID))
	document := domain.Document{}
	encoder := encoderApplicationRequest{}

	err := encoder.encodeOthersFields(&application, &document)
	if err != nil {
		t.Fatal("error encodingAttachment document: ", err)
	}
	expectedCreatedAt := new(string)
	*expectedCreatedAt = "2023-06-30T13:31:26+02:00"
	if document.CreatedAt != *expectedCreatedAt {
		t.Fatal("expected \"2023-06-30T13:31:26+02:00\" but got: ", document.CreatedAt)
	}
}

func TestEncodeOtherfiledsTwo(t *testing.T) {
	application, _ := utils.GetApplicationStruct([]byte(testutils.INPUT_APPLICATION_WITHOUT_COMPAILED_MODULES))
	document := domain.Document{}
	encoder := encoderApplicationRequest{}

	err := encoder.encodeOthersFields(&application, &document)
	if err != nil {
		t.Fatal("error encodingAttachment document: ", err)
	}

	if document.CreatedAt != "" {
		t.Fatal("expected empty field but got: ", document.CreatedAt)
	}
}
