package specificencoders

import (
	"testing"

	"opencitylabs.it/documentdispatcher/internal/core/domain"
	testutils "opencitylabs.it/documentdispatcher/internal/core/testUtils"
	"opencitylabs.it/documentdispatcher/logger"
	"opencitylabs.it/documentdispatcher/utils"
)

func TestMapMainDocumentfromEncoderApplicationOutcome(t *testing.T) {
	logger := logger.GetLogger()
	defer func() {
		_ = logger.Sync()
	}()
	application, _ := utils.GetApplicationStruct([]byte(testutils.INPUT_APPLICATION_STATUS_7000))
	document := domain.Document{}
	encoder := encoderApplicationOutcome{
		log: logger,
	}
	err := encoder.mapMainDocument(&application.OutcomeFile, &document)
	if err != nil {
		t.Fatal("error mappingMain document: ", err)
	}
	if document.MainDocument.Name != "outcomeFile originalName.pdf" {
		t.Fatal("expected \"outcomeFile originalName.pdf\" but got: ", document.MainDocument.Name)
	}
	if *document.MainDocument.Description != "outcomedescription" {
		t.Fatal("expected \"outcomedescription\" but got: ", *document.MainDocument.Description)
	}
	if document.MainDocument.MimeType != "application/pdf" {
		t.Fatal("expected \"application/pdf\" but got: ", document.MainDocument.MimeType)
	}
	if document.MainDocument.URL != "outcomeFile url" {
		t.Fatal("expected \"outcomeFile url\" but got: ", document.MainDocument.URL)
	}
	if document.MainDocument.Filename != "64648a3cdb514.pdf" {
		t.Fatal("expected \"64648a3cdb514.pdf\" but got: ", document.MainDocument.Filename)
	}
}

func TestEncodeAttachments7000(t *testing.T) {
	logger := logger.GetLogger()
	defer func() {
		_ = logger.Sync()
	}()
	application, err := utils.GetApplicationStruct([]byte(testutils.INPUT_APPLICATION_STATUS_7000))
	if err != nil {
		t.Fatal("error GetApplicationStruct document: ", err)
	}
	document := domain.Document{}
	encoder := encoderApplicationOutcome{
		log: logger,
	}

	err = encoder.encodeAttachments(&application, &document)
	if err != nil {
		t.Fatal("error encodingAttachment document: ", err)
	}

	if document.Attachments[0].Name != "allegato1originalname.png" {
		t.Fatal("expected \"allegato1originalname.png\" but got: ", document.Attachments[0].Name)
	}

	if document.Attachments[1].Name != "allegato2originalname.png" {
		t.Fatal("expected \"allegato2originalname.png\" but got: ", document.Attachments[1].Name)
	}
	if document.Attachments[0].Filename != "allegato1originalname.png" {
		t.Fatal("expected \"allegato1originalname.png\" but got: ", document.Attachments[0].Filename)
	}
	if document.Attachments[1].Filename != "allegato2originalname.png" {
		t.Fatal("expected \"allegato2originalname.png\" but got: ", document.Attachments[1].Filename)
	}

}
