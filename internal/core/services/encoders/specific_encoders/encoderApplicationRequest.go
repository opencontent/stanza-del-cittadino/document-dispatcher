package specificencoders

import (
	"errors"

	"opencitylabs.it/documentdispatcher/internal/core/domain"
	"opencitylabs.it/documentdispatcher/internal/core/services/encoders"
	encoderutils "opencitylabs.it/documentdispatcher/internal/core/services/encoders/encoderUtils"
	"opencitylabs.it/documentdispatcher/utils"
)

type encoderApplicationRequest struct {
}

func NewencoderApplicationRequest() encoders.SpecificEncoder {
	return &encoderApplicationRequest{}
}

// document is both input and output
func (r *encoderApplicationRequest) EncodeSpecificFields(app *domain.Application, doc *domain.Document) error {
	err := r.encodeMainDocument(app, doc)
	if err != nil {
		return err
	}
	err = r.encodeAttachments(app, doc)
	if err != nil {
		return err
	}
	err = r.encodeOthersFields(app, doc)
	if err != nil {
		return err
	}
	return nil
}

func (r *encoderApplicationRequest) encodeMainDocument(app *domain.Application, doc *domain.Document) error {
	lastCompiledModule, err := encoderutils.GetLatestModule(app)
	if err != nil {
		return err
	}
	err = r.mapMainDocument(lastCompiledModule, doc)
	if err != nil {
		return err
	}

	return nil
}

func (r *encoderApplicationRequest) mapMainDocument(compiledModule *domain.CompiledModule, doc *domain.Document) error {
	var emptyCompileModuleStrcut *domain.CompiledModule
	if compiledModule == emptyCompileModuleStrcut {
		return nil
	}
	if !compiledModule.ProtocolRequired {
		return nil
	}
	maindoc := &doc.MainDocument
	if compiledModule.OriginalName == "" {
		return errors.New("compiledModule.OriginalName cannot be empty")
	}
	maindoc.Name = compiledModule.OriginalName

	if compiledModule.Description == "" {
		return errors.New(" compiledModule.Description cannot be empty")
	}
	maindoc.Description = &compiledModule.Description
	mimetype, err := utils.GetMIMEtypeFromFileName(compiledModule.OriginalName)
	if err != nil {
		return err
	}
	maindoc.MimeType = mimetype

	if len(compiledModule.URL) == 0 {
		return errors.New(" outcomeFile.Url cannot be empty")
	}
	maindoc.URL = compiledModule.URL

	if compiledModule.Name == "" {
		return errors.New(" compiledModule.Name cannot be empty")
	}
	doc.MainDocument.Filename = compiledModule.Name

	return nil
}

func (r *encoderApplicationRequest) encodeAttachments(app *domain.Application, doc *domain.Document) error {
	for _, attachment := range app.Attachments {
		if !attachment.ProtocolRequired {
			continue
		}
		docAttachment := domain.File{}

		if attachment.OriginalName == "" {
			return errors.New("attachment.OriginalName cannot be empty")
		}
		docAttachment.Name = attachment.OriginalName

		if attachment.Description == "" {
			return errors.New("attachment.Description cannot be empty")
		}
		description := attachment.Description
		docAttachment.Description = &description

		mimetype, err := utils.GetMIMEtypeFromFileName(attachment.OriginalName)
		if err != nil {
			return err
		}
		docAttachment.MimeType = mimetype
		if len(attachment.URL) == 0 {
			return errors.New("attachment.Url cannot be empty")
		}
		docAttachment.URL = attachment.URL

		if attachment.Name == "" {
			return errors.New("attachment.Name cannot be empty")
		}
		docAttachment.Filename = attachment.OriginalName

		doc.Attachments = append(doc.Attachments, docAttachment)

	}
	if len(doc.Attachments) == 0 {
		doc.Attachments = []domain.File{}
	}
	return nil
}

func (r *encoderApplicationRequest) encodeOthersFields(app *domain.Application, doc *domain.Document) error {

	if len(app.CompiledModules) != 0 {
		lastCompiledModule, err := encoderutils.GetLatestModule(app)
		if err != nil {
			return err
		}
		doc.CreatedAt = lastCompiledModule.CreatedAt
	}

	return nil
}
