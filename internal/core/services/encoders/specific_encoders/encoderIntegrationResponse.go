package specificencoders

import (
	"errors"

	"opencitylabs.it/documentdispatcher/internal/core/domain"
	"opencitylabs.it/documentdispatcher/internal/core/services/encoders"
	encoderutils "opencitylabs.it/documentdispatcher/internal/core/services/encoders/encoderUtils"
	"opencitylabs.it/documentdispatcher/utils"
)

type encoderIntegrationResponse struct {
}

func NewEncoderIntegrationResponse() encoders.SpecificEncoder {
	return &encoderIntegrationResponse{}
}

func (r *encoderIntegrationResponse) EncodeSpecificFields(app *domain.Application, doc *domain.Document) error {
	err := r.encodeMainDocument(app, doc)
	if err != nil {
		return err
	}
	err = r.encodeAttachments(app, doc)
	if err != nil {

		return err
	}
	err = r.encodeOthersFields(app, doc)
	if err != nil {
		return err
	}
	return nil
}

func (r *encoderIntegrationResponse) encodeMainDocument(app *domain.Application, doc *domain.Document) error {
	if encoderutils.IsIntegrationsEmpty(app) {
		return nil
	}
	lastInbound, err := encoderutils.GetLatestIntegrationInbound(app)
	if err != nil {
		return err
	}

	err = r.mapMainDocument(lastInbound, doc)
	if err != nil {
		return err
	}

	return nil
}

func (r *encoderIntegrationResponse) mapMainDocument(inbound *domain.Inbound, doc *domain.Document) error {
	maindoc := &doc.MainDocument
	if inbound.OriginalName == "" {
		return errors.New("inbound.OriginalName cannot be empty")
	}
	maindoc.Name = inbound.OriginalName

	if inbound.Description == "" {
		return errors.New(" inbound.Description cannot be empty")
	}
	maindoc.Description = &inbound.Description

	mimetype, err := utils.GetMIMEtypeFromFileName(inbound.OriginalName)
	if err != nil {
		return err
	}
	maindoc.MimeType = mimetype

	if len(inbound.URL) == 0 {
		return errors.New(" outcomeFile.Url cannot be empty")
	}
	maindoc.URL = inbound.URL

	if inbound.Name == "" {
		return errors.New(" inbound.Name cannot be empty")
	}
	doc.MainDocument.Filename = inbound.Name

	return nil
}

func (r *encoderIntegrationResponse) encodeAttachments(app *domain.Application, doc *domain.Document) error {
	if encoderutils.IsIntegrationsEmpty(app) {
		return nil
	}
	lastInbound, err := encoderutils.GetLatestIntegrationInbound(app)
	if err != nil {
		return err
	}

	for _, attachment := range lastInbound.Attachments {
		if !attachment.ProtocolRequired {
			continue
		}
		docAttachment := domain.File{}

		if attachment.OriginalName == "" {
			return errors.New("attachment.OriginalName cannot be empty")
		}
		docAttachment.Name = attachment.OriginalName

		if attachment.Description == "" {
			return errors.New("attachment.Description cannot be empty")
		}
		description := attachment.Description
		docAttachment.Description = &description

		mimetype, err := utils.GetMIMEtypeFromFileName(attachment.OriginalName)
		if err != nil {
			return err
		}
		docAttachment.MimeType = mimetype
		if len(attachment.URL) == 0 {
			return errors.New("attachment.Url cannot be empty")
		}
		docAttachment.URL = attachment.URL

		if attachment.Name == "" {
			return errors.New("attachment.Name cannot be empty")
		}
		docAttachment.Filename = attachment.OriginalName

		doc.Attachments = append(doc.Attachments, docAttachment)
	}
	if len(doc.Attachments) == 0 {
		doc.Attachments = []domain.File{}
	}

	return nil
}

func (r *encoderIntegrationResponse) encodeOthersFields(app *domain.Application, doc *domain.Document) error {

	if len(app.Integrations) != 0 {
		inbound, err := encoderutils.GetLatestIntegrationInbound(app)
		if err != nil {
			return err
		}
		doc.CreatedAt = inbound.CreatedAt
	}

	return nil
}
