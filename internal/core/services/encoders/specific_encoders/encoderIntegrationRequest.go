package specificencoders

import (
	"errors"

	"opencitylabs.it/documentdispatcher/internal/core/domain"
	"opencitylabs.it/documentdispatcher/internal/core/services/encoders"
	encoderutils "opencitylabs.it/documentdispatcher/internal/core/services/encoders/encoderUtils"
	"opencitylabs.it/documentdispatcher/utils"
)

type encoderIntegrationRequest struct {
}

func NewEncoderIntegrationRequest() encoders.SpecificEncoder {
	return &encoderIntegrationRequest{}
}

func (r *encoderIntegrationRequest) EncodeSpecificFields(app *domain.Application, doc *domain.Document) error {
	err := r.encodeMainDocument(app, doc)
	if err != nil {
		return err
	}
	err = r.encodeAttachments(app, doc)
	if err != nil {
		return err
	}
	err = r.encodeOthersFields(app, doc)
	if err != nil {
		return err
	}
	return nil
}

func (r *encoderIntegrationRequest) encodeMainDocument(app *domain.Application, doc *domain.Document) error {

	lastOutbound, err := encoderutils.GetLatestIntegrationOutbound(app)
	if err != nil {
		return err
	}

	err = r.mapMainDocument(lastOutbound, doc)
	if err != nil {
		return err
	}

	return nil
}

func (r *encoderIntegrationRequest) mapMainDocument(outbound *domain.Outbound, doc *domain.Document) error {
	maindoc := &doc.MainDocument
	if outbound.OriginalName == "" {
		return errors.New("outbound.OriginalName cannot be empty")
	}
	maindoc.Name = outbound.OriginalName

	if outbound.Description == "" {
		return errors.New(" outbound.Description cannot be empty")
	}
	maindoc.Description = &outbound.Description

	mimetype, err := utils.GetMIMEtypeFromFileName(outbound.OriginalName)
	if err != nil {
		return err
	}
	maindoc.MimeType = mimetype

	if len(outbound.URL) == 0 {
		return errors.New(" outcomeFile.Url cannot be empty")
	}
	maindoc.URL = outbound.URL

	if outbound.Name == "" {
		return errors.New(" outbound.Name cannot be empty")
	}
	doc.MainDocument.Filename = outbound.Name

	return nil
}

func (r *encoderIntegrationRequest) encodeAttachments(app *domain.Application, doc *domain.Document) error {
	if encoderutils.IsIntegrationsEmpty(app) {
		return nil
	}
	lastOutbound, err := encoderutils.GetLatestIntegrationOutbound(app)
	if err != nil {
		return err
	}

	for _, attachment := range lastOutbound.Attachments {
		if !attachment.ProtocolRequired {
			continue
		}
		docAttachment := domain.File{}

		if attachment.OriginalName == "" {
			return errors.New("attachment.OriginalName cannot be empty")
		}
		docAttachment.Name = attachment.OriginalName

		if attachment.Description == "" {
			return errors.New("attachment.Description cannot be empty")
		}
		description := attachment.Description
		docAttachment.Description = &description

		mimetype, err := utils.GetMIMEtypeFromFileName(attachment.OriginalName)
		if err != nil {
			return err
		}
		docAttachment.MimeType = mimetype
		if len(attachment.URL) == 0 {
			return errors.New("attachment.Url cannot be empty")
		}
		docAttachment.URL = attachment.URL

		if attachment.Name == "" {
			return errors.New("attachment.Name cannot be empty")
		}
		docAttachment.Filename = attachment.OriginalName

		doc.Attachments = append(doc.Attachments, docAttachment)

	}
	if len(doc.Attachments) == 0 {
		doc.Attachments = []domain.File{}
	}

	return nil
}

func (r *encoderIntegrationRequest) encodeOthersFields(app *domain.Application, doc *domain.Document) error {

	if len(app.Integrations) != 0 {
		outbound, err := encoderutils.GetLatestIntegrationOutbound(app)
		if err != nil {
			return err
		}
		doc.CreatedAt = outbound.CreatedAt
	}

	return nil
}
