package encoders

import "opencitylabs.it/documentdispatcher/internal/core/domain"

type Encoder interface {
	EncodeDocument(app *domain.Application, doc *domain.Document) error
}

type CommonEncoder interface {
	EncodeCommonFields(*domain.Application, *domain.Document) error
}

type SpecificEncoder interface {
	EncodeSpecificFields(*domain.Application, *domain.Document) error
}
