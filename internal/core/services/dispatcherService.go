package services

import (
	"errors"

	"github.com/getsentry/sentry-go"
	"github.com/go-playground/validator"
	"go.uber.org/zap"
	"opencitylabs.it/documentdispatcher/config"
	"opencitylabs.it/documentdispatcher/internal/core/domain"
	"opencitylabs.it/documentdispatcher/internal/core/ports"
	"opencitylabs.it/documentdispatcher/internal/core/services/encoders"
	"opencitylabs.it/documentdispatcher/internal/core/services/encoders/commonencoder"
	specificencoders "opencitylabs.it/documentdispatcher/internal/core/services/encoders/specific_encoders"
	servicesutils "opencitylabs.it/documentdispatcher/internal/core/services/servicesUtils"
	"opencitylabs.it/documentdispatcher/logger"
	"opencitylabs.it/documentdispatcher/sentryutils"
)

type dispatcherService struct {
	log          *zap.Logger
	conf         config.Config
	sentryHub    *sentry.Hub
	validator    *validator.Validate
	producerRepo ports.DocumentProducerRepository
	docsDataRepo ports.DocumentsDataRepository
	Msg          string
}

func NewDispatcherService(producerRepo ports.DocumentProducerRepository, docsDataRepo ports.DocumentsDataRepository) ports.DispatcherService {
	return &dispatcherService{
		log:          logger.GetLogger(),
		conf:         *config.GetConfig(),
		sentryHub:    sentryutils.InitLocalSentryhub(),
		validator:    validator.New(),
		producerRepo: producerRepo,
		docsDataRepo: docsDataRepo,
	}
}

func (r *dispatcherService) ProcessMessage(application *domain.Application) error {

	if application == nil {
		r.Msg = "application nil pointer"
		return errors.New("application nil pointer")
	}
	if application.Status == "2000" && len(application.CompiledModules) == 0 {
		r.Msg = "Irrelevant application - compiled modules null "
		return nil
	}
	err := r.validateApplicationMessage(application)
	if err != nil {
		r.Msg = err.Error()
		r.log.Error(r.Msg,
			zap.String("application id", application.ID))
		sentry.CaptureException(errors.New(r.Msg))
		return err
	}

	encoder, err := r.buildEncoder(application.Status)
	if err != nil {
		r.Msg = err.Error()
		r.log.Debug(r.Msg,
			zap.String("application id", application.ID))
		return nil
	}

	document := &domain.Document{}

	err = encoder.EncodeDocument(application, document)
	if err != nil {
		r.Msg = err.Error()
		r.log.Error(r.Msg,
			zap.String("application id", application.ID))
		sentry.CaptureException(errors.New(r.Msg))
		return err
	}

	documentType := servicesutils.GetDocType(application.Status)
	if documentType == "" {
		r.Msg = "Irrelevant application"
		return nil
	}
	if !r.shouldProcessThisApplication(application.Type) {
		r.Msg = "Irrelevant application"
		return nil
	}
	if r.conf.Kafka.KsqdbEnabled {
		isDocumentExists, err := r.docsDataRepo.IsDocumentExists(application, document.CreatedAt, documentType)
		if err != nil {
			r.Msg = err.Error()
			sentry.CaptureException(errors.New(r.Msg))
			r.log.Error(r.Msg,
				zap.String("application id", application.ID))
			return err
		}

		if isDocumentExists {
			r.Msg = "document already created"
			return nil
		}
	}

	err = r.producerRepo.ProduceMessage(*document)
	if err != nil {
		r.Msg = err.Error()
		r.log.Error(r.Msg,
			zap.String("application id", application.ID))
		sentry.CaptureException(errors.New(r.Msg))
		return err
	}
	r.Msg = "document " + document.ID + " created"
	//r.log.Debug(r.Msg)
	return nil
}

func (r *dispatcherService) validateApplicationMessage(application *domain.Application) error {

	err := r.validator.Struct(application)
	if err != nil {
		return err
	}
	return nil
}

func (r *dispatcherService) buildEncoder(appStatus string) (encoders.Encoder, error) {
	commonEncoder := commonencoder.NewCommonEncoder()
	var specificEncoder encoders.SpecificEncoder

	switch appStatus {
	case "2000":
		specificEncoder = specificencoders.NewencoderApplicationRequest()
	case "4100":
		specificEncoder = specificencoders.NewEncoderIntegrationRequest()
	case "4300":
		specificEncoder = specificencoders.NewEncoderIntegrationResponse()
	case "7000", "9000":
		specificEncoder = specificencoders.NewEncoderApplicationOutcome()
	case "20000", "50000":
		specificEncoder = specificencoders.NewEncoderrApplicationWithdraw()
	default:
		return nil, errors.New("discarded because status is " + appStatus)
	}
	encoder := encoders.NewEncoder(commonEncoder, specificEncoder)
	return encoder, nil
}

func (r *dispatcherService) GetMessage() string {
	return r.Msg
}

func (r *dispatcherService) shouldProcessThisApplication(appType string) bool {
	if appType != "" && appType == "external" {
		return false
	}
	return true
}
