package ports

import "opencitylabs.it/documentdispatcher/internal/core/domain"

// driver ports
type DispatcherService interface {
	ProcessMessage(*domain.Application) error
	GetMessage() string
}

type DocumentService interface {
	ProcessMessage(*domain.DocumentDto) error
	GetMessage() string
	GetDocument() *domain.Document
}

// driven ports
type DocumentProducerRepository interface {
	ProduceMessage(document domain.Document) error
	CloseKafkaWriter()
}

type DocumentsDataRepository interface {
	IsDocumentExists(app *domain.Application, createdAt, docType string) (bool, error)
}
