package testutils

import (
	"encoding/json"

	"opencitylabs.it/documentdispatcher/internal/core/domain"
)

const CORRECT_FULL_DTO = `{
	"title": "Test Document",
	"holder": [{
		"name": "John",
		"family_name": "Doe",
		"tax_identification_number": "1234567890"
	}],
  "remote_collection": {
    "entity_instance_id": "EXT12345",
    "entity_type": "Archivio Digitale"
  }
}`

const MISSING_HOLDER_DTO = `{
	"title": "Test Document",
	  "remote_collection": {
    "entity_instance_id": "EXT12345",
    "entity_type": "Archivio Digitale"
  }
}`

const PARTIAL_HOLDER_DTO = `{
	"title": "Test Document",
	"holder": [{
		"name": "John"
	}],
	  "remote_collection": {
    "entity_instance_id": "EXT12345",
    "entity_type": "Archivio Digitale"
  }
}`

const MULTIPLE_HOLDERS_DTO = `{
	"title": "Test Document",
	"holder": [
		{"name": "John", "family_name": "Doe", "tax_identification_number": "123"},
		{"name": "Jane", "family_name": "Smith", "tax_identification_number": "456"}
	],
	  "remote_collection": {
    "entity_instance_id": "EXT12345",
    "entity_type": "Archivio Digitale"
  }
}`

const TITLE_CONTAINS_HOLDER_INFO_DTO = `{
	"title": "John Doe 1234567890",
	"holder": [{
		"name": "John",
		"family_name": "Doe",
		"tax_identification_number": "1234567890"
	}],
	  "remote_collection": {
    "entity_instance_id": "EXT12345",
    "entity_type": "Archivio Digitale"
  }
}`

const TITLE_CONTAINS_PARTIAL_INFO_DTO = `{
	"title": "Test Document John",
	"holder": [{
		"name": "John",
		"family_name": "Doe"
	}],
	  "remote_collection": {
    "entity_instance_id": "EXT12345",
    "entity_type": "Archivio Digitale"
  }
}`

func GetDocumentDtoFromJson(jsonStr string) (*domain.DocumentDto, error) {
	var dto domain.DocumentDto
	err := json.Unmarshal([]byte(jsonStr), &dto)
	if err != nil {
		return nil, err
	}
	return &dto, nil
}
