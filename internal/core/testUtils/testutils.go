package testutils

// applications
const INPUT_APPLICATION_STATUS_2000 = `{
	"app_id": "symfony-core:2.20.4",
	"app_version": "2.20.4",
	"attachments": [
		{
			"created_at": "2023-06-30T13:29:31+02:00",
			"description": "attachment description 1",
			"external_id": null,
			"id": "attachment id 1",
			"name": "attachment name 1.pdf",
			"originalName": "attachment original name 1.pdf",
			"protocol_number": null,
			"protocol_required": true,
			"url": "attachment url 1"
		},
		{
			"created_at": "2023-06-30T13:29:42+02:00",
			"description": "attachment description 2",
			"external_id": null,
			"id": "attachment id 2",
			"name": "attachment name 2.png",
			"originalName": "attachment original name 2.png",
			"protocol_number": null,
			"protocol_required": true,
			"url": "attachment url 2"
		},
		{
			"created_at": "2023-06-30T13:29:42+02:00",
			"description": "Allegato3",
			"external_id": null,
			"id": "attachment id 3",
			"name": "attachment name 3.jpg",
			"originalName": "attachment original name 3.jpg",
			"protocol_number": null,
			"protocol_required": true,
			"url": "attachment url 3"
		}
	],
	"authentication": {
		"authentication_method": "spid",
		"certificate": null,
		"certificate_issuer": null,
		"certificate_subject": null,
		"instant": "2023-06-30T13:14:48+0200",
		"session_id": "22222222",
		"session_index": "22222222",
		"spid_code": "PPPPP8VG9X4E6G",
		"spid_level": "2"
	},
	"backoffice_data": null,
	"compiled_modules": [
		{
			"created_at": "2023-06-30T13:31:26+02:00",
			"description": "Modulo Richiesta del servizio",
			"external_id": null,
			"id": "6dc1a86e-db17-45c9-8627-40d184920e3E",
			"name": "649ebd00a8161.pdf",
			"originalName": "34gsdf024-22222222.pdf",
			"protocol_number": null,
			"protocol_required": true,
			"url": "https://www2.test.it/comune-di-seriate/api/applications/22222222-0e5d-4a14-8de1-7f423571fb3e/attachments/6dc1a86e-db17-45c9-8627-40d184920e3E?version=1"
		},
		{
			"created_at": "2023-07-15T13:31:25+02:00",
			"description": "Modulo Richiesta del servizio",
			"external_id": null,
			"id": "6dc1a86e-db17-45c9-8627-40d184920e3E",
			"name": "649ebd00a8161.pdf",
			"originalName": "sdfg24-22222222.pdf",
			"protocol_number": null,
			"protocol_required": true,
			"url": "https://www2.test.it/comune-di-seriate/api/applications/22222222-0e5d-4a14-8de1-7f423571fb3e/attachments/6dc1a86e-db17-45c9-8627-40d184920e3E?version=1"
		},
		{
			"created_at": "2023-07-15T13:31:27+02:00",
			"description": "Modulo Richiesta del servizio",
			"external_id": null,
			"id": "6dc1a86e-db17-45c9-8627-40d184920e3E",
			"name": "649ebd00a8161.pdf",
			"originalName": "sdfg3.2024-202.30630013E.pdf",
			"protocol_number": null,
			"protocol_required": true,
			"url": "https://www2.test.it"
		}
	],
	"created_at": "2023-06-30T13:14:48+02:00",
	"creation_time": 1688123688,
	"data": {
		"Diagnosi_funzionale1": [
			{
				"description": "Allegato",
				"id": "23180c89-0161-48c8-a855-4cf75ee8fc3e",
				"name": "Diagnosi Funzionale I.L._compressed-22222222-cbd9-4147-af4a-eb8821365f3e.pdf",
				"originalName": "Diagnosi Funzionale I.L._compressed.pdf",
				"protocol_required": true,
				"url": "https://www2.test.it/comune-di-seriate/api/applications/22222222-0e5d-4a14-8de1-7f423571fb3e/attachments/23180c89-0161-48c8-a855-4cf75ee8fc3e?version=1"
			}
		],
		"Diagnosi_funzionale2": [],
		"RichiestaSiNo.no": false,
		"RichiestaSiNo.si": true,
		"accettazioneDichiarazioneDiResponsabilta6": true,
		"accettazioneDichiarazioneDiResponsabilta7": true,
		"annoProtocolloNumeroProtocollo": "2022-2023",
		"annoProtocolloNumeroProtocollo1": "2022-2023",
		"annoScolastico": "2023/2024",
		"applicant.data.Born.data.natoAIl": "1989-06-28T00:00:00+02:00",
		"applicant.data.Born.data.place_of_birth": "Potenza ",
		"applicant.data.address.data.address": "Via del Suffragio",
		"applicant.data.address.data.address_data.cod_catastale": "L378",
		"applicant.data.address.data.address_data.codice_comune": 12345,
		"applicant.data.address.data.address_data.codice_nuts2": "ITH0",
		"applicant.data.address.data.address_data.comune": "Trento",
		"applicant.data.address.data.address_data.estinto": 0,
		"applicant.data.address.data.address_data.estinto_dal": "",
		"applicant.data.address.data.address_data.id_luogo_istituito": "",
		"applicant.data.address.data.address_data.istituito_dal": "",
		"applicant.data.address.data.address_data.luogo_istat": "TRENTO",
		"applicant.data.address.data.address_data.note": "",
		"applicant.data.address.data.address_data.pr": "TN",
		"applicant.data.address.data.address_data.provincia": "Trento",
		"applicant.data.address.data.address_data.regione": "Trentino-Alto Adige/Südtirol",
		"applicant.data.address.data.address_data.slug": "trento",
		"applicant.data.address.data.codice_catastale": "L000",
		"applicant.data.address.data.country": "Italia",
		"applicant.data.address.data.county": "TN",
		"applicant.data.address.data.house_number": "13",
		"applicant.data.address.data.municipality": "Trento",
		"applicant.data.address.data.postal_code": "38122",
		"applicant.data.cell_number": "34700000",
		"applicant.data.completename.data.name": "Coccorita",
		"applicant.data.completename.data.surname": "Coccorico",
		"applicant.data.email_address": "xxxx@gmail.com",
		"applicant.data.fiscal_code.data.fiscal_code": "CCCCCC73C50F158S",
		"applicant.data.gender.data.gender": "maschio",
		"applicant.data.numeroDiTelefonoFisso": "",
		"codiceMeccanograficoIstituto": "BGIC81203E",
		"dellistitutoScolastico": "Istituto Comprensivo \"A. Mazzi\"",
		"eMailItstituto": "coccorita@istruzione.it",
		"ePresenteCertificazioneAggiornata": true,
		"ePresenteCertificazioneAggiornata1": true,
		"emailGenitore": "cocco@gmail.com",
		"eventualiNote1": "",
		"eventualiNote2": "",
		"form1.data.name": "Leyla",
		"form1.data.surname": "Fray",
		"form2.data.natoAIl": "2012-09-08T00:00:00+02:00",
		"form2.data.place_of_birth": "Bergamo",
		"form3.data.fiscal_code": "SSSSSS12P48A794C",
		"frequentanteListututo": "Comprensivo \"A. Mazzi\"",
		"frequentanteListututo1": "F.lli Calvi",
		"frequentanteListututo2": "unica",
		"frequentanteListututo3": "5",
		"inQualitaDi": "delegatoDirigente",
		"oreSettimanali": "0",
		"recapitoTelefonico": "1111111111",
		"recapitoTelefonicoIstituto": "2222222222",
		"referenteInsegnanti": "Simonetta Bellini",
		"select": "scuolaPrimaria",
		"specialistaDiRiferimento": "NPI dott.ssa Lucia test, Psicologa dott.ssa Silvia CognomeDiSilvia",
		"textField": "40",
		"verbale_situazioneh1": [
			{
				"description": "Allegato",
				"id": "924cfa9c-0c81-4ada-a761-80a0308e563e",
				"name": "Verbale Accertamento I.L.-6e1b377d-7a3d-4bb3-b921-8acd97e2773e.pdf",
				"originalName": "Verbale Accertamento I.L..pdf",
				"protocol_required": true,
				"url": "https://www2.test.it/comune-di-seriate/api/applications/22222222-0e5d-4a14-8de1-7f423571fb3e/attachments/924cfa9c-0c81-4ada-a761-80a0308e563e?version=1"
			}
		]
	},
	"event_created_at": "2023-06-30T13:31:26+02:00",
	"event_id": "e9bf89d5-fdfe-44b1-b89d-b0fcae29999a",
	"event_version": 2,
	"external_id": null,
	"flow_changed_at": "2023-06-30T13:31:07+02:00",
	"geographic_areas": [],
	"id": "22222222-0e5d-4a14-8de1-7f423571fb3e",
	"integrations": [],
	"latest_status_change_at": "2023-06-30T13:31:26+02:00",
	"latest_status_change_time": 1688124686,
	"links": [
		{
			"action": "register",
			"description": "Register Application",
			"url": "https://www2.test.it/comune-di-seriate/api/applications/22222222-0e5d-4a14-8de1-7f423571fb3e/transition/register?version=1"
		},
		{
			"action": "assign",
			"description": "Assign Application",
			"url": "https://www2.test.it/comune-di-seriate/api/applications/22222222-0e5d-4a14-8de1-7f423571fb3e/transition/assign?version=1"
		}
	],
	"meetings": [],
	"operator_id": null,
	"outcome": null,
	"outcome_attachments": [],
	"outcome_file": null,
	"outcome_motivation": null,
	"outcome_protocol_document_id": null,
	"outcome_protocol_number": null,
	"outcome_protocol_numbers": null,
	"outcome_protocol_time": null,
	"outcome_protocolled_at": null,
	"path": "/applications",
	"payment_data": [],
	"payment_type": null,
	"protocol_document_id": null,
	"protocol_folder_code": null,
	"protocol_folder_number": null,
	"protocol_number": null,
	"protocol_numbers": [],
	"protocol_time": null,
	"protocolled_at": null,
	"service": "richiesta-del-servizio-di-assistenza-specialistica-per-lautonomia-e-la-comunicazione-personale-a-s-2023-2024",
	"service_group_name": null,
	"service_id": "f542da62-9796-419c-bcc6-d309c3fdeb4a",
	"service_name": "Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024",
	"source_type": "http",
	"status": "2000",
	"status_name": "status_submitted",
	"subject": "Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024 Coccorita Coccorico CCCCCC73C50F158S",
	"submission_time": 1688124667,
	"submitted_at": "2023-06-30T13:31:07+02:00",
	"tenant": "N12345-c90c-4f30-8d92-N12345",
	"tenant_id": "N12345-c90c-4f30-8d92-N12345",
	"timestamp": "2023-06-30T11:31:26.964642084Z",
	"user": "a717b215-4277-4c9f-a056-4d8948c2e33e",
	"user_compilation_notes": null,
	"user_group_id": null,
	"user_name": "Coccorita Coccorico",
	"x-forwarded-for": "172.31.66.208",
	"__registry_metadata": {
		"attempts": 1,
		"retry_at": "2023-06-30T11:46:21.746820+00:00",
		"retry_topic": "retry_10_production"
	}
}`

const INPUT_APPLICATION_WITHOUT_ID = `{
	"app_id": "symfony-core:2.20.4",
	"app_version": "2.20.4",
	"attachments": [
		{
			"created_at": "2023-06-30T13:29:31+02:00",
			"description": "Allegato",
			"external_id": null,
			"id": "924cfa9c-0c81-4ada-a761-80a0308e563e",
			"name": "649ebc9b4828b73205333e.pdf",
			"originalName": "verbale-accertamento-i.l.-6e1b377d-7a3d-4bb3-b921-8acd97e2773e.pdf",
			"protocol_number": null,
			"protocol_required": true,
			"url": "https://www2.test.it/comune-di-seriate/api/applications/22222222-0e5d-4a14-8de1-7f423571fb3e/attachments/924cfa9c-0c81-4ada-a761-80a0308e563e?version=1"
		},
		{
			"created_at": "2023-06-30T13:29:42+02:00",
			"description": "Allegato",
			"external_id": null,
			"id": "23180c89-0161-48c8-a855-4cf75ee8fc3e",
			"name": "asdfag4fd593917023e.pdf",
			"originalName": "diagnosi-funzionale-i.l._compressed-22222222-cbd9-4147-af4a-eb8821365f3e.pdf",
			"protocol_number": null,
			"protocol_required": true,
			"url": "https://www2.test.it/comune-di-seriate/api/applications/22222222-0e5d-4a14-8de1-7f423571fb3e/attachments/23180c89-0161-48c8-a855-4cf75ee8fc3e?version=1"
		},
		{
			"created_at": "2023-06-30T13:29:42+02:00",
			"description": "Allegato",
			"external_id": null,
			"id": "23180c89-0161-48c8-a855-4cf75ee8fc3e",
			"name": "asdfag4fd593917023e.pdf",
			"originalName": "diagnosi-funzionale-i.l._compressed-22222222-cbd9-4147-af4a-eb8821365f3e.pdf",
			"protocol_number": null,
			"protocol_required": false,
			"url": "https://www2.test.it/comune-di-seriate/api/applications/22222222-0e5d-4a14-8de1-7f423571fb3e/attachments/23180c89-0161-48c8-a855-4cf75ee8fc3e?version=1"
		}
	],
	"authentication": {
		"authentication_method": "spid",
		"certificate": null,
		"certificate_issuer": null,
		"certificate_subject": null,
		"instant": "2023-06-30T13:14:48+0200",
		"session_id": "22222222",
		"session_index": "22222222",
		"spid_code": "PPPPP8VG9X4E6G",
		"spid_level": "2"
	},
	"backoffice_data": null,
	"compiled_modules": [
		{
			"created_at": "2023-06-30T13:31:26+02:00",
			"description": "Modulo Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024 2023-06-30 01:31",
			"external_id": null,
			"id": "6dc1a86e-db17-45c9-8627-40d184920e3E",
			"name": "649ebd00a8161.pdf",
			"originalName": "modulo-richiesta-del-servizio-di-assistenza-specialistica-per-lautonomia-e-la-comunicazione-personale-a.s.20232024-22222222.pdf",
			"protocol_number": null,
			"protocol_required": true,
			"url": "https://www2.test.it/comune-di-seriate/api/applications/22222222-0e5d-4a14-8de1-7f423571fb3e/attachments/6dc1a86e-db17-45c9-8627-40d184920e3E?version=1"
		}
	],
	"created_at": "2023-06-30T13:14:48+02:00",
	"creation_time": 1688123688,
	"data": {
		"Diagnosi_funzionale1": [
			{
				"description": "Allegato",
				"id": "23180c89-0161-48c8-a855-4cf75ee8fc3e",
				"name": "Diagnosi Funzionale I.L._compressed-22222222-cbd9-4147-af4a-eb8821365f3e.pdf",
				"originalName": "Diagnosi Funzionale I.L._compressed.pdf",
				"protocol_required": true,
				"url": "https://www2.test.it/comune-di-seriate/api/applications/22222222-0e5d-4a14-8de1-7f423571fb3e/attachments/23180c89-0161-48c8-a855-4cf75ee8fc3e?version=1"
			}
		],
		"Diagnosi_funzionale2": [],
		"RichiestaSiNo.no": false,
		"RichiestaSiNo.si": true,
		"accettazioneDichiarazioneDiResponsabilta6": true,
		"accettazioneDichiarazioneDiResponsabilta7": true,
		"annoProtocolloNumeroProtocollo": "2022-2023",
		"annoProtocolloNumeroProtocollo1": "2022-2023",
		"annoScolastico": "2023/2024",
		"applicant.data.completename.data.name": "Coccorita",
		"applicant.data.completename.data.surname": "Coccorico",
		"applicant.data.fiscal_code.data.fiscal_code": "CCCCCC73C50F158S",
		"codiceMeccanograficoIstituto": "BGIC81203E",
		"dellistitutoScolastico": "Istituto Comprensivo \"A. Mazzi\"",
		"eMailItstituto": "coccorita@istruzione.it",
		"ePresenteCertificazioneAggiornata": true,
		"ePresenteCertificazioneAggiornata1": true,
		"emailGenitore": "cocco@gmail.com",
		"eventualiNote1": "",
		"eventualiNote2": "",
		"form1.data.name": "Leyla",
		"form1.data.surname": "Fray",
		"form2.data.natoAIl": "2012-09-08T00:00:00+02:00",
		"form2.data.place_of_birth": "Bergamo",
		"form3.data.fiscal_code": "SSSSSS12P48A794C",
		"frequentanteListututo": "Comprensivo \"A. Mazzi\"",
		"frequentanteListututo1": "F.lli Calvi",
		"frequentanteListututo2": "unica",
		"frequentanteListututo3": "5",
		"inQualitaDi": "delegatoDirigente",
		"oreSettimanali": "0",
		"recapitoTelefonico": "1111111111",
		"recapitoTelefonicoIstituto": "2222222222",
		"referenteInsegnanti": "Simonetta Bellini",
		"select": "scuolaPrimaria",
		"specialistaDiRiferimento": "NPI dott.ssa Lucia test, Psicologa dott.ssa Silvia CognomeDiSilvia",
		"textField": "40",
		"verbale_situazioneh1": [
			{
				"description": "Allegato",
				"id": "924cfa9c-0c81-4ada-a761-80a0308e563e",
				"name": "Verbale Accertamento I.L.-6e1b377d-7a3d-4bb3-b921-8acd97e2773e.pdf",
				"originalName": "Verbale Accertamento I.L..pdf",
				"protocol_required": true,
				"url": "https://www2.test.it/comune-di-seriate/api/applications/22222222-0e5d-4a14-8de1-7f423571fb3e/attachments/924cfa9c-0c81-4ada-a761-80a0308e563e?version=1"
			}
		]
	},
	"event_created_at": "2023-06-30T13:31:26+02:00",
	"event_id": "e9bf89d5-fdfe-44b1-b89d-b0fcae29999a",
	"event_version": 2,
	"external_id": null,
	"flow_changed_at": "2023-06-30T13:31:07+02:00",
	"geographic_areas": [],
	"integrations": [],
	"latest_status_change_at": "2023-06-30T13:31:26+02:00",
	"latest_status_change_time": 1688124686,
	"links": [
		{
			"action": "register",
			"description": "Register Application",
			"url": "https://www2.test.it/comune-di-seriate/api/applications/22222222-0e5d-4a14-8de1-7f423571fb3e/transition/register?version=1"
		},
		{
			"action": "assign",
			"description": "Assign Application",
			"url": "https://www2.test.it/comune-di-seriate/api/applications/22222222-0e5d-4a14-8de1-7f423571fb3e/transition/assign?version=1"
		}
	],
	"meetings": [],
	"operator_id": null,
	"outcome": null,
	"outcome_attachments": [],
	"outcome_file": null,
	"outcome_motivation": null,
	"outcome_protocol_document_id": null,
	"outcome_protocol_number": null,
	"outcome_protocol_numbers": null,
	"outcome_protocol_time": null,
	"outcome_protocolled_at": null,
	"path": "/applications",
	"payment_data": [],
	"payment_type": null,
	"protocol_document_id": null,
	"protocol_folder_code": null,
	"protocol_folder_number": null,
	"protocol_number": null,
	"protocol_numbers": [],
	"protocol_time": null,
	"protocolled_at": null,
	"service": "richiesta-del-servizio-di-assistenza-specialistica-per-lautonomia-e-la-comunicazione-personale-a-s-2023-2024",
	"service_group_name": null,
	"service_id": "f542da62-9796-419c-bcc6-d309c3fdeb4a",
	"service_name": "Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024",
	"source_type": "http",
	"status": "2000",
	"status_name": "status_submitted",
	"subject": "Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024 Coccorita Coccorico CCCCCC73C50F158S",
	"submission_time": 1688124667,
	"submitted_at": "2023-06-30T13:31:07+02:00",
	"tenant": "N12345-c90c-4f30-8d92-N12345",
	"tenant_id": "N12345-c90c-4f30-8d92-N12345",
	"timestamp": "2023-06-30T11:31:26.964642084Z",
	"user": "a717b215-4277-4c9f-a056-4d8948c2e33e",
	"user_compilation_notes": null,
	"user_group_id": null,
	"user_name": "Coccorita Coccorico",
	"x-forwarded-for": "172.31.66.208",
	"__registry_metadata": {
		"attempts": 1,
		"retry_at": "2023-06-30T11:46:21.746820+00:00",
		"retry_topic": "retry_10_production"
	}
}`

const INPUT_APPLICATION_WITHOUT_COMPAILED_MODULES = `{
	"app_id": "symfony-core:2.20.4",
	"app_version": "2.20.4",
	"attachments": [
		{
			"created_at": "2023-06-30T13:29:31+02:00",
			"description": "Allegato",
			"external_id": null,
			"id": "924cfa9c-0c81-4ada-a761-80a0308e563e",
			"name": "649ebc9b4828b73205333e.pdf",
			"originalName": "verbale-accertamento-i.l.-6e1b377d-7a3d-4bb3-b921-8acd97e2773e.pdf",
			"protocol_number": null,
			"protocol_required": true,
			"url": "https://www2.test.it/comune-di-seriate/api/applications/22222222-0e5d-4a14-8de1-7f423571fb3e/attachments/924cfa9c-0c81-4ada-a761-80a0308e563e?version=1"
		},
		{
			"created_at": "2023-06-30T13:29:42+02:00",
			"description": "Allegato",
			"external_id": null,
			"id": "23180c89-0161-48c8-a855-4cf75ee8fc3e",
			"name": "asdfag4fd593917023e.pdf",
			"originalName": "diagnosi-funzionale-i.l._compressed-22222222-cbd9-4147-af4a-eb8821365f3e.pdf",
			"protocol_number": null,
			"protocol_required": true,
			"url": "https://www2.test.it/comune-di-seriate/api/applications/22222222-0e5d-4a14-8de1-7f423571fb3e/attachments/23180c89-0161-48c8-a855-4cf75ee8fc3e?version=1"
		}
	],
	"authentication": {
		"authentication_method": "spid",
		"certificate": null,
		"certificate_issuer": null,
		"certificate_subject": null,
		"instant": "2023-06-30T13:14:48+0200",
		"session_id": "22222222",
		"session_index": "22222222",
		"spid_code": "PPPPP8VG9X4E6G",
		"spid_level": "2"
	},
	"backoffice_data": null,
	"compiled_modules": [],
	"created_at": "2023-06-30T13:14:48+02:00",
	"creation_time": 1688123688,
	"data": {
		"Diagnosi_funzionale1": [
			{
				"description": "Allegato",
				"id": "23180c89-0161-48c8-a855-4cf75ee8fc3e",
				"name": "Diagnosi Funzionale I.L._compressed-22222222-cbd9-4147-af4a-eb8821365f3e.pdf",
				"originalName": "Diagnosi Funzionale I.L._compressed.pdf",
				"protocol_required": true,
				"url": "https://www2.test.it/comune-di-seriate/api/applications/22222222-0e5d-4a14-8de1-7f423571fb3e/attachments/23180c89-0161-48c8-a855-4cf75ee8fc3e?version=1"
			}
		],
		"Diagnosi_funzionale2": [],
		"RichiestaSiNo.no": false,
		"RichiestaSiNo.si": true,
		"accettazioneDichiarazioneDiResponsabilta6": true,
		"accettazioneDichiarazioneDiResponsabilta7": true,
		"annoProtocolloNumeroProtocollo": "2022-2023",
		"annoProtocolloNumeroProtocollo1": "2022-2023",
		"annoScolastico": "2023/2024",
		"applicant.data.completename.data.name": "Coccorita",
		"applicant.data.completename.data.surname": "Coccorico",
		"applicant.data.fiscal_code.data.fiscal_code": "CCCCCC73C50F158S",
		"codiceMeccanograficoIstituto": "BGIC81203E",
		"dellistitutoScolastico": "Istituto Comprensivo \"A. Mazzi\"",
		"eMailItstituto": "coccorita@istruzione.it",
		"ePresenteCertificazioneAggiornata": true,
		"ePresenteCertificazioneAggiornata1": true,
		"emailGenitore": "cocco@gmail.com",
		"eventualiNote1": "",
		"eventualiNote2": "",
		"form1.data.name": "Leyla",
		"form1.data.surname": "Fray",
		"form2.data.natoAIl": "2012-09-08T00:00:00+02:00",
		"form2.data.place_of_birth": "Bergamo",
		"form3.data.fiscal_code": "SSSSSS12P48A794C",
		"frequentanteListututo": "Comprensivo \"A. Mazzi\"",
		"frequentanteListututo1": "F.lli Calvi",
		"frequentanteListututo2": "unica",
		"frequentanteListututo3": "5",
		"inQualitaDi": "delegatoDirigente",
		"oreSettimanali": "0",
		"recapitoTelefonico": "1111111111",
		"recapitoTelefonicoIstituto": "2222222222",
		"referenteInsegnanti": "Simonetta Bellini",
		"select": "scuolaPrimaria",
		"specialistaDiRiferimento": "NPI dott.ssa Lucia test, Psicologa dott.ssa Silvia CognomeDiSilvia",
		"textField": "40",
		"verbale_situazioneh1": [
			{
				"description": "Allegato",
				"id": "924cfa9c-0c81-4ada-a761-80a0308e563e",
				"name": "Verbale Accertamento I.L.-6e1b377d-7a3d-4bb3-b921-8acd97e2773e.pdf",
				"originalName": "Verbale Accertamento I.L..pdf",
				"protocol_required": true,
				"url": "https://www2.test.it/comune-di-seriate/api/applications/22222222-0e5d-4a14-8de1-7f423571fb3e/attachments/924cfa9c-0c81-4ada-a761-80a0308e563e?version=1"
			}
		]
	},
	"event_created_at": "2023-06-30T13:31:26+02:00",
	"event_id": "e9bf89d5-fdfe-44b1-b89d-b0fcae29999a",
	"event_version": 2,
	"external_id": null,
	"flow_changed_at": "2023-06-30T13:31:07+02:00",
	"geographic_areas": [],
	"id": "22222222-0e5d-4a14-8de1-7f423571fb3e",
	"integrations": [],
	"latest_status_change_at": "2023-06-30T13:31:26+02:00",
	"latest_status_change_time": 1688124686,
	"links": [
		{
			"action": "register",
			"description": "Register Application",
			"url": "https://www2.test.it/comune-di-seriate/api/applications/22222222-0e5d-4a14-8de1-7f423571fb3e/transition/register?version=1"
		},
		{
			"action": "assign",
			"description": "Assign Application",
			"url": "https://www2.test.it/comune-di-seriate/api/applications/22222222-0e5d-4a14-8de1-7f423571fb3e/transition/assign?version=1"
		}
	],
	"meetings": [],
	"operator_id": null,
	"outcome": null,
	"outcome_attachments": [],
	"outcome_file": null,
	"outcome_motivation": null,
	"outcome_protocol_document_id": null,
	"outcome_protocol_number": null,
	"outcome_protocol_numbers": null,
	"outcome_protocol_time": null,
	"outcome_protocolled_at": null,
	"path": "/applications",
	"payment_data": [],
	"payment_type": null,
	"protocol_document_id": null,
	"protocol_folder_code": null,
	"protocol_folder_number": null,
	"protocol_number": null,
	"protocol_numbers": [],
	"protocol_time": null,
	"protocolled_at": null,
	"service": "richiesta-del-servizio-di-assistenza-specialistica-per-lautonomia-e-la-comunicazione-personale-a-s-2023-2024",
	"service_group_name": null,
	"service_id": "f542da62-9796-419c-bcc6-d309c3fdeb4a",
	"service_name": "Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024",
	"source_type": "http",
	"status": "2000",
	"status_name": "status_submitted",
	"subject": "Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024 Coccorita Coccorico CCCCCC73C50F158S",
	"submission_time": 1688124667,
	"submitted_at": "2023-06-30T13:31:07+02:00",
	"tenant": "N12345-c90c-4f30-8d92-N12345",
	"tenant_id": "N12345-c90c-4f30-8d92-N12345",
	"timestamp": "2023-06-30T11:31:26.964642084Z",
	"user": "a717b215-4277-4c9f-a056-4d8948c2e33e",
	"user_compilation_notes": null,
	"user_group_id": null,
	"user_name": "Coccorita Coccorico",
	"x-forwarded-for": "172.31.66.208",
	"__registry_metadata": {
		"attempts": 1,
		"retry_at": "2023-06-30T11:46:21.746820+00:00",
		"retry_topic": "retry_10_production"
	}
}`

const INPUT_APPLICATION_STATUS_7000 = `{
	"app_id": "symfony-core:2.17.0",
	"app_version": "2.17.0",
	"attachments": [
		{
			"created_at": "2023-04-04T17:05:32+00:00",
			"description": "Allegato",
			"external_id": null,
			"id": "0be6cb18-b427-42c9-bc90-aa54dfa592f4",
			"name": "642c3cbc2dbf9394256919.png",
			"originalName": "1delegaaccessoatti.png-a75dd822-95cf-443a-8307-0b53157e3dc4.png",
			"protocol_number": null,
			"protocol_required": true,
			"url": "https://www2.test.it/comune-di-nomecomune/api/applications/234524gg-3050-42c0-98c0-234524gg/attachments/0be6cb18-b427-42c9-bc90-aa54dfa592f4?version=1"
		},
		{
			"created_at": "2023-04-04T17:05:33+00:00",
			"description": "pdf",
			"external_id": null,
			"id": "0a235502-0043-4017-9448-234524gg",
			"name": "642c3cbd0d183521566879.png",
			"originalName": "2delegaaccessoatti.png-234524gg-0b88-4854-b123-55bc25f3632b.png",
			"protocol_number": null,
			"protocol_required": true,
			"url": "https://www2.test.it/comune-di-nomecomune/api/applications/234524gg-3050-42c0-98c0-234524gg/attachments/0a235502-0043-4017-9448-234524gg?version=1"
		}
	],
	"authentication": {
		"authentication_method": "spid",
		"certificate": null,
		"certificate_issuer": null,
		"certificate_subject": null,
		"instant": "2023-04-04T17:04:34+0200",
		"session_id": "22222222",
		"session_index": "22222222",
		"spid_code": "AIDP0002393477",
		"spid_level": "2"
	},
	"backoffice_data": null,
	"compiled_modules": [
		{
			"created_at": "2023-04-04T17:06:59+00:00",
			"description": "Modulo Richiesta di accesso agli atti in materia edilizia 2023-04-04 05:08",
			"external_id": null,
			"id": "23b88d03-0e17-4542-97c9-0108780ff906",
			"name": "642c3d0a10d58.pdf",
			"originalName": "modulo-richiesta-di-accesso-agli-atti-in-materia-edilizia-202304040508.pdf",
			"protocol_number": null,
			"protocol_required": true,
			"url": "https://www2.test.it/comune-di-nomecomune/api/applications/234524gg-3050-42c0-98c0-234524gg/attachments/23b88d03-0e17-4542-97c9-0108780ff906?version=1"
		}
	],
	"created_at": "2023-04-04T15:04:34+00:00",
	"creation_time": 1680620674,
	"data": {
		"applicant.data.Born.data.natoAIl": "1986-07-14T00:00:00+00:00",
		"applicant.data.Born.data.place_of_birth": "Pisa",
		"applicant.data.address.data.address": "Via San N12345 ",
		"applicant.data.address.data.county": "PI",
		"applicant.data.address.data.house_number": "31",
		"applicant.data.address.data.municipality": "Pisa",
		"applicant.data.address.data.postal_code": "56125",
		"applicant.data.completename.data.name": "Nome",
		"applicant.data.completename.data.surname": "Cantini",
		"applicant.data.email_address": "bcantini@live.it",
		"applicant.data.fiscal_code.data.fiscal_code": "XXXXX86L54G702A",
		"applicant.data.gender.data.gender": "femmina",
		"checkbox": false,
		"declaration.copia_autentica": false,
		"declaration.simple_copy": true,
		"documents": "Concessione n.7 del 22.02.2001 intestata a Nome Silvia XXXXX",
		"for_whom": "other",
		"id_card_person_in-charge1": [
			{
				"description": "pdf",
				"id": "0a235502-0043-4017-9448-234524gg",
				"name": "DelegaAccessoAtti.png-234524gg-0b88-4854-b123-55bc25f3632b.png",
				"originalName": "DelegaAccessoAtti.png.png",
				"protocol_required": true,
				"url": "https://www2.test.it/comune-di-nomecomune/api/applications/234524gg-3050-42c0-98c0-234524gg/attachments/0a235502-0043-4017-9448-234524gg?version=1"
			}
		],
		"in_charge": "professionistaIncaricato",
		"parcels.0.pEd": "",
		"parcels.0.pf": "20",
		"parcels.0.pm": "302",
		"parcels.0.sub": "",
		"payment_amount": 27,
		"pecACuiInviareLeComunicazioni": "Nome.cantini@geopec.it",
		"privacy_policy_check": true,
		"proxy1": [
			{
				"description": "Allegato",
				"id": "0be6cb18-b427-42c9-bc90-aa54dfa592f4",
				"name": "DelegaAccessoAtti.png-a75dd822-95cf-443a-8307-0b53157e3dc4.png",
				"originalName": "DelegaAccessoAtti.png.png",
				"protocol_required": true,
				"url": "https://www2.test.it/comune-di-nomecomune/api/applications/234524gg-3050-42c0-98c0-234524gg/attachments/0be6cb18-b427-42c9-bc90-aa54dfa592f4?version=1"
			}
		],
		"reason": "Recupero numero autorizzazione paesaggistica per successiva nuova richiesta",
		"request_birth.data.natoAIl": "1965-03-19T00:00:00+00:00",
		"request_birth.data.place_of_birth": "Catania",
		"request_name.data.name": "Nome Silvia",
		"request_name.data.surname": "XXXXX"
	},
	"event_created_at": "2023-05-21T22:35:25+00:00",
	"event_id": "466313ed-5239-413e-97f3-59731e3058ee",
	"event_version": 2,
	"external_id": null,
	"flow_changed_at": "2023-05-21T22:35:25+00:00",
	"geographic_areas": [],
	"id": "234524gg-3050-42c0-98c0-234524gg",
	"integrations": [],
	"latest_status_change_at": "2023-05-21T22:35:25+00:00",
	"latest_status_change_time": 1684708525,
	"links": [
		{
			"action": "withdraw",
			"description": "Withdraw Application",
			"url": "https://www2.test.it/comune-di-nomecomune/api/applications/234524gg-3050-42c0-98c0-234524gg/transition/withdraw?version=1"
		}
	],
	"meetings": [],
	"outcome": true,
	"outcome_attachments": [
		{
			"created_at": "2023-04-04T17:05:32+00:00",
			"description": "Allegato",
			"external_id": null,
			"id": "0be6cb18-b427-42c9-bc90-aa54dfa592f4",
			"name": "allegato1.png",
			"originalName": "allegato1originalname.png",
			"protocol_number": null,
			"protocol_required": true,
			"url": "url primo allegato"
		},
		{
			"created_at": "2023-04-04T17:05:33+00:00",
			"description": "pdf",
			"external_id": null,
			"id": "0a235502-0043-4017-9448-234524gg",
			"name": "allegato2.png",
			"originalName": "allegato2originalname.png",
			"protocol_number": null,
			"protocol_required": true,
			"url": "url secondo allegato"
		}
	],
	"outcome_file": {
		"created_at": "2023-05-17T10:03:11+00:00",
		"description": "outcomedescription",
		"external_id": null,
		"id": "b86defa1-57d7-476d-995a-00da46c7bd97",
		"name": "64648a3cdb514.pdf",
		"originalName": "outcomeFile originalName.pdf",
		"protocol_number": null,
		"protocol_required": true,
		"url": "outcomeFile url"
	},
	"outcome_motivation": "<p><br></p><div>\r\n\r\n\t\r\n\t\r\n\t\r\n\t\r\n<!--\r\n\t\t@page { margin: 0.79in }\r\n\t\tP { margin-bottom: 0.08in }\r\n\t-->\r\n\t\r\n\r\n\r\n<p align=\"JUSTIFY\">Buongiorno,</p>\r\n<p align=\"JUSTIFY\">con\r\nla presente sono a comunicare\r\nl'importo dovuto per diritti di segreteria in relazione alla\r\nrichiesta di accesso agli atti prot. n. 5250 del 04/04/2023.</p>\r\n<p align=\"JUSTIFY\">L'importo\r\nstabilito per ricerche di archivio in modalità telematica e\r\nfornitura di copie digitali integrali è pari ad € 27,00 per ogni\r\npratica reperita.</p>\r\n<p align=\"JUSTIFY\"><span style=\"font-family: &quot;Titillium Web&quot;, Geneva, Tahoma, sans-serif; text-align: left;\">Per\r\nquanto sopra, l'importo dovuto è di :</span><br></p>\r\n<p align=\"JUSTIFY\">€\r\n27,00\r\nx   n. 1 pratiche reperite = € 27,00</p><p align=\"JUSTIFY\"><span style=\"font-family: &quot;Titillium Web&quot;, Geneva, Tahoma, sans-serif; text-align: left;\">Cordiali\r\nsaluti</span><br></p></div><div>\r\n<p align=\"RIGHT\"><i style=\"font-family: &quot;Titillium Web&quot;, Geneva, Tahoma, sans-serif; text-align: left;\">L'istruttore\r\nAmministrativo</i><br></p>\r\n<p align=\"RIGHT\"><i>Sig.\r\nCristina Batoni</i>\r\n</p>\r\n<p align=\"RIGHT\"><br>\r\n</p>\r\n\r\n</div><div><p align=\"JUSTIFY\"> \r\n</p>\r\n\r\n</div>",
	"outcome_protocol_document_id": null,
	"outcome_protocol_number": null,
	"outcome_protocol_numbers": [],
	"outcome_protocol_time": null,
	"outcome_protocolled_at": null,
	"path": "/applications",
	"payment_data": {
		"amount": "27",
		"expire_at": "2023-08-15T10:03:13+02:00",
		"landing": {
			"method": "GET",
			"url": "https://www2.test.it/comune-di-nomecomune/it/pratiche/234524gg-3050-42c0-98c0-234524gg/detail"
		},
		"notify": {
			"method": "POST",
			"url": "https://www2.test.it/comune-di-nomecomune/api/applications/234524gg-3050-42c0-98c0-234524gg/payment"
		},
		"reason": "234524gg-3050-42c0-98c0-234524gg - XXXXX86L54G702A",
		"split": null
	},
	"payment_type": "iris",
	"protocol_document_id": "1204737",
	"protocol_folder_code": null,
	"protocol_folder_number": null,
	"protocol_number": "5250",
	"protocol_numbers": [
		{
			"id": "23b88d03-0e17-4542-97c9-0108780ff906",
			"protocollo": "5250"
		},
		{
			"id": "0a235502-0043-4017-9448-234524gg",
			"protocollo": "5250"
		},
		{
			"id": "0be6cb18-b427-42c9-bc90-aa54dfa592f4",
			"protocollo": "5250"
		}
	],
	"protocol_time": 1680620826,
	"protocolled_at": "2023-04-04T15:07:06+00:00",
	"service": "richiesta-di-accesso-agli-atti-in-materia-edilizia",
	"service_group_name": "Edilizia",
	"service_id": "9e35b776-e029-47e2-a08d-bdd659be4e52",
	"service_name": "Richiesta di accesso agli atti in materia edilizia",
	"source_type": "http",
	"status": "7000",
	"status_name": "status_complete",
	"subject": "Richiesta di accesso agli atti in materia edilizia Nome Cantini XXXXX86L54G702A",
	"submission_time": 1680620787,
	"submitted_at": "2023-04-04T15:06:27+00:00",
	"tenant": "285dff7a-090d-473c-a076-9e54547aa1a6",
	"tenant_id": "285dff7a-090d-473c-a076-9e54547aa1a6",
	"timestamp": "2023-05-21T22:35:25.412894398Z",
	"user": "f9e3a089-3f13-4509-972e-d4976d7f8984",
	"user_compilation_notes": null,
	"user_name": "Nome Cantini",
	"x-forwarded-for": "172.31.52.142",
	"__registry_metadata": {
		"attempts": 4,
		"retry_at": "2023-05-21T23:55:54.537801+00:00",
		"retry_topic": "retry_30_production"
	}
}`

const INPUT_APPLICATION_STATUS_20000 = `{
	"app_id": "symfony-core:2.20.4",
	"app_version": "2.20.4",
	"attachments": [
		{
			"created_at": "2023-06-30T13:29:20+02:00",
			"description": "Allegato1",
			"external_id": null,
			"id": "6e3ddbb6-5cad-4165-9a74-N12345",
			"name": "1.PDF",
			"originalName": "verbale-1.pdf",
			"protocol_number": null,
			"protocol_required": true,
			"url": "https://www2.test.it/1"
		},
		{
			"created_at": "2023-06-30T13:29:32+02:00",
			"description": "Allegato2",
			"external_id": null,
			"id": "d8a4a561-1e10-46ab-91f4-XXXXX",
			"name": "2.PDF",
			"originalName": "diagnosi2.pdf",
			"protocol_number": null,
			"protocol_required": true,
			"url": "https://www2.test.it/2"
		},
		{
			"created_at": "2023-06-30T13:55:05+02:00",
			"description": "Ritiro pratica Richiesta del servizio3",
			"external_id": null,
			"id": "f8beaf17-aade-472d-bb3d-a9e844f15588",
			"name": "3.pdf",
			"originalName": "ritiro-pratica-richiesta-del-servizio3.pdf",
			"protocol_number": null,
			"protocol_required": true,
			"url": "https://www2.test.it/3"
		}
	],
	"authentication": {
		"authentication_method": "spid",
		"certificate": null,
		"certificate_issuer": null,
		"certificate_subject": null,
		"instant": "2023-06-30T12:28:51+0200",
		"session_id": "XXXXX",
		"session_index": "XXXXX",
		"spid_code": "XXXXXUS4QXAK0",
		"spid_level": "2"
	},
	"backoffice_data": null,
	"compiled_modules": [
		{
			"created_at": "2023-06-30T13:40:32+02:00",
			"description": "Modulo Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024 2023-06-30 01:40",
			"external_id": null,
			"id": "e28dc9f3-2e6c-4712-bc8e-XXXXX",
			"name": "649ebf27430f1.pdf",
			"originalName": "modulo-richiesta-del-servizio-di-assistenza-specialistica-per-lautonomia-e-la-comunicazione-personale-a.s.20232024-202306300140.pdf",
			"protocol_number": null,
			"protocol_required": true,
			"url": "https://www2.test.it/comune-di-seriate/api/applications/5c3adff7-bf23-4201-95a3-N12345/attachments/e28dc9f3-2e6c-4712-bc8e-XXXXX?version=1"
		}
	],
	"created_at": "2023-06-30T13:25:01+02:00",
	"creation_time": 1688124301,
	"data": {
		"Diagnosi_funzionale": [
			{
				"description": "Allegato",
				"id": "d8a4a561-1e10-46ab-91f4-XXXXX",
				"name": "Diagnosi XXXXX-d5aa18ce-67eb-4c10-8d89-e07f5e78c564.PDF",
				"originalName": "Diagnosi XXXXX.PDF",
				"protocol_required": true,
				"url": "https://www2.test.it/comune-di-seriate/api/applications/5c3adff7-bf23-4201-95a3-N12345/attachments/d8a4a561-1e10-46ab-91f4-XXXXX?version=1"
			}
		],
		"Diagnosi_funzionale2": [],
		"RichiestaSiNo.no": true,
		"RichiestaSiNo.si": false,
		"accettazioneDichiarazioneDiResponsabilta6": true,
		"accettazioneDichiarazioneDiResponsabilta7": true,
		"annoScolastico": "2023/2024",
		"applicant.data.completename.data.name": "Nome",
		"applicant.data.completename.data.surname": "XXXXX",
		"applicant.data.fiscal_code.data.fiscal_code": "NXXXXXX66H70L331N",
		"codiceMeccanograficoIstituto": "BGIC875006",
		"dataCertificazione": "18/04/2023",
		"dataCertificazione1": "28/11/2022",
		"dellistitutoScolastico": "I.C. BATTISTI - SERIATE",
		"eMailItstituto": "bgiwert5006@istruzione.it",
		"emailGenitore": "NXXXXXX@gmail.com",
		"eventualiNote1": "",
		"eventualiNote2": "",
		"finoAl": "AL TERMINE DELLA SCUOLA PRIMARIA",
		"form1.data.name": "Nome",
		"form1.data.surname": "NXXXXXX",
		"form2.data.natoAIl": "2017-12-12T00:00:00+01:00",
		"form2.data.place_of_birth": "BERGAMO",
		"form3.data.fiscal_code": "XXXXXX17T52A794R",
		"frequentanteListututo": "I.C. BATTISTI - SERIATE",
		"frequentanteListututo1": "RODARI",
		"frequentanteListututo2": "A",
		"frequentanteListututo3": "1",
		"inQualitaDi": "delegatoDirigente",
		"oreSettimanali": "10-12",
		"recapitoTelefonico": "3203099841",
		"recapitoTelefonicoIstituto": "035294016",
		"referenteInsegnanti": "Cognome Nome",
		"select": "scuolaPrimaria",
		"specialistaDiRiferimento": "Cognome Nome",
		"textField": "27",
		"verbale_situazioneh": [
			{
				"description": "Allegato",
				"id": "6e3ddbb6-5cad-4165-9a74-N12345",
				"name": "Verbale XXXXX-85e30a64-890d-405b-964b-N12345.PDF",
				"originalName": "Verbale XXXXX.PDF",
				"protocol_required": true,
				"url": "https://www2.test.it/comune-di-seriate/api/applications/5c3adff7-bf23-4201-95a3-N12345/attachments/6e3ddbb6-5cad-4165-9a74-N12345?version=1"
			}
		]
	},
	"event_created_at": "2023-06-30T13:55:05+02:00",
	"event_id": "ae54c667-f23c-469d-a7ab-d005446179b8",
	"event_version": 2,
	"external_id": null,
	"flow_changed_at": "2023-06-30T13:55:05+02:00",
	"geographic_areas": [],
	"id": "5c3adff7-bf23-4201-95a3-N12345",
	"integrations": [],
	"latest_status_change_at": "2023-06-30T13:55:05+02:00",
	"latest_status_change_time": 1688126105,
	"links": [],
	"meetings": [],
	"operator_id": null,
	"outcome": null,
	"outcome_attachments": [],
	"outcome_file": null,
	"outcome_motivation": null,
	"outcome_protocol_document_id": null,
	"outcome_protocol_number": null,
	"outcome_protocol_numbers": null,
	"outcome_protocol_time": null,
	"outcome_protocolled_at": null,
	"path": "/applications",
	"payment_data": [],
	"payment_type": null,
	"protocol_document_id": null,
	"protocol_folder_code": null,
	"protocol_folder_number": null,
	"protocol_number": null,
	"protocol_numbers": [],
	"protocol_time": null,
	"protocolled_at": null,
	"service": "richiesta-del-servizio-di-assistenza-specialistica-per-lautonomia-e-la-comunicazione-personale-a-s-2023-2024",
	"service_group_name": null,
	"service_id": "f542da62-9796-419c-bcc6-d309c3fdeb4a",
	"service_name": "Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024",
	"source_type": "http",
	"status": "20000",
	"status_name": "status_withdraw",
	"subject": "Richiesta del servizio di assistenza specialistica per l'autonomia e la comunicazione personale a.s.2023/2024 Nome XXXXX NXXXXXX66H70L331N",
	"submission_time": 1688124691,
	"submitted_at": "2023-06-30T13:31:31+02:00",
	"tenant": "N12345-c90c-4f30-8d92-N12345",
	"tenant_id": "N12345-c90c-4f30-8d92-N12345",
	"timestamp": "2023-06-30T11:55:05.318433997Z",
	"user": "a7445139-e377-454c-9372-5cab3dad95a6",
	"user_compilation_notes": null,
	"user_group_id": null,
	"user_name": "Nome XXXXX",
	"x-forwarded-for": "172.31.52.142",
	"__registry_metadata": {
		"attempts": 13,
		"retry_at": "2023-07-03T02:15:47.546939+00:00",
		"retry_topic": "retry_720_production"
	}
}`

const INPUT_APPLICATION_STATUS_4100 = `{
	"app_id": "symfony-core:2.17.2",
	"app_version": "2.17.2",
	"attachments": [],
	"authentication": {
		"authentication_method": "spid",
		"certificate": null,
		"certificate_issuer": null,
		"certificate_subject": null,
		"instant": "2023-05-30T16:45:09.606Z",
		"session_id": "N12345",
		"session_index": "N12345-1HQXd_M",
		"spid_code": "AIDP0000904643",
		"spid_level": null
	},
	"backoffice_data": null,
	"compiled_modules": [
		{
			"created_at": "2023-05-30T19:13:04+02:00",
			"description": "Modulo SCIA Pratica Edilizia v2 2023-05-30 07:16",
			"external_id": null,
			"id": "de653144-b5f3-42cd-bbe5-N12345",
			"name": "64762e92bf0fd.pdf",
			"originalName": "modulo-scia-pratica-edilizia-v2-N12345.pdf",
			"protocol_number": null,
			"protocol_required": true,
			"url": "https://www2.test.it/comune-di-test/api/applications/f578b6b2-c6a7-4545-8ade-123456/attachments/de653144-b5f3-42cd-bbe5-N12345?version=1"
		}
	],
	"created_at": "2023-05-24T16:10:29+02:00",
	"creation_time": 1684937429,
	"data": {
		"DOM_CI": [],
		"applicant.data.Born.data.natoAIl": "1973-12-21T00:00:00+01:00",
		"applicant.data.Born.data.place_of_birth": "Trento",
		"applicant.data.address.data.address": "via San N12345",
		"applicant.data.address.data.county": "TN",
		"applicant.data.address.data.house_number": "66",
		"applicant.data.address.data.municipality": "Trento",
		"applicant.data.address.data.postal_code": "38121",
		"applicant.data.completename.data.name": "Nome",
		"applicant.data.completename.data.surname": "Cognome",
		"applicant.data.email_address": "Nome_Cognome@libero.it",
		"applicant.data.fiscal_code.data.fiscal_code": "XXXXXX73T21L378S",
		"applicant.data.gender.data.gender": "maschio",
		"cadastral_parcels.0.comuneCatastale": 105,
		"cadastral_parcels.0.numero": "77",
		"cadastral_parcels.0.pm": "2",
		"cadastral_parcels.0.subalterno": "1",
		"cadastral_parcels.0.tipo": "E",
		"checkbox": true,
		"checkbox1": true,
		"checkbox7": false,
		"checkbox8": true,
		"checkbox9": true,
		"chkSpaziParcheggio": false,
		"figureProfessionali.0.alboProfessionale": "Geometri",
		"figureProfessionali.0.cap": "38121",
		"figureProfessionali.0.citta": "Trento",
		"figureProfessionali.0.civico": "66",
		"figureProfessionali.0.codiceFiscale": "XXXXXX73T21L378S",
		"figureProfessionali.0.cognome": "Cognome",
		"figureProfessionali.0.dataDiNascita": "21/12/1973",
		"figureProfessionali.0.indirizzo": "via San N12345",
		"figureProfessionali.0.luogoDiNascita": "Trento",
		"figureProfessionali.0.nome": "Nome",
		"figureProfessionali.0.numeroAlbo": "1888",
		"figureProfessionali.0.partitaIva": "",
		"figureProfessionali.0.provincia": "TN",
		"figureProfessionali.0.provinciaAlbo": "TN",
		"figureProfessionali.0.ruolo": "PROGETTISTA",
		"oggettoDeiLavori": "Cambio destinazione d'uso da bar ad abitazione",
		"richiedenti.0.cap": "38076",
		"richiedenti.0.citta": "Madruzzo",
		"richiedenti.0.civico": "3/b",
		"richiedenti.0.codiceFiscale": "AAAA98C29D530J",
		"richiedenti.0.cognome": "Pauletto",
		"richiedenti.0.dataDiNascita1": "29/03/1998",
		"richiedenti.0.indirizzo": "via Castel Madruzzo",
		"richiedenti.0.luogoDiNascita": "Feltre",
		"richiedenti.0.nome": "Daniel",
		"richiedenti.0.partitaIva": "",
		"richiedenti.0.provincia": "TN",
		"richiedenti.0.ruolo": "Comproprietario",
		"richiedenti.1.cap": "38076",
		"richiedenti.1.citta": "Madruzzo",
		"richiedenti.1.civico": "3/b",
		"richiedenti.1.codiceFiscale": "XXXXXX98P55A462F",
		"richiedenti.1.cognome": "Cognome",
		"richiedenti.1.dataDiNascita1": "15/09/1998",
		"richiedenti.1.indirizzo": "via Castel Madruzzo",
		"richiedenti.1.luogoDiNascita": "Ascoli Piceno",
		"richiedenti.1.nome": "Gaia",
		"richiedenti.1.partitaIva": "",
		"richiedenti.1.provincia": "TN",
		"richiedenti.1.ruolo": "Comproprietario",
		"scia_pratica_edilizia": [
			{
				"description": "Allegato",
				"id": "123456-f10d-4bd0-adc8-94848c8559ad",
				"name": "SCIA Cambio d-uso firmata.pdf-aeb25895-22da-4176-a8ed-123456",
				"originalName": "SCIA Cambio d'uso firmata.pdf",
				"protocol_required": true,
				"url": "https://www2.test.it/comune-di-test/api/applications/f578b6b2-c6a7-4545-8ade-123456/attachments/123456-f10d-4bd0-adc8-94848c8559ad?version=1"
			}
		],
		"tipoDiIntervento": "ristrutturazione_edilizia"
	},
	"event_created_at": "2023-06-06T11:30:08+02:00",
	"event_id": "85b7688d-e6dd-479c-9467-123456",
	"event_version": 2,
	"external_id": null,
	"flow_changed_at": "2023-06-06T11:30:07+02:00",
	"geographic_areas": [],
	"id": "f578b6b2-c6a7-4545-8ade-123456",
	"integrations": [
		{
			"inbound": null,
			"outbound": {
				"attachments": [
					{
						"created_at": "2023-06-06T11:30:04+02:00",
						"description": "1 description 1",
						"external_id": null,
						"id": "1 id 1",
						"name": "1 name1.pdf",
						"originalName": "1 originalName1.pdf",
						"protocol_number": null,
						"protocol_required": true,
						"url": "1 url1"
					},
					{
						"created_at": "2023-06-06T11:30:05+02:00",
						"description": "1 description 2",
						"external_id": null,
						"id": "1 id 2",
						"name": "1 name2.pdf",
						"originalName": "1 originalName2.pdf",
						"protocol_number": null,
						"protocol_required": true,
						"url": "1 url2"
					}
				],
				"created_at": "2023-06-06T11:30:08+04:00",
				"description": "description 1",
				"external_id": null,
				"id": "id 1",
				"name": "name 1.xxx",
				"originalName": "original name 1.xxx",
				"protocol_number": null,
				"protocol_required": true,
				"url": "url 1"
			}
		},
		{
			"inbound": null,
			"outbound": {
				"attachments": [
					{
						"created_at": "2023-06-06T11:30:04+02:00",
						"description": "2 description 1",
						"external_id": null,
						"id": "2 id 1",
						"name": "2 name1.pdf",
						"originalName": "2 originalName1.pdf",
						"protocol_number": null,
						"protocol_required": true,
						"url": "2 url1"
					},
					{
						"created_at": "2023-06-06T11:30:05+02:00",
						"description": "2 description 2",
						"external_id": null,
						"id": "2 id 2",
						"name": "2 name2.pdf",
						"originalName": "2 originalName2.pdf",
						"protocol_number": null,
						"protocol_required": true,
						"url": "2 url2"
					}
				],
				"created_at": "2023-06-06T11:30:09+02:00",
				"description": "description 2",
				"external_id": null,
				"id": "id 2 ",
				"name": "name2.png",
				"originalName": "original name 2.png",
				"protocol_number": null,
				"protocol_required": true,
				"url": "url2"
			}
		}
		,
		{
			"inbound": null,
			"outbound": {
				"attachments": [
					{
						"created_at": "2023-06-06T11:30:04+02:00",
						"description": "3 description 1",
						"external_id": null,
						"id": "3 id 1",
						"name": "3 name1.pdf",
						"originalName": "3 originalName1.pdf",
						"protocol_number": null,
						"protocol_required": true,
						"url": "3 url1"
					},
					{
						"created_at": "2023-06-06T11:30:05+02:00",
						"description": "3 description 2",
						"external_id": null,
						"id": "id 2",
						"name": "3 name2.pdf",
						"originalName": "3 originalName2.pdf",
						"protocol_number": null,
						"protocol_required": true,
						"url": "3 url2"
					},
					{
						"created_at": "2023-06-06T11:30:05+02:00",
						"description": "3 description 3",
						"external_id": null,
						"id": "3 id 3",
						"name": "3 name3.pdf",
						"originalName": "3 originalName3.pdf",
						"protocol_number": null,
						"protocol_required": true,
						"url": "3 url3"
					}
				],
				"created_at": "2023-06-06T11:30:53+02:00",
				"description": "description 3",
				"external_id": null,
				"id": "id 3 ",
				"name": "name3.pdf",
				"originalName": "original name 3.pdf",
				"protocol_number": null,
				"protocol_required": true,
				"url": "url3"
			}
		}
	],
	"latest_status_change_at": "2023-06-06T11:30:07+02:00",
	"latest_status_change_time": 1686043807,
	"links": [
		{
			"action": "register-integration-request",
			"description": "Register integration request",
			"url": "https://www2.test.it/comune-di-test/api/applications/f578b6b2-c6a7-4545-8ade-123456/transition/register-integration-request?version=1"
		}
	],
	"meetings": [],
	"outcome": null,
	"outcome_attachments": [],
	"outcome_file": null,
	"outcome_motivation": null,
	"outcome_protocol_document_id": null,
	"outcome_protocol_number": null,
	"outcome_protocol_numbers": null,
	"outcome_protocol_time": null,
	"outcome_protocolled_at": null,
	"path": "/applications",
	"payment_data": [],
	"payment_type": null,
	"protocol_document_id": "1076035280",
	"protocol_folder_code": null,
	"protocol_folder_number": null,
	"protocol_number": "5016",
	"protocol_numbers": [
		{
			"id": "de653144-b5f3-42cd-bbe5-N12345",
			"protocollo": "5016"
		},
		{
			"id": "123456-f10d-4bd0-adc8-94848c8559ad",
			"protocollo": "5016"
		},
		{
			"id": "8bbf6d58-301a-4a95-ba76-c9379c4ec4b0",
			"protocollo": "5016"
		},
		{
			"id": "75369cc8-5347-4713-a533-88636671f126",
			"protocollo": "5016"
		},
		{
			"id": "bb7c0ac8-86be-469c-a9f1-92a464a3d2f3",
			"protocollo": "5016"
		},
		{
			"id": "cdaf624a-7c4b-4499-824d-465e4d4eb996",
			"protocollo": "5016"
		},
		{
			"id": "090417a4-6374-4e8c-a253-dab453972725",
			"protocollo": "5016"
		},
		{
			"id": "6c252d77-e9d1-4dd1-9ec9-7157009f9840",
			"protocollo": "5016"
		},
		{
			"id": "c4d13bea-7792-4d50-a26e-8a2c130adebb",
			"protocollo": "5016"
		},
		{
			"id": "bb94bcf7-a777-4dc1-8637-a39fa144a7bb",
			"protocollo": "5016"
		},
		{
			"id": "6860e5c6-73a6-4767-8b59-1ab5e84055ab",
			"protocollo": "5016"
		},
		{
			"id": "0b44acc2-aced-4456-b764-bfc0ccdb36e7",
			"protocollo": "5016"
		},
		{
			"id": "4c69be4e-bb2c-4d59-bc09-05d146d8a4d3",
			"protocollo": "5016"
		},
		{
			"id": "16ae51e1-9676-4a71-9c53-b84c01d9ad1f",
			"protocollo": "5016"
		},
		{
			"id": "79097886-cf5d-4fed-99ca-67e46df4ef6e",
			"protocollo": "5016"
		},
		{
			"id": "562f3883-f454-41f0-9d46-5e164f881f1a",
			"protocollo": "5016"
		},
		{
			"id": "03bb2aad-b94d-4c8d-87f8-985dcf67d1bf",
			"protocollo": "5016"
		},
		{
			"id": "a13770b4-af84-4ae8-af9a-9f034adb0c94",
			"protocollo": "5016"
		},
		{
			"id": "99b4ef3b-d6ab-45eb-961e-f23f7b93e1ee",
			"protocollo": "5016"
		},
		{
			"id": "aa7f3b58-34f6-4b32-ba21-76307c567db4",
			"protocollo": "5016"
		},
		{
			"id": "cb35a603-e233-4a15-9591-7bf6c29e97ce",
			"protocollo": "5016"
		},
		{
			"id": "760249ef-c113-44f5-a597-49e308cd0c71",
			"protocollo": "5016"
		},
		{
			"id": "eabc23ce-6457-4961-a86d-2377f3c6d152",
			"protocollo": "5016"
		},
		{
			"id": "cbb1d1ef-3ae2-4863-82dd-686c7b0b1fcb",
			"protocollo": "5016"
		}
	],
	"protocol_time": 1685466829,
	"protocolled_at": "2023-05-30T19:13:49+02:00",
	"service": "scia-pratica-edilizia-v2",
	"service_group_name": "Pratiche edilizie Online",
	"service_id": "1415f9ae-18f9-4d65-8e03-e67ecd5da41d",
	"service_name": "SCIA Pratica Edilizia v2",
	"source_type": "http",
	"status": "4100",
	"status_name": "status_request_integration",
	"subject": "SCIA Pratica Edilizia v2 Nome Cognome XXXXXX73T21L378S",
	"submission_time": 1685466713,
	"submitted_at": "2023-05-30T19:11:53+02:00",
	"tenant": "123456-9e7f-4b19-81b4-123456",
	"tenant_id": "123456-9e7f-4b19-81b4-123456",
	"timestamp": "2023-06-06T09:30:08.504884480Z",
	"user": "0e0400ef-4aba-4a51-a468-71378d9a609a",
	"user_compilation_notes": null,
	"user_group_id": null,
	"user_name": "Nome Cognome",
	"x-forwarded-for": "172.31.20.9",
	"__registry_metadata": {
		"attempts": 1,
		"retry_at": "2023-06-06T09:40:45.719919+00:00",
		"retry_topic": "retry_10_production"
	}
}`

const INPUT_APPLICATION_STATUS_4300 = `{
	"app_id": "symfony-core:2.17.2",
	"app_version": "2.17.2",
	"attachments": [],
	"authentication": {
		"authentication_method": "spid",
		"certificate": null,
		"certificate_issuer": null,
		"certificate_subject": null,
		"instant": "2022-11-09T18:36:44+test",
		"session_id": "test",
		"session_index": "123456",
		"spid_code": "test",
		"spid_level": "2"
	},
	"backoffice_data": null,
	"compiled_modules": [
		{
			"created_at": "2022-11-09T19:07:25+01:00",
			"description": "Allegato - Modulo Diritto test studio 2022 - Buono libri a. s. 2022/2023 202211090707.pdf",
			"external_id": null,
			"id": "0510ea1e-9842-4bd9-test-123456",
			"name": "636bec5de13df.pdf",
			"originalName": "modulo-diritto-allo-studio-test-buono-test-a.-s.-20222023-202211090707.pdf",
			"protocol_number": null,
			"protocol_required": true,
			"url": "https://www2.test.it/comune-di-sestu/api/test/64450b76-bd1f-4446-bf65-648f9528bc31/test/0510ea1e-9842-4bd9-9d46-123456?version=1"
		}
	],
	"created_at": "2022-11-09T13:05:20+01:00",
	"creation_time": 1667995520,
	"data": {
		"ComunicareTempestivamenteLeNuoeveCoordinateBancarie": true,
		"ComunicazioneDiCambioDiIndirizzo": true,
		"DocumentoDiIdentita": [],
		"accredito": true,
		"allegati": true,
		"anagraficaAltraPersona.data.child_date_of_birth": "12/08/2011",
		"anagraficaAltraPersona.data.child_fiscalcode": "test",
		"anagraficaAltraPersona.data.child_gender": "test",
		"anagraficaAltraPersona.data.child_name": "test",
		"anagraficaAltraPersona.data.child_place_of_birth": "test",
		"anagraficaAltraPersona.data.child_surname": "test",
		"applicant.data.Born.data.natoAIl": "1989-05-16T00:00:00+02:00",
		"applicant.data.Born.data.place_of_birth": "test",
		"applicant.data.address.data.address": "Via test da test 4",
		"applicant.data.address.data.municipality": "test",
		"applicant.data.address.data.postal_code": "test",
		"applicant.data.cell_number": "+393489300756",
		"applicant.data.completename.data.name": "test",
		"applicant.data.completename.data.surname": "test",
		"applicant.data.email_address": "test.mm@gmail.com",
		"applicant.data.fiscal_code.data.fiscal_code": "test",
		"attestazioneIsee2022": [
			{
				"description": "Allegato",
				"id": "381d4684-79c7-411f-8101-bbd11963bab0",
				"name": "isee-test-6bf0-4c22-8c17-f4548b2bcedf.pdf",
				"originalName": "isee.pdf",
				"protocol_required": true,
				"url": "https://www2.test.it/test-di-test/api/applications/64450b76-bd1f-4446-test-test/attachments/381d4684-79c7-411f-8101-bbd11963bab0?version=1"
			}
		],
		"checkbox": true,
		"chiEIlBeneficiario": "test/test del minore:",
		"chiede": true,
		"classeFrequentata": "1 media",
		"codiceIbanEIntestariContoCarta": [
			{
				"description": "Allegato",
				"id": "b709cbe3-2202-4524-bf77-a0a3a6877e3b",
				"name": "20221109_190430-db4d8aff-fbd1-4770-afe6-test.jpg",
				"originalName": "test.jpg",
				"protocol_required": true,
				"url": "https://www2.test.it/comune-di-sestu/api/applications/64450b76-test-4446-bf65-test/attachments/test-2202-4524-bf77-test?version=1"
			}
		],
		"comuneSedeDellaScuola": "Sestu",
		"conto_corrente.data.IBAN": "IT61B3608105138249244749262",
		"conto_corrente.data.banca": "test italiane",
		"conto_corrente.data.intestatario": "test test",
		"dallEnte": "CIA test srl",
		"documentazioneDiSpesa": [
			{
				"description": "Allegato",
				"id": "819a6240-b63c-451b-a72e-560fb133dcc3",
				"name": "spesa-test-f04c-4cae-b547-test.pdf",
				"originalName": "spesa.pdf",
				"protocol_required": true,
				"url": "https://www2.test.it/comune-di-sestu/api/test/64450b76-test-4446-test-648f9528bc31/test/819a6240-b63c-test-a72e-560fb133dcc3?version=1"
			}
		],
		"euro": 5312.52,
		"eventualiNote": "",
		"invioDiOgniComunicazione": true,
		"isee_data": "24/02/2022",
		"nomeDellaScuola": "test test",
		"quietanzaDiretta": false,
		"scuola.1/2 anno scuola secondaria di secondo grado (I fascia)": false,
		"scuola.3/4/5 anno scuola secondaria di secondo grado (II fascia) ": false,
		"scuola.secondaria di primo grado (I fascia)": true,
		"sdcfile": [],
		"sdcfile1": [],
		"sdcfile2": [],
		"sdcfile3": [],
		"sdcfile4": [],
		"sdcfile5": [],
		"sdcfile6": [],
		"sdcfile7": [],
		"spesa": 180
	},
	"event_created_at": "2023-06-09T13:50:54+02:00",
	"event_id": "f688d904-c019-4ce5-82e0-364391505a6c",
	"event_version": 2,
	"external_id": null,
	"flow_changed_at": "2023-06-09T13:50:54+02:00",
	"geographic_areas": [],
	"id": "64450b76-bd1f-4446-bf65-648f9528bc31",
	"integrations": [
		{
			"inbound": {
				"attachments": [
					{
						"created_at": "2023-06-06T11:30:04+02:00",
						"description": "1 description 1",
						"external_id": null,
						"id": "1 id 1",
						"name": "1 name1.pdf",
						"originalName": "1 originalName1.pdf",
						"protocol_number": null,
						"protocol_required": true,
						"url": "1 url1"
					},
					{
						"created_at": "2023-06-06T11:30:05+02:00",
						"description": "1 description 2",
						"external_id": null,
						"id": "1 id 2",
						"name": "1 name2.pdf",
						"originalName": "1 originalName2.pdf",
						"protocol_number": null,
						"protocol_required": true,
						"url": "1 url2"
					}
				],
				"created_at": "2023-06-11T13:50:54+02:00",
				"description": "1 description",
				"external_id": null,
				"id": "1 id",
				"name": "1 name.pdf",
				"originalName": "1 originalName.pdf",
				"protocol_number": null,
				"protocol_required": true,
				"url": "1 url"
			},
			"outbound": {}
		},
		{
			"inbound": {
				"attachments": [
					{
						"created_at": "2023-06-06T11:30:04+02:00",
						"description": "2 description 1",
						"external_id": null,
						"id": "2 id 1",
						"name": "2 name1.pdf",
						"originalName": "2 originalName1.pdf",
						"protocol_number": null,
						"protocol_required": true,
						"url": "2 url1"
					},
					{
						"created_at": "2023-06-06T11:30:05+02:00",
						"description": "2 description 2",
						"external_id": null,
						"id": "2 id 2",
						"name": "2 name2.pdf",
						"originalName": "2 originalName2.pdf",
						"protocol_number": null,
						"protocol_required": true,
						"url": "2 url2"
					}
				],
				"created_at": "2023-06-09T13:50:54+02:00",
				"description": "2 description",
				"external_id": null,
				"id": "2 id",
				"name": "2 name.pdf",
				"originalName": "2 originaName.pdf",
				"protocol_number": null,
				"protocol_required": true,
				"url": "2 url"
			},
			"outbound": {}
		}
	],
	"latest_status_change_at": "2023-06-09T13:50:54+02:00",
	"latest_status_change_time": 1686311454,
	"links": [
		{
			"action": "register-integration-answer",
			"description": "Register test answer",
			"url": "test"
		}
	],
	"meetings": [],
	"outcome": null,
	"outcome_attachments": [],
	"outcome_file": null,
	"outcome_motivation": null,
	"outcome_protocol_document_id": null,
	"outcome_protocol_number": null,
	"outcome_protocol_numbers": null,
	"outcome_protocol_time": null,
	"outcome_protocolled_at": null,
	"path": "/applications",
	"payment_data": [],
	"payment_type": null,
	"protocol_document_id": "1899151116",
	"protocol_folder_code": null,
	"protocol_folder_number": null,
	"protocol_number": "41025",
	"protocol_numbers": [
		{
			"id": "0510ea1e-9842-4bd9-9d46-123456",
			"protocollo": "41025"
		},
		{
			"id": "381d4684-79c7-411f-8101-bbd11963bab0",
			"protocollo": "41025"
		},
		{
			"id": "819a6240-b63c-451b-a72e-560fb133dcc3",
			"protocollo": "41025"
		},
		{
			"id": "b709cbe3-2202-4524-bf77-a0a3a6877e3b",
			"protocollo": "41025"
		}
	],
	"protocol_time": 1668017274,
	"protocolled_at": "2022-11-09T19:07:54+01:00",
	"service": "test",
	"service_group_name": "test",
	"service_id": "test",
	"service_name": "test",
	"source_type": "http",
	"status": "4300",
	"status_name": "status_submitted_after_integration",
	"subject": "test",
	"submission_time": 1668017139,
	"submitted_at": "2022-11-09T19:05:39+01:00",
	"tenant": "test-3d8c-test-8d53-test",
	"tenant_id": "feb6e02e-3d8c-4cb9-8d53-eeedd7cc930a",
	"timestamp": "2023-06-test:50:54.768287898Z",
	"user": "b6389b06-c3f0-4f91-9639-d4230194102f",
	"user_compilation_notes": null,
	"user_group_id": null,
	"user_name": "test test",
	"x-forwarded-for": "172.31.71.130",
	"__registry_metadata": {
		"attempts": 2,
		"retry_at": "2023-06-09T12:11:35.544419+00:00",
		"retry_topic": "test"
	}
}
`
