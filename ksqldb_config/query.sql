-- Stream documents
CREATE STREAM IF NOT EXISTS documents_stream  (
    id VARCHAR,
    remote_id VARCHAR,
    type VARCHAR,
    title VARCHAR,
    authors ARRAY<STRUCT<
        type VARCHAR,
        tax_identification_number VARCHAR,
        name VARCHAR,
        family_name VARCHAR,
        street_name VARCHAR,
        building_number VARCHAR,
        postal_code VARCHAR,
        town_name VARCHAR,
        country_subdivision VARCHAR,
        country VARCHAR,
        email VARCHAR,
        role VARCHAR
    >>,
    registration_data STRUCT<
        transmission_type VARCHAR,
        date VARCHAR,
        document_number VARCHAR,
        registry_code VARCHAR    
    >,
    created_at VARCHAR,
    updated_at VARCHAR
) WITH (
    KAFKA_TOPIC='documents',
    KEY_FORMAT='KAFKA',
    PARTITIONS=1,
    VALUE_FORMAT='JSON'
);

--Tabella documents status
CREATE TABLE IF NOT EXISTS documents_status AS
SELECT id,
LATEST_BY_OFFSET(remote_id) AS remote_id,
LATEST_BY_OFFSET(type) AS type,
LATEST_BY_OFFSET(registration_data->transmission_type) AS registration_transmission_type,
PARSE_TIMESTAMP(LATEST_BY_OFFSET(registration_data->date), 'yyyy-MM-dd''T''HH:mm:ss[xxx]') AS registration_data_date,
LATEST_BY_OFFSET(registration_data->document_number) AS registration_document_number,
PARSE_TIMESTAMP(LATEST_BY_OFFSET(created_at), 'yyyy-MM-dd''T''HH:mm:ss[xxx]') AS created_at,
PARSE_TIMESTAMP(LATEST_BY_OFFSET(updated_at), 'yyyy-MM-dd''T''HH:mm:ss[xxx]') AS updated_at
FROM DOCUMENTS_STREAM
GROUP BY id
EMIT CHANGES;

