package sentryutils

import (
	"github.com/getsentry/sentry-go"
)

// general sentry configuration
// release ed evironment questi due valori prenderli da config:envronment si predende da env. la realse si prende da un file di config. leggerlo con viper . se non c'è la versione scrivere "no version"
func SentryInit(dsn, appVersion, environment string) error {
	err := sentry.Init(sentry.ClientOptions{
		Dsn:              dsn,
		TracesSampleRate: 1.0,
		Release:          appVersion,
		Environment:      environment,
		AttachStacktrace: true,
	})
	if err != nil {
		return err
	}
	return nil
}

// clone the main hub for local use into a goroutine
func InitLocalSentryhub() *sentry.Hub {
	localHub := sentry.CurrentHub().Clone()
	localHub.ConfigureScope(func(scope *sentry.Scope) {
		//todo change or remove it?
		scope.SetTag("secretTag", "go#2")
	})
	return localHub
}
