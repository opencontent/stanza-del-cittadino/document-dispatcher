package main

import (
	"context"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/getsentry/sentry-go"
	"go.uber.org/zap"
	"opencitylabs.it/documentdispatcher/config"
	"opencitylabs.it/documentdispatcher/internal/adapters/drivers"
	httpserver "opencitylabs.it/documentdispatcher/internal/adapters/drivers/httpServer"
	"opencitylabs.it/documentdispatcher/internal/adapters/repositories"
	"opencitylabs.it/documentdispatcher/internal/core/services"
	"opencitylabs.it/documentdispatcher/logger"
	"opencitylabs.it/documentdispatcher/sentryutils"
)

func main() {

	config := config.GetConfig()
	logger := logger.GetLogger()
	ctx, exitFn := context.WithCancel(context.Background())
	wg := &sync.WaitGroup{}

	err := sentryutils.SentryInit(config.Sentry.Dsn, config.App.Version, config.App.Environment)
	if err != nil {
		logger.Error("sentry.Init: ", zap.Error(err))
		sentry.CaptureException(err)
	}

	//repositories init
	kafkaProducerRepo := repositories.NewKafkaProducer(ctx)
	ksqlRepo := repositories.NewKsqlRepository()

	DocumentService := services.NewDocumentService(kafkaProducerRepo, ksqlRepo)

	//services init
	kafkaService := services.NewDispatcherService(kafkaProducerRepo, ksqlRepo)
	//drivers init
	server := httpserver.NewHttpDocumentServer(ctx, DocumentService)
	kafkaConsumerDriver := drivers.NewKafkaDocumentConsumer(config.Kafka.KafkaServer, config.Kafka.ConsumerTopic, config.Kafka.ConsumerGroup, kafkaService)

	wg.Add(2)
	go func() {
		defer wg.Done()
		logger.Debug("server listening on " + config.Server.AddressPort)
		server.Run()
	}()
	//core logic entry point
	go func() {
		defer wg.Done()
		kafkaConsumerDriver.ProcessMessage(ctx, config)
	}()

	// gracefully shutdown
	quit := make(chan os.Signal, 1)

	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit

	wg.Wait()

	exitFn()
	sentry.Flush(2 * time.Second)
	_ = logger.Sync()
	// a timeout of 15 seconds to shutdown the server
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	kafkaConsumerDriver.CloseKafkaReader()
	kafkaProducerRepo.CloseKafkaWriter()

	logger.Debug("all processes done, shutting down")

}
