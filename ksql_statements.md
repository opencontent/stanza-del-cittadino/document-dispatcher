```sql
-- Stream documents
CREATE STREAM IF NOT EXISTS documents_stream  (
    id VARCHAR,
    tenant_id VARCHAR,
    remote_id VARCHAR,
    type STRUCT<
        name VARCHAR,
        child STRUCT<
            name VARCHAR
        >
    >,
    collection STRUCT<
        id VARCHAR,
        type VARCHAR
    >,
    title VARCHAR,
    user_id VARCHAR,
    author STRUCT<
        type VARCHAR,
        tax_identification_number VARCHAR,
        name VARCHAR,
        family_name VARCHAR,
        street_name VARCHAR,
        building_number VARCHAR,
        postal_code VARCHAR,
        town_name VARCHAR,
        country_subdivision VARCHAR,
        country VARCHAR,
        email VARCHAR,
        role VARCHAR
    >,
    main_document STRUCT<
        name VARCHAR,
        filename VARCHAR,
        description VARCHAR,
        download_urls ARRAY<
            STRUCT<
                type VARCHAR,
                url VARCHAR
            >
        >
    >,
    attachments ARRAY<
        STRUCT<
            name VARCHAR,
            filename VARCHAR,
            description VARCHAR,
            download_urls ARRAY<
                STRUCT<
                    type VARCHAR,
                    url VARCHAR
                >
            >
        >
    >,
    registration_data STRUCT<
        transmission_type VARCHAR,
        date VARCHAR,
        document_number VARCHAR,
        registry_code VARCHAR    
    >,
    folder STRUCT<
        title VARCHAR,
        id VARCHAR
    >,
    created_at VARCHAR,
    updated_at VARCHAR,
    description VARCHAR,
    short_description VARCHAR,
    document_url VARCHAR,
    responsible_office VARCHAR,
    available_formats VARCHAR,
    distribution_license_id VARCHAR,
    "topics" VARCHAR,
    more_info VARCHAR,
    normative_requirements VARCHAR,
    related_documents VARCHAR,
    related_services VARCHAR,
    life_events VARCHAR,
    business_events VARCHAR,
    image VARCHAR,
    image_gallery VARCHAR,
    version VARCHAR,
    event_id VARCHAR,
    event_version VARCHAR,
    event_created_at VARCHAR,
    app_id VARCHAR,
    visbility VARCHAR
) WITH (
    KAFKA_TOPIC='documents',
    KEY_FORMAT='KAFKA',
    PARTITIONS=24,
    VALUE_FORMAT='JSON'
);


-- Stream documents_attachments e document_attachments_detail
-- Ne servono due per recuperare i link di download innestati nel campo attachments
CREATE STREAM IF NOT EXISTS documents_attachments_stream AS
SELECT id,
remote_id AS remote_id,
main_document AS main_document,
EXPLODE(attachments) AS attachments
FROM DOCUMENTS_STREAM
EMIT CHANGES;

CREATE STREAM IF NOT EXISTS documents_attachments_detail_stream AS
SELECT id,
remote_id AS remote_id,
main_document->name AS main_document_name,
main_document->filename AS main_document_filename,
main_document->description AS main_document_description,
EXPLODE(main_document->download_urls) AS main_document_download_urls,
attachments->name AS attachment_name,
attachments->filename AS attachment_filename,
attachments->description AS attachment_description,
EXPLODE(attachments->download_urls) AS attachment_download_urls
FROM DOCUMENTS_ATTACHMENTS_STREAM
EMIT CHANGES;


--Tabella documents status
CREATE TABLE IF NOT EXISTS documents_status AS
SELECT id,
LATEST_BY_OFFSET(remote_id) AS remote_id,
LATEST_BY_OFFSET(registration_data->transmission_type) AS registration_transmission_type,
PARSE_TIMESTAMP(LATEST_BY_OFFSET(registration_data->date), 'yyyy-MM-dd''T''HH:mm:ss[xxx]') AS registration_data_date,
LATEST_BY_OFFSET(registration_data->document_number) AS registration_document_number,
PARSE_TIMESTAMP(LATEST_BY_OFFSET(created_at), 'yyyy-MM-dd''T''HH:mm:ss[xxx]') AS created_at,
PARSE_TIMESTAMP(LATEST_BY_OFFSET(updated_at), 'yyyy-MM-dd''T''HH:mm:ss[xxx]') AS updated_at
FROM DOCUMENTS_STREAM
GROUP BY id
EMIT CHANGES;

--Tabella documents detail
CREATE TABLE IF NOT EXISTS documents_detail AS
SELECT id,
LATEST_BY_OFFSET(tenant_id) AS tenant_id,
LATEST_BY_OFFSET(remote_id) AS remote_id,
LATEST_BY_OFFSET(type->name) AS type_name,
LATEST_BY_OFFSET(type->child->name) AS type_child_name,
LATEST_BY_OFFSET(collection->id) AS collection_id,
LATEST_BY_OFFSET(collection->type) AS collection_type,
LATEST_BY_OFFSET(title) AS title,
LATEST_BY_OFFSET(user_id) AS user_id,
LATEST_BY_OFFSET(author->type) AS author_type,
LATEST_BY_OFFSET(author->tax_identification_number) AS author_tax_identification_number,
LATEST_BY_OFFSET(author->name) AS author_name,
LATEST_BY_OFFSET(author->family_name) AS author_family_name,
LATEST_BY_OFFSET(author->street_name) AS author_street_name,
LATEST_BY_OFFSET(author->building_number) AS author_building_number,
LATEST_BY_OFFSET(author->postal_code) AS author_postal_code,
LATEST_BY_OFFSET(author->town_name) AS author_town_name,
LATEST_BY_OFFSET(author->country_subdivision) AS author_country_subdivision,
LATEST_BY_OFFSET(author->country) AS author_country,
LATEST_BY_OFFSET(author->email) AS author_email,
LATEST_BY_OFFSET(author->role) AS author_role,
LATEST_BY_OFFSET(main_document->name) AS main_document_name,
LATEST_BY_OFFSET(main_document->filename) AS main_document_filename,
LATEST_BY_OFFSET(main_document->description) AS main_document_description,
LATEST_BY_OFFSET(registration_data->transmission_type) AS registration_data_transmission_type,
PARSE_TIMESTAMP(LATEST_BY_OFFSET(registration_data->date), 'yyyy-MM-dd''T''HH:mm:ss[xxx]') AS registration_data_date,
LATEST_BY_OFFSET(registration_data->document_number) AS registration_data_document_number,
LATEST_BY_OFFSET(registration_data->registry_code) AS registration_data_registry_code,
LATEST_BY_OFFSET(folder->title) AS folder_title,
LATEST_BY_OFFSET(folder->id) AS folder_id,
PARSE_TIMESTAMP(LATEST_BY_OFFSET(created_at), 'yyyy-MM-dd''T''HH:mm:ss[xxx]') AS created_at,
PARSE_TIMESTAMP(LATEST_BY_OFFSET(updated_at), 'yyyy-MM-dd''T''HH:mm:ss[xxx]') AS updated_at,
LATEST_BY_OFFSET(description) AS description,
LATEST_BY_OFFSET(short_description) AS short_description,
LATEST_BY_OFFSET(document_url) AS document_url,
LATEST_BY_OFFSET(responsible_office) AS responsible_office,
LATEST_BY_OFFSET(available_formats) AS available_formats,
LATEST_BY_OFFSET(distribution_license_id) AS distribution_license_id,
LATEST_BY_OFFSET("topics") AS "topics",
LATEST_BY_OFFSET(more_info) AS more_info,
LATEST_BY_OFFSET(normative_requirements) AS normative_requirements,
LATEST_BY_OFFSET(related_documents) AS related_documents,
LATEST_BY_OFFSET(related_services) AS related_services,
LATEST_BY_OFFSET(life_events) AS life_events,
LATEST_BY_OFFSET(business_events) AS business_events,
LATEST_BY_OFFSET(image) AS image,
LATEST_BY_OFFSET(image_gallery) AS image_gallery,
LATEST_BY_OFFSET(version) AS version,
LATEST_BY_OFFSET(event_id) AS event_id,
LATEST_BY_OFFSET(event_version) AS event_version,
PARSE_TIMESTAMP(LATEST_BY_OFFSET(event_created_at), 'yyyy-MM-dd''T''HH:mm:ss[xxx]') AS event_created_at,
LATEST_BY_OFFSET(app_id) AS app_id,
LATEST_BY_OFFSET(visbility) as visbility
FROM DOCUMENTS_STREAM
GROUP BY id
EMIT CHANGES;

--Tabella documents attachments detail
CREATE TABLE IF NOT EXISTS documents_attachments_detail AS
SELECT id,
LATEST_BY_OFFSET(remote_id) AS remote_id,
LATEST_BY_OFFSET(main_document_name) AS main_document_name,
LATEST_BY_OFFSET(main_document_filename) AS main_document_filename,
LATEST_BY_OFFSET(main_document_description) AS main_document_description,
LATEST_BY_OFFSET(main_document_download_urls->type) AS main_document_download_urls_type,
LATEST_BY_OFFSET(main_document_download_urls->url) AS main_document_download_urls_url,
LATEST_BY_OFFSET(attachment_name) AS attachment_name,
LATEST_BY_OFFSET(attachment_filename) AS attachment_filename,
LATEST_BY_OFFSET(attachment_description) AS attachment_description,
LATEST_BY_OFFSET(attachment_download_urls->type) as attachment_download_urls_type,
LATEST_BY_OFFSET(attachment_download_urls->url) as attachment_download_urls_url
FROM DOCUMENTS_ATTACHMENTS_DETAIL_STREAM
GROUP BY id
EMIT CHANGES;

DROP TABLE IF EXISTS documents_detail;
DROP TABLE IF EXISTS documents_attachments_detail;
DROP TABLE IF EXISTS documents_status;
DROP STREAM IF EXISTS documents_attachments_detail_stream;
DROP STREAM IF EXISTS documents_attachments_stream;
DROP STREAM IF EXISTS documents_stream;
```
