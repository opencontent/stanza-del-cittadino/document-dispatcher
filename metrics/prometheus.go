package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	OpsSuccessProcessed = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: "oc_document_success_events_processed_total",
			Help: "The total number of successfully processed events",
		},
		[]string{"env", "tenant"},
	)

	OpsFailedProcessed = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: "oc_document_failed_event_processed_total",
			Help: "The total number of failed processed events",
		},
		[]string{"env", "tenant"},
	)
)
