#!/bin/bash
#descrizione del test

echo "+++ running 1_test.sh"


cat test/1_application.json | jq -c . | kaf produce applications
echo "application prodotta"

sleep 10

output=$(kaf consume documents --limit-messages 1 2>&1)
offset_1=$(echo "$output" | grep -oP 'Offset:\s*\K\d+')
#consumo il primo messaggio e lo metto su 1_document.json
#kaf consume documents --offset $offset_1 --limit-messages 1 > test/1_document.json
kaf consume documents --offset "$offset_1" --limit-messages 1 > test/1_document.json

sed -i '/updated_at/d' test/1_document.json
sed -i '/created_at/d' test/1_document.json
sed -i '/app_id/d' test/1_document.json
sed -i '/event_created_at/d' test/1_document.json
sed -i '/event_id/d' test/1_document.json
sed -i '/id/d' test/1_document.json

sed -i '/updated_at/d' test/1_expected_document.json
sed -i '/created_at/d' test/1_expected_document.json
sed -i '/app_id/d' test/1_expected_document.json
sed -i '/event_created_at/d' test/1_expected_document.json
sed -i '/event_id/d' test/1_expected_document.json
sed -i '/id/d' test/1_expected_document.json

tr -d '[:space:]' < test/1_document.json > temp_file && mv temp_file test/1_document.json
tr -d '[:space:]' < test/1_expected_document.json > temp_file && mv temp_file test/1_expected_document.json

# Compare the cleaned files
if diff test/1_document.json test/1_expected_document.json; then
    echo "test 1 OK test passed"
else
    echo " test 1 KO test not passed"
    exit 1
fi