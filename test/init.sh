#!/bin/bash
echo "+++ running the script"
# Capture the output of the go build command

#running the service
./documentdispatcher &

# Wait for Kafka to be ready
wait-for-it kafka:9092 --timeout=15 --strict -- echo "Kafka is up"

kaf config add-cluster local -b kafka:9092 && \
kaf config use-cluster local

# Creare il topic 'documents'
kaf topic create documents

./test/1_test.sh

#template integrazione niovi test
#.test/2_test.sh