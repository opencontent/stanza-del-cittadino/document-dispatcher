# Document Dispatcher

Questo componente è parte del sistema di generazione dei documenti a partire dalle relative pratiche per permettere la protocollazione in maniera indipendente.  

## Cosa fa 

Il document Dispatcher genera i documenti utili per la protocollazione a partire dalle relative pratiche generate dalla Stanza Del Cittadino

## Struttura del repository
Il repository ha come unico riferimento il branch main. \
La struttura di base riprende i principi del hexagonal architecture in cui:\
**main.go** - entrypoint del servizio. Qui vengono assemblate e avviate le strutture seguenti.\
**intenrnal/core/domain**: - contiene le entità utilizzate dalla business logic\
**internal/core/services**: - contiente la business logic\
**internal/core/ports**: - contiente le intefacce che descrivono come i driver e i driven interagiscono la busines logic\
**internal/adapters/drivers**: - contiene i driver, ovvero le tecnologie esposte dal servizio per poter essere richiamati dall'esterno \
**internal/adapters/repositories**: - contiene i driven, ovvero le tecnologie utilizzate dal servizio per poter accedere al mondo esterno \
**config/** - contiene la struttura atta alla configurazione del servizio: variabili d'ambiente, configurazione hardcoded\
**metrics/** - contiene la descrizione delle metriche Prometheus esposte dal servizio\
**sentryUtils/** - contiene la configurazione di Sentry per il monitoring degli errori\
**logger/** - contiene la configurazione del sistema di logs in formato in formato Apache\
**utils/** - contiene le utils del codice(funzioni accessorie..) \
Altri File: \
file di configurazione per la continuous integration:\ 
dockerFile, docker-compose.yml, docker-compose.ovverride.yml, gitlab-ci.yml, dockerignore, gitignore\

## Prerequisiti e dipendenze
tutte le librerie esterne utilizzate sono elencate nel file go.mod. La sola presenza di tale file e del suo attuale contenuto permette una gestione automatica delle dipendenze. \
È necessaria l'installazione di Docker e Docker Compose per il deploy automatico del servizio e di tutti gli applicativi accessori(kafka, kafka-ui, ksqldb-server, ksqldb-cli, zookeeper..) 

## Monitoring
il sistema usufruisce di Sentry per il monitoring degli errori, inoltre sono esposti i seguenti endpoint per il monitoring : \
- /status : endpoint per l'healtcheck. Resituisce 200
- /metrics : espone le metriche in formato Prometheus

### Configurazione variabili d'ambiente
| Nome                             | Default | Descrizione                                                             | 
|----------------------------------|---------|-------------------------------------------------------------------------|
| ENVIRONMENT                      | test    | indica l'ambiente di sviluppo(locale, dev,prod...utilizzato da sentry)  |
| KAFKA_SERVER                     | test    | indirizzo del broker kafka per connetersi al cluster                    |
| KAFKA_CONSUMER_GROUP             | test    | consumer group                                                          |
| KAFKA_CONSUMER_TOPIC             | test    | identifica il topic da cui consumare gli eventi                         |
| KAFKA_PRODUCER_TOPIC             | test    | identifica il topic in cui produrre gli eventi                          |
| KAFKA_KSQL                       | test    | indica l'endpoint per connettersi a Ksqldb                              |
| KAFKA_KSQL_TABLE                 | test    | indica il nome della tabella da interrogare per vedere se un detterminato documento è stato creato                                |
| KAFKA_KSQL_ENABLED                 | true    |è impostata a false solo per i test end2end. Disattiva l'interorgazione a ksqldb                           |
| SERVER_ADDRESS_PORT              | test    | indica l'indirizzo e la porta utilizzati per l'healthcheck              |
| ENDPOINT_HEALTHCHECK             | test    | indica l'endpoint utilizzato per l'heathcheck                           |
| ENDPOINT_METRICS             | test    | indica l'endpoint utilizzato da Prometheus per le metriche                  |
| SENTRY_DSN                       | test    | endpoint per il monitoring di sentry                                    |


## Come si usa
Aggiungere il file docker-compose.override.yml avente seguente struttura : 
```yaml
version: '<your-value>'
services:
  documentdispatcher:
    build:
      context: '<your-value>'
    environment:
        ENVIRONMENT: '<your-value>'
        KAFKA_SERVER: '<your-value>'
        KAFKA_CONSUMER_GROUP: '<your-value>'
        KAFKA_CONSUMER_TOPIC: '<your-value>'
        KAFKA_PRODUCER_TOPIC: '<your-value>'
        KAFKA_KSQL: '<your-value>'
        KAFKA_KSQL_TABLE: '<your-value>'
        
        SERVER_ADDRESS_PORT: '<your-value>'
        ENDPOINT_HEALTHCHECK: '<your-value>'
        ENDPOINT_METRICS: '<your-value>'
        SENTRY_DSN: '<your-value>'

    ports:
      - "<your-value>:<your-value>"
```

Da terminale: 
1. `git clone  git@gitlab.com:opencity-labs/area-personale/document-dispatcher.git`
2. `cd document-dispatcher`
3. `docker-compose build`
4. `docker compose up -d`

## Testing
sono presenti unit test. per lanciarli, utilizzare il commando `go test .`

## Stadio di sviluppo

il software si trova in fase `Feature-complete`


## Autori e Riconoscimenti

* Opencity Labs

## License

AGPL 3.0 only

## Contatti

support@opencitylabs.it


