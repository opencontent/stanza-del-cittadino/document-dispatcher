package utils

import (
	"encoding/json"
	"errors"
	"fmt"
	"mime"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/google/uuid"
	"opencitylabs.it/documentdispatcher/config"
	"opencitylabs.it/documentdispatcher/internal/core/domain"
)

func GetEventIDs(rawData []byte) (domain.Event, error) {
	var event domain.Event
	if !json.Valid(rawData) {
		return event, errors.New("invalid JSON syntax")
	}

	err := json.Unmarshal(rawData, &event)
	if err != nil {
		return event, err
	}
	return event, nil
}

func GetApplicationStruct(rawData []byte) (domain.Application, error) {
	var applicationMessage domain.Application
	if !json.Valid(rawData) {
		return applicationMessage, errors.New("invalid JSON syntax")
	}

	err := json.Unmarshal(rawData, &applicationMessage)
	if err != nil {
		return applicationMessage, err
	}
	return applicationMessage, nil
}

func GetDocumentStruct(rawData []byte) (domain.Document, error) {
	var documentMessage domain.Document
	if !json.Valid(rawData) {
		return documentMessage, errors.New("invalid JSON syntax")
	}

	if err := json.Unmarshal(rawData, &documentMessage); err != nil {
		return documentMessage, err
	}
	return documentMessage, nil
}

func IsRightEventVersion(rawData []byte) (bool, error) {
	config := config.GetConfig()
	appVersion, err := getApplicationVersionStruct(rawData)
	if err != nil {
		return false, err
	}
	if GetIntFromInterface(appVersion.EventVersion) != config.App.ApplicationEventVersion {
		return false, nil
	}

	return true, nil
}

func getApplicationVersionStruct(rawData []byte) (domain.ApplicationEventVerison, error) {

	var applicationMessage domain.ApplicationEventVerison
	if !json.Valid(rawData) {
		return applicationMessage, errors.New("invalid JSON syntax")
	}

	err := json.Unmarshal(rawData, &applicationMessage)
	if err != nil {
		return applicationMessage, err
	}
	return applicationMessage, nil
}

func GetIntFromInterface(field interface{}) int {

	switch v := field.(type) {
	case float64:
		return int(v)
	case string:
		num, _ := strToInt(fmt.Sprintf("%v", v))
		return num
	default:
		return 0
	}

}

func strToInt(str string) (int, error) {
	i, err := strconv.ParseFloat(str, 64)
	if err != nil {
		return 0, err
	}
	return int(i), nil
}

func IsValidUUID(u string) bool {
	_, err := uuid.Parse(u)
	return err == nil
}

func GetMax255String(s string) string {

	if s != "" {
		if len(s) > 255 {
			return TruncateStringWithThreeDots(251, s)
		} else {
			return s
		}

	}
	return s
}

func ConcatIfNotPresent(s1 *string, s2 string) {
	if !strings.Contains(*s1, s2) {
		*s1 += " " + s2
	}
}

func TruncateStringWithThreeDots(length int, text string) string {
	truncatedStr := text[0:length]
	truncatedStr += "..."
	return truncatedStr
}

func GetMIMEtypeFromFileName(filename string) (string, error) {

	extension := filepath.Ext(filename)
	mimeType := mime.TypeByExtension(extension)
	if mimeType == "" {
		return "application/octet-stream", nil
	}
	return mimeType, nil

}

func SetEventFieldBeforProduce(doc *domain.Document) {
	//app id
	doc.AppId = "document-dispatcher:" + config.VERSION
	//event_created_at
	// Get the current time in UTC
	currentTimeUTC := time.Now().UTC()

	// Set Rome time zone manually (UTC+1 or UTC+2 depending on daylight saving time)
	romeLocation := time.FixedZone("Rome", 1*60*60)

	// Convert current time to Rome time zone
	currentTimeRome := currentTimeUTC.In(romeLocation)

	// Format the time as ISO 8601
	iso8601Format := "2006-01-02T15:04:05+01:00"
	romeTime := currentTimeRome.Format(iso8601Format)
	newUuidEventId := uuid.NewString()

	doc.EventCreatedAt = romeTime
	doc.UpdatedAt = doc.EventCreatedAt
	doc.EventId = newUuidEventId
	doc.AppId = "document-dispatcher:" + config.VERSION
}

func GetEventVersion(value interface{}) (float32, error) {
	switch v := value.(type) {
	case string:
		// Try to convert the string to a float32
		f, err := strconv.ParseFloat(v, 32)
		if err != nil {
			return 0, fmt.Errorf("cannot convert string to float32: %v", err)
		}
		return float32(f), nil
	case float32:
		return v, nil
	case float64:
		return float32(v), nil
	case int:
		return float32(v), nil
	default:
		return 0, fmt.Errorf("unsupported type: %T", value)
	}
}

func StringPtr(s string) *string {
	s1 := s
	return &s1
}
