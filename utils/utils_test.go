package utils

import (
	"testing"
)

func TestGetMIMEtypeFromFileName(t *testing.T) {
	input := "modulo-scia-pratica-edilizia-202310100414.pdf"
	expected := "application/pdf"
	result, err := GetMIMEtypeFromFileName(input)
	if err != nil {
		t.Fatal("unexpected error ")
	}

	if result != expected {
		t.Fatal("expected \"application/pdf\", got:", result)
	}
}

func TestGGetEventVersionString(t *testing.T) {
	input := "2.0"
	expected := 2.0
	result, err := GetEventVersion(input)
	if err != nil {
		t.Fatal("unexpected error " + err.Error())
	}

	if result != float32(expected) {
		t.Fatal("expected \"application/pdf\", got:", result)
	}
}

func TestGGetEventVersionStringInt(t *testing.T) {
	input := "2"
	expected := 2.0
	result, err := GetEventVersion(input)
	if err != nil {
		t.Fatal("unexpected error " + err.Error())
	}

	if result != float32(expected) {
		t.Fatal("expected \"application/pdf\", got:", result)
	}
}

func TestGGetEventVersionFloat64(t *testing.T) {
	input := 2.0
	expected := 2.0
	result, err := GetEventVersion(input)
	if err != nil {
		t.Fatal("unexpected error " + err.Error())
	}

	if result != float32(expected) {
		t.Fatal("expected \"application/pdf\", got:", result)
	}
}

func TestGGetEventVersionInt(t *testing.T) {
	input := 2
	expected := 2.0
	result, err := GetEventVersion(input)
	if err != nil {
		t.Fatal("unexpected error " + err.Error())
	}

	if result != float32(expected) {
		t.Fatal("expected \"application/pdf\", got:", result)
	}
}
